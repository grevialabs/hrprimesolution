<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER;

$PAGE_HEADER = NULL;
$PAGE = $MODULE = "Konten";
$bread['member'] = 'Member';
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);
$PAGE_TITLE = $PAGE.' - '.DEFAULT_PAGE_TITLE;

$do = $page_id = $list_content = $obj_page = NULL;

if (isset($_GET['do'])) $do = $_GET['do'];
if (isset($_GET['page_id'])) $page_id = $_GET['page_id'];

// SAVE
if (isset($_POST['btnSave']) && isset($_POST['name']) && isset($_POST['content'])) {
	
	$param = array(
		'name' => $_POST['name'],
		'content' => $_POST['content'],
		// 'gender' => $_POST['gender'],
	);
	
	$save = $this->page_model->save($param);
	($save)?$message['message'] = MESSAGE::SAVE : $message['message'] = MESSAGE::ERROR;
	$message['message'] = getMessage($message['message']);
}

// UPDATE
if (isset($_POST['btnUpdate']) && isset($_POST['name']) && isset($_POST['content'])) {
	// debug($_POST['content']);
	// debug($this->input->post('content'));
	// $content = preg_replace('/(<p>|<\/p>)/i','',$_POST['content']);
	$param = array(
		'name' => $_POST['name'],
		// 'slug' => $_POST['slug'],
		'content' => $_POST['content'],
	);
	
	$update = $this->page_model->update($page_id, $param);
	($update)?$message['message'] = MESSAGE::UPDATE : $message['message'] = MESSAGE::ERROR;
	$message['message'] = getMessage($message['message']);
}

if (($do == 'edit' || $do == 'view') && isset($page_id))
{
	$obj_page = $this->page_model->get( array('page_id' => $page_id) );
}
else
{
	$list_content = $this->page_model->get_list();
	$list_content = $list_content['data'];
}
?>

<script src="<?php echo base_url()?>asset/js/tinymce/tinymce.min.js"></script>
<script>tinymce.init({ 
	selector:'textarea',
	plugins: [
      'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
      'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
      'save table contextmenu directionality template paste textcolor'
    ],
    toolbar: 'styleselect | bold italic | bullist numlist | link image | print preview media fullpage'
});</script>


<div class="col-md-3">
	<?php echo $SIDEMENUBAR?>
</div>

<div class="col-md-9">
	<h1><?php echo $MODULE?></h1>
	
	<?php // echo $SETTINGMENUBAR ?>
	<?php 
	if (isset($message['message'])) echo message($message['message']).BR;
	?>
	
	<?php 
	if (!isset($do)) 
	{ 
		?>
		<!--<a href="?do=insert" class="btn btn-success br"><i class="fa fa-plus"></i>&nbsp; <?php echo ADD?></a><br/>-->
		<?php
	}
	?>
	
	<?php 
	if ((($do == "edit" || $do == "view") && !empty($obj_page)) || $do == "insert")
	{
		// if (!empty($obj_page))
		// {
			// if(isset($obj_page['content'])) $obj_page['content'] = preg_replace('^(\<p/>|<\/p>)$','',$obj_page['content']);
			?>
		<form method="post">
			Name<br/>
			<input type="text" class="input wdtFul name" name="name" value="<?php if ($obj_page['name']) echo $obj_page['name']?>" /><br/><br/>
			
			Slug<br/>
			<input type="text" class="input wdtFul slug" name="slug" value="<?php if ($obj_page['slug']) echo $obj_page['slug']?>" disabled /><br/><br/>
			
			Content
			<textarea class="input wdtFul wyiwsg" name="content" rows="25"><?php if ($obj_page['content']) echo $obj_page['content']?></textarea>
			<br/>
			<?php 
			if ( $do == 'insert')
			{
				?>
			<input type="submit" name="btnSave" class="btn btn-success btn-md wdtFul" value="<?php echo SAVE ?>"/>
				<?php 
			}
			elseif( $do == 'edit')
			{
				?>
			<input type="submit" name="btnUpdate" class="btn btn-success btn-md wdtFul" value="<?php echo UPDATE ?>"/>
				<?php 
			}
			?>
		</form>
			<?php
		// }
		if (($do == 'view' || $do == 'edit') && empty($obj_page))
		{
			echo "No data";
		}
	}
	elseif(!empty($list_content)) 
	{
		?>
		<table class="table table-bordered">
			<tr class="alert alert-warning">
				<td class="b talCnt" width="5px">#</td>
				<td class="b" class="talLft">Nama</td>
				<td class="b talCnt" width="200px">Option</td>
			</tr>
			<?php 
			foreach($list_content as $key => $rs)
			{
				$key+=1;
				$id = $rs['page_id'];
				
				?>
			<tr>
				<td class="talCnt"><?php echo $key?></td>
				<td><?php echo $rs['name']?></td>
				<td width="100px" class="talCnt">
				<a class="btn btn-success btn-sm" href="?do=edit&page_id=<?php echo $id?>"><?php echo EDIT ?></a>
				<!--<a class="btn btn-danger btn-sm" href="?do=delete&page_id=<?php echo $id?>"><?php echo DELETE ?></a>-->
				</td>
			</tr>
				<?php 
			}
			?>
		</table>
	<?php
	}
	else
	{
		echo "No data";
	}
	?>
	<br/><br/>
</div>
<?php if ($do == "insert" ) {?>
<script>
	$(document).ready(function(){
		$('.name').keyup(function(){
			var name = $(this).val().toLowerCase();
			name = name.replace(/[^\w]+/ig, "-");
			$('.slug').val(name);
		})
	})
</script>
<?php } ?>