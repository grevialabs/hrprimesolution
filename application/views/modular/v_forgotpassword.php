<?php 
global $PAGE_TITLE, $MODULE;

$MODULE = ACTIVATION;
$PAGE_TITLE = $MODULE.' - '.DEFAULT_PAGE_TITLE;

/*|
  | LOGIN
*/

$do = $email = $code = NULL;

if (isset($_GET['do'])) $do = $_GET['do'];
if (isset($_GET['email']) && is_valid_email($_GET['email'])) $email = $_GET['email'];
if (isset($_GET['code']) && is_alphanumeric($_GET['code'])) $code = $_GET['code'];


if ($do == 'submit' && is_filled($email) && is_filled($code) )
{
	$param['active_code'] = $code;
	$obj = $this->member_model->get($param);
	if (!empty($obj['email'])) 
	{
		if ($obj['is_active'] == 0)
		{
			$param['is_active'] = 1;
			$update = $this->member_model->update($obj['member_id'],$param);
			$gInfo = EMAIL.' '. $email.' '.IS_ACTIVATED;
		}
		else
		{
			$gInfo = EMAIL.' '.$email.' '.IS_ACTIVATED_ALREADY;
		}
	}
	else
	{
		$gInfo = DATA_NOT_FOUND;
	}
}


?>
<div class='col-md-3'>
</div>
<div class='col-md-6'>
	<h1 class="title-header">&nbsp;<?php echo $MODULE?></h1><hr/>
	<form method="post" id="frmReg" class="form-group">
		<div class="errReg"></div>
		<?php if (isset($gInfo))echo message($gInfo); ?>
		
		<div class="form-group form-group-md br">
			<a class="btn btn-info" href="<?php echo base_url().'login'?>"><?php echo LOGIN ?></a></div>
		</div>
		<div class='clearfix'></div><br/>
	</form>
</div>
<div class='col-md-3'>
</div>