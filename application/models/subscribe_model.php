<?php
class Subscribe_Model extends MY_Model
{
	public function get($attr = NULL)
	{
		$query = "
		SELECT *
		FROM job_subscribe
		WHERE 1
		";
		if (isset($attr['subscribe_id']) && $attr['subscribe_id'] != NULL) 
		{
			$query.= " AND subscribe_id = " . replace_quote($attr['subscribe_id']);
		}
		
		if (isset($attr['email']) && $attr['email'] != NULL) 
		{
			$query.= " AND email = " . replace_quote($attr['email']);
		}
		
		if (isset($attr['last']) && $attr['last'] != NULL) 
		{
			$query.= " ORDER BY subscribe_id DESC LIMIT 1";
		}
		$return = $this->db->query($query)->row_array();
		return $return;
	}
	
	public function get_list($attr = NULL)
	{
		$query = "
		SELECT *
		FROM job_subscribe 
		WHERE 1
		";
		if (isset($attr['email']) && $attr['email'] != NULL) 
		{
			$query.= " AND email = " . $attr['email'];
		}
		
		if (isset($attr['keyword']) && $attr['keyword'] != NULL)
		{
			$query.= ' AND name LIKE "' . $this->db->escape_like_str($attr['keyword']) . '%"';
			$query.= ' OR email LIKE "' . $this->db->escape_like_str($attr['keyword']) . '%"';
		}
		
		if(isset($attr['order']) && $attr['order'] != NULL)
		{
			$query.= ' ORDER BY ' . $attr['order'];
		}
		else
		{
			$query.= ' ORDER BY subscribe_id DESC';
		}
		
		$return['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE)
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset; 
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		$return['data'] = $this->db->query($query)->result_array();
		return $return;
	}
	
	public function save($data)
	{
		$list_field = $list_value = '';
		$query = 'INSERT INTO job_subscribe ';
		$i = 1;
		foreach($data as $key => $val)
		{
			$list_field.= $key;
			$list_value.= replace_quote($val);
			if ($i != count($data)) {
				$list_field.= ' ,';
				$list_value.= ' ,';
			}
			$i++;
		}
		
		$list_field.= ',creator_date';
		$list_value.= ','.replace_quote(getDatetime());
		
		$query.= '('.$list_field.') VALUES('.$list_value.')';
		$save = $this->db->query($query);
		if ($save) return TRUE; else return FALSE;
	}
	
	public function update($id, $data)
	{
		$query = 'UPDATE job_subscribe SET';
		$i = 1;
		foreach($data as $key => $val)
		{
			$query.= ' '.$key .' = ' . replace_quote($val);
			if ($i != count($data)) $query.= ' ,';
			$i++;
		}
		
		$query.= ', editor_date = '.replace_quote(getDatetime());
		
		$query.= ' WHERE subscribe_id = '. replace_quote($id,'num');
		$update = $this->db->query($query);
		if ($update) return TRUE; else return FALSE;
	}
	
	public function delete($id)
	{
		$query = 'DELETE FROM job_subscribe WHERE subscribe_id = ' . replace_quote($id,'num');
		$delete = $this->db->query($query);
		if ($delete) return TRUE; else return FALSE;
	}
	
	public function curl_get_list($attr = NULL) 
	{
		$return = $this->get_list($attr);
		$return = json_encode($return);
		return $return;
	}
	
}