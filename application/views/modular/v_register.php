<?php 
global $PAGE_TITLE;

$PAGE_TITLE = REGISTER.' - '.DEFAULT_PAGE_TITLE;

/*|
  | REGISTER
*/
if ($_POST) {
	$gInfoReg = NULL;
	if(post('hdnRegister') == 1) {
		
		if(!is_filled($_POST['first_name'])) {
			$gInfoReg.= NAME.' '.MUST_FILLED;
		} else if(!is_filled($_POST['email'])) {
			$gInfoReg.= EMAIL.' '.MUST_FILLED;
		} else if(!is_filled($_POST['password'])) {
			$gInfoReg.= PASSWORD.' '.MUST_FILLED;
		} else if(!is_filled($_POST['gender'])) {
			$gInfoReg.= PLEASE_SELECT.' '.NAME;
		} else if(!is_filled($_POST['dob'])) {
			$gInfoReg.= PLEASE_SELECT.' '.DOB;
		} else {
			$objMember = NULL;
			$objMember = $this->member_model->get(
				array('email' => $_POST['email'])
			);
			if (empty($objMember)) {
				$email = $_POST['email'];
				$full_name = $_POST['first_name'].' '.$_POST['last_name'];
				$active_code = genRandomString(30);
					
				$param = array(
					'first_name' => $_POST['first_name'],
					'last_name' => $_POST['last_name'],
					'email' => $_POST['email'],
					'thepassword' => encrypt($_POST['password']),
					'gender' => $_POST['gender'],
					'dob' => $_POST['dob'],
					'active_code' => $active_code,
					'is_admin' => 0,
					'is_subscribe' => 1,
					'is_active' => 0,
					'is_deleted' => 0,
				);
				
				$save = $this->member_model->save($param);
				$save = TRUE;
				if($save){			
					
					// benerin lagi nih
					$email_member ='<html><head></head><body>';
					$email_member.='<div style="padding:15px">'.DEAR.' '.$full_name.',<br/><br/>'.YOU_GOT_THIS_EMAIL_BECAUSE_YOU_REGISTER_IN.' '.DEFAULT_PAGE_TITLE.'.';
					$email_member.= PLEASE_CLICK.' <a href="'.base_url().'activation?do=submit&email='.$email.'&code='.$active_code.'">'.THIS_LINK.'</a> '.OR_COPY_PASTE_THIS_URL_TO_BROWSER.' '.base_url().'activation?do=submit&email='.$email.'&code='.$active_code.' '.TO_CONFIRM_YOUR_ACCOUNT.'<br/><br/>';
					$email_member.='<p>'.WARM_REGARDS.'.<br/>'.JOBTALENTO_TEAM.'.</p></div></body></html>';
					if (!is_internal()) {
						// SEND EMAIL HERE
						$to = $email;
						$subject = ACTIVATION_ACCOUNT_MEMBER;
						$headers = "From: ".JOBTALENTO_TEAM.".<noreply@jobtalento.com> \r\n";
						$headers .= "Reply-To: \r\n";
						$headers .= "MIME-Version: 1.0\r\n";
						$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
						$send = mail($to,$subject,$email_member,$headers);
					} else {
						unset($_POST);
						$gInfoReg.= REGISTRATION_SUCCESS.' '.PLEASE_LOGIN.' '.$email_member;
					}
				} else {
					$gInfoReg = REGISTRATION_FAIL_PLEASE_TRY_AGAIN;
				}
			}else{
				$gInfoReg = EMAIL.' <b>'.$_POST['email'].'</b> '. HAS_REGISTERED.' '.PLEASE_USE_ANOTHER_EMAIL;
			}
		}
	}
}
?>
<div class="col-sm-3">
&nbsp;
</div>
<div class='col-sm-6'>
	<h1 class="title-header">&nbsp;<?php echo REGISTRATION?></h1><hr/>
	<div class='col-sm-2'>
		<div id='fblogin'><fb:login-button scope="public_profile,email" size="large" onlogin="checkLoginState();"></fb:login-button></div>
	</div>
	<!--
	<div class='col-sm-2'>
		<div id="gConnect">
			<button class="g-signin"
				data-scope="https://www.googleapis.com/auth/plus.profile.emails.read"
				data-requestvisibleactions="http://schemas.google.com/AddActivity"
				data-clientId="<?php //echo GOOGLE_CLIENT_ID ?>"
				data-callback="onSignInCallback"
				data-theme="dark"
				data-cookiepolicy="single_host_origin">
			</button>
		</div>
	</div>
	-->
	<div class='clearfix'></div>
	
	<form method="post" id="frmReg" class="form-group">
		<div class="errReg"></div>
		<?php // if (isset($gInfoReg))echo message($gInfoReg); ?>
		<div class="input-group margin-bottom-sm br">
			<span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
			<div class='col-sm-6 padNon' style="padding-right:5px">
			<input class="form-control" type="text" placeholder="<?php echo FIRST_NAME?>" name="f_first_name" date-toggle="senpai" value='<?php if (isset($_POST['first_name'])) echo $_POST['first_name']?>' required/>
			</div>
			<div class='col-sm-6 padNon' >
			<input class="form-control" type="text" placeholder="<?php echo LAST_NAME?>" name="f_last_name" date-toggle="senpai" value='<?php if (isset($_POST['last_name'])) echo $_POST['last_name']?>' required/>
			</div>
			<input type='hidden' name='f_FacebookID' id='f_FacebookID'/>
		</div>
		<div class="input-group margin-bottom-sm br">
		  <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
		  <input class="form-control" type="email" placeholder="<?php echo EMAIL?>" name="f_email" value='<?php if(isset($_POST['email'])) echo $_POST['email']?>' required/>
		</div>
		<div class="input-group margin-bottom-sm br">
		  <span class="input-group-addon"><i class="fa fa-lock fa-fw"></i></span>
		  <input class="form-control" type="password" placeholder="<?php echo PASSWORD?>" name="f_password" minlength="4" required/>
		</div>
		<div class="input-group margin-bottom-sm br">
		  <span class="input-group-addon"><i class="fa fa-neuter fa-fw"></i></span>
		  <select name="f_gender" id='f_gender' placeholder="Gender" title="<?php echo SELECT_GENDER?>" class="form-control" required>
			<option></option>
			<option value="1" <?php if(isset($_POST['gender']) && $_POST['gender'] == 1)echo 'selected'?>><?php echo MALE?></option>
			<option value="2" <?php if(isset($_POST['gender']) && $_POST['gender'] == 2)echo 'selected'?>><?php echo FEMALE?></option>
		  </select>
		</div>
		<div class="input-group margin-bottom-sm br">
		  <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
		  <input class="datepicker form-control" type="text" placeholder="Tgl lahir" name="f_dob" value='<?php if(isset($_POST['dob'])) echo $_POST['dob']?>' required/>
		</div>
		<div class='form-group form-group-md padTop10 br'>
			<div class='col-sm-12'><label for='f_IsAgree'> <?php echo INFO_I_AGREE_TERM_CONDITION?> </label></div><br/>
		</div>
		<div class="form-group form-group-md padTop10 padNon br" >
			<div class="col-sm-12 padTop10 br talCnt"><input type='hidden' name='hdnRegister' value='1'/><button value="Register" class="btn btn-info wdtFul"/><?php echo REGISTER?></button></div>
		</div>
		<div class='clearfix'></div><br/>
		
		<?php if (isset($gInfoReg))echo print_message($gInfoReg); ?>
		
	</form>
</div>
<div class="col-sm-3">
&nbsp;
</div>


<!-- START FACEBOOK -->

<script>
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
	  //$('#fblogin').hide();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      //document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      // document.getElementById('status').innerHTML = 'Please log ' +
        'into Facebook.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '<?php echo FACEBOOK_APP_ID?>',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.1' // use version 2.1
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    //console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
      //alert(JSON.stringify(response));
	  //console.log('Successful login for: ' + response.name);
      //document.getElementById('status').innerHTML = 'Thanks for logging in, ' + response.name + '!';
	  <?php if (!is_filled(get('data'))) { ?>
	  //document.getElementById('status').innerHTML = JSON.stringify(response);
	  //window.location = '<?php echo base_url().'login?facebook=1'?>' + '&data=' + JSON.stringify(response);
	  <?php } ?>
	  document.getElementById('f_FacebookID').value = response.id;

	  document.getElementById('f_FullName').value = response.name;
	  document.getElementById('f_Email').value = response.email;
	  var userGender = response.gender;
	  if (response.gender == 'male') userGender = '1';
	  else if( response.gender == 'female') userGender = '2';
		var dd = document.getElementById('f_Gender');
		for (var i = 0; i < dd.options.length; i++) {
			if (dd.options[i].value == userGender) {
				dd.selectedIndex = i;
				break;
			}
		}
    });
  }
</script>
<!-- GOOGLE + LOGIN 
<script src="https://apis.google.com/js/client:platform.js" async defer></script>

GOOGLE + LOGIN -->


<script type="text/javascript">
// var helper = (function() {
  // var BASE_API_PATH = 'plus/v1/';

  // return {
    // /**
     // * Hides the sign in button and starts the post-authorization operations.
     // *
     // * @param {Object} authResult An Object which contains the access token and
     // *   other authentication information.
     // */
    // onSignInCallback: function(authResult) {
      // gapi.client.load('plus','v1').then(function() {
        // $('#authResult').html('Auth Result:<br/>');
        // for (var field in authResult) {
          // $('#authResult').append(' ' + field + ': ' +
              // authResult[field] + '<br/>');
        // }
        // if (authResult['access_token']) {
          // $('#authOps').show('slow');
          // $('#gConnect').hide();
          // helper.profile();
          // helper.people();
        // } else if (authResult['error']) {
          // // There was an error, which means the user is not signed in.
          // // As an example, you can handle by writing to the console:
          // console.log('There was an error: ' + authResult['error']);
          // $('#authResult').append('Logged out');
          // $('#authOps').hide('slow');
          // $('#gConnect').show();
        // }
        // console.log('authResult', authResult);
      // });
    // },

	// /**
     // * Calls the OAuth2 endpoint to disconnect the app for the user.
     // */
    // disconnect: function() {
      // // Revoke the access token.
      // $.ajax({
        // type: 'GET',
        // url: 'https://accounts.google.com/o/oauth2/revoke?token=' +
            // gapi.auth.getToken().access_token,
        // async: false,
        // contentType: 'application/json',
        // dataType: 'jsonp',
        // success: function(result) {
          // console.log('revoke response: ' + result);
          // $('#authOps').hide();
          // $('#profile').empty();
          // $('#visiblePeople').empty();
          // $('#authResult').empty();
          // $('#gConnect').show();
        // },
        // error: function(e) {
          // console.log(e);
        // }
      // });
    // },

    // /**
     // * Gets and renders the list of people visible to this app.
     // */
    // people: function() {
      // gapi.client.plus.people.list({
        // 'userId': 'me',
        // 'collection': 'visible'
      // }).then(function(res) {
        // var people = res.result;
        // // $('#visiblePeople').empty();
        // // $('#visiblePeople').append('Number of people visible to this app: ' +
            // // people.totalItems + '<br/>');
        // // for (var personIndex in people.items) {
          // // person = people.items[personIndex];
          // // $('#visiblePeople').append('<img src="' + person.image.url + '">');
        // // }
      // });
    // },

    // /**
     // * Gets and renders the currently signed in user's profile data.
     // */
    // profile: function(){
      // gapi.client.plus.people.get({
        // 'userId': 'me'
      // }).then(function(res) {
        // var profile = res.result;
		// var email_google;
		// alert(res.result.emails.value);
		// <?php if (is_filled(get('data')) ) { ?>
		// //window.location = '<?php echo base_url().'login?data=' ?>' + JSON.stringify(profile);
		// <?php } ?>
        // $('#f_Email').val(profile.email);
		// $('#f_FullName').val(profile.displayName);
		// email_google = JSON.parse(profile.emails);
		// alert(email_google[0].value);
		// $('#profile').empty();
		// $('#profile').append(JSON.stringify(profile));
        // // $('#profile').append(
            // // $('<p><img src=\"' + profile.image.url + '\"></p>'));
        // // $('#profile').append(
            // // $('<p>Hello ' + profile.displayName + '!<br />Tagline: ' +
            // // profile.tagline + '<br />About: ' + profile.aboutMe + '</p>'));
        // // if (profile.cover && profile.coverPhoto) {
          // // $('#profile').append(
              // // $('<p><img src=\"' + profile.cover.coverPhoto.url + '\"></p>'));
        // // }
      // }, function(err) {
        // var error = err.result;
        // $('#profile').empty();
        // $('#profile').append(error.message);
      // });
    // }
  // };
// })();

/**
 * jQuery initialization
 */
$(document).ready(function() {
  $('#disconnect').click(helper.disconnect);
  // if ($('[data-clientId="<?php echo GOOGLE_CLIENT_ID?>"]').length > 0) {
    // alert('This sample requires your OAuth credentials (client ID) ' +
        // 'from the Google APIs console:\n' +
        // '    https://code.google.com/apis/console/#:access\n\n' +
        // 'Find and replace YOUR_CLIENT_ID with your client ID.'
    // );
  // }
});

/**
 * Calls the helper method that handles the authentication flow.
 *
 * @param {Object} authResult An Object which contains the access token and
 *   other authentication information.
 */
function onSignInCallback(authResult) {
  helper.onSignInCallback(authResult);
}
</script>
<!--
  Below we include the Login Button social plugin. This button uses
  the JavaScript SDK to present a graphical Login button that triggers
  the FB.login() function when clicked.
-->


<!-- END FACEBOOK -->

	