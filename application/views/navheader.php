<!--
<div class="container" style="bottom:0">
	<div class="col-md-12" style="bottom:0">
	<ul class="list-inline topList">
		<li style="border-left:1px solid #000">Test</li>
		<li style="border-left:1px solid #000">AA</li>
		<li style="border-left:1px solid #000">AABB</li>
	</ul>
	</div>
</div>
-->
<!-- Static navbar -->
<?php 

global $current_segment;
if ($this->uri->segment(1)) $current_segment = $this->uri->segment(1);

function menu_active($page) 
{
	global $current_segment;
	$str = '';
	if ($current_segment === $page)
	{
		$str = 'nyala';
	}
	return $str;
}
	
function nyala($page)
{
	$uri = $ret = NULL;
	if ($this->uri->segment(1))
	{
		$uri = $this->uri->segment(1);
		if ($uri == $page) $ret = "nyala";
	}
	return $ret;
}
?>
    <nav class="navbar " style="padding-bottom:10px; margin-bottom:0px; border-bottom:1px solid #DBDBEA; background: #FFFFFF">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            
            <span class="fa fa-bars" style="max-height:10px;padding:none"></span>
          </button>
          
		  
		  <!-- HOME YA -->
		  <a class="navbar-brand no-u img-responsive" href="<?php echo base_url()?>"><img src="<?php echo base_url().'asset/images/logo_baru.png'?>" width="180px"/></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse" style="margin-top:15px">
		  <ul class="nav navbar-nav lnkMenu">
			
          </ul>
		  <ul class="nav navbar-nav navbar-right clrWht lnkMenu">
			<li class="<?php echo menu_active('home') ?>"><a href="<?php echo base_url()?>">Home <?php echo show_active_menu('index')?></a></li>
			<li class="<?php echo menu_active('about') ?>"><a href="<?php echo base_url()?>about">About Us</a></li>
			<li class="dropdown <?php //echo menu_active('service') ?>">
			<a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" href="<?php echo base_url()?>service">Our Service <span class="caret"></span></a>
			
			<?php 
			$list_service = $this->page_model->get_list(array('list_page_id' => '6,7,8,9,10,11'));
			$list_service = $list_service['data'];
			if (!empty($list_service))
			{
				?>
				<ul class="dropdown-menu" role="menu">
				<?php
				foreach($list_service as $service)
				{
				$url = base_url().'our_service/'.$service['slug'];
				?> 
				<li><a href="<?php echo $url ?>"><?php echo $service['name']?></a></li>
				<?php
				}
				?>
				</ul>
				<?php
			}
			?>
			</li>
			<li class="<?php echo menu_active('why') ?>"><a href="<?php echo base_url()?>why">Why Us</a></li>
			<li class="<?php echo menu_active('career') ?>"><a href="<?php echo base_url()?>career">Career</a></li>
			<li class="<?php echo menu_active('contact') ?>"><a href="<?php echo base_url()?>contact">Contact</a></li>
			
		  </ul>

        </div><!--/.nav-collapse -->
      </div>
    </nav>
	
