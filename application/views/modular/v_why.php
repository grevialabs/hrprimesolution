<?php
global $PAGE, $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;

$PAGE = "Why Us";
$BREADCRUMB = $this->common_model->breadcrumb(NULL, $PAGE);
$PAGE_TITLE = $PAGE.' - '.DEFAULT_PAGE_TITLE;;
$PAGE_HEADER = $PAGE;

$gInfo = '';

$img = base_url().'asset/images/';
?>

<div class="jumbotron bgWht">
	<div class="container">
		<div class="row">
			
			<?php if (isset($BREADCRUMB)) echo $BREADCRUMB.'' ?>
			<?php if (isset($PAGE_HEADER)) echo '<h2>'.$PAGE_HEADER.'</h2>' ?>
		
			<div class="col-sm-4 talCnt">
				<img src="<?php echo $img.'why_1.jpg';?>" class="" /><br/>
				<span class="b i">We Care</span><br/><br/>
				
				<div class="talJst">
				As a small business, we are able to show each of our individual clients that we care through the personalized services we provide. If you have a question, we are there to help. We will often reach out to you instead of you fighting to be able to talk to us. Attentive, personal service is our specialty.
				</div>
			</div>
			<div class="col-sm-4 talCnt">
				<img src="<?php echo $img.'why_2.jpg';?>" class="" /><br/>
				<span class="b i">We're Experienced</span><br/><br/>
				
				<div class="talJst">
				With our consultants’ expertise and our experience in the field of human resource management, we have worked in a variety of industries, for a wide variety of clients and businesses. Understanding how these types of business models differ helps us to better suit our services to fit your company’s needs.
				</div>
			</div>
			<div class="col-sm-4 talCnt">
				<img src="<?php echo $img.'why_3.jpg';?>" class="" /><br/>
				<span class="b i">We Can Help</span><br/><br/>
				
				<div class="talJst">
				While the bigger, corporate human resource management companies might just provide you with generic, pre-planned services, we are able to customize our services to meet your needs. This kind of attention will surely help your business while allowing you to focus on other aspects of the company.
				</div>
			</div>
			
			<div class="col-sm-12">
			<hr/>
				Other than that, HR Prime Solution are:
				<ul>
				<li>Experienced & well networked consultants</li>
				<li>Strong team of highly qualified HR Consultants and  Associates</li>
				<li>Personalized and flexible delivery options</li>
				<li>Value for money and long term partner in progress</li>
				<li>All services are tailored to meet clients needs</li>
				<li>Various programs and services</li>
				</ul>
			</div>
		</div>
	</div>
</div>