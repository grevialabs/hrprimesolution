<?php

Class MY_Controller extends CI_Controller
{
	public $CI = NULL;
	
	public function __construct()
	{
		
		parent::__construct();
		
		$this->CI = & get_instance();
		
		$this->load->model('common_model');
		
		
		if ($_POST) {
			$tmp = array();
			foreach($_POST as $key => $val){
				// STRPOS use !== to accept 0 or TRUE
				if (isset($key) && strpos($key,'f_') !== FALSE) {
					$field = explode('f_',$key);
					$tmp[$field[1]] = $val;
				} else {
					if (is_array($val)) {
						foreach($val as $k => $dval) {
							// $dval = mysql_real_escape_string($dval);
							$dval = $this->db->escape_str($dval);
							$val[$k] = $dval;
						}
						$tmp[$key] = $val;
					} else {
						// $val = mysql_real_escape_string($val);
						$val = $this->db->escape_str($val);
						$tmp[$key] = $val;
					}
				}
			}
			$_POST = NULL;
			$_POST = $tmp;
		}
		
		/* START LANGUAGE */
		
		$lang = NULL;
		$allowed_lang = array('en','id');
		if (!isset($_COOKIE['the_lang'])) {
			if (isset($_GET['lang']) && in_array($_GET['lang'],$allowed_lang) == TRUE) 
				$lang = $_GET['lang'];
			else
				$lang = 'en';
			setcookie('the_lang',$lang, time()+86400, base_url());
		} else {

			if (isset($_COOKIE['the_lang']) && in_array($_COOKIE['the_lang'],$allowed_lang) == TRUE) {
				$lang = $_COOKIE['the_lang'];
			} else {
				$lang = "en";
			}
			//setcookie('the_lang',$lang, time()+86400, base_url());
			
			if (isset($_GET['lang']) && in_array($_GET['lang'],$allowed_lang) == TRUE) {
				$lang = $_GET['lang'];
				setcookie('the_lang',$lang, time()+86400, base_url());
			}
		
		}
		$lang = 'id';
		$this->lang->load('general',$lang);
		
		if (isset($_GET['do']) && $_GET['do'] == "redir") {
			if (isset($_GET['url'])) {
				$url = urldecode($_GET['url']);
				redirect($url);
			} 
		}
		
		global $config;
		$this->load->model('config_model');
		$config = $this->config_model->get();
		// debug($config);
	}
	
	public function active_fetch_class() 
	{
		return $this->router->fetch_class();
	}
	
	public function fetch_method() 
	{
		return $this->router->fetch_method();
	}
	
	public function curl($url,$method,$data)
	{
		$url = 'http://localhost/jobtalento.com/'.$url;
		
		$ch = curl_init( $url );
		
		# Setup request to send json via POST.
		if (isset($method) && $method == "post")
		{
			
			$data_post = '';
			//create name value pairs seperated by &
			foreach($data as $k => $v) 
			{ 
			  $data_post .= $k . '='.$v.'&'; 
			}
			$data_post = rtrim($data_post, '&');
		    
			curl_setopt( $ch, CURLOPT_POST, count($data_post));
			curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_post );

		}
		
		# Return response instead of printing.
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		
		# Send request.
		$result = curl_exec($ch);
		curl_close($ch);
		
		# Print response.
		// echo "<pre>";
		return $result;
		// echo "</pre>";
	}
	
	/*
	 * Callback: 
	 array
	 response: string,
	 error_msg: string,
	*/
	public function curl_sendblue($method = 'post', $data)
	{
		$result = $smtp = NULL;
		$smtp['url'] = 'https://api.sendinblue.com/v3/smtp/email';
		$smtp['key'] = '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';
		// $smtp['from'] = 'grevianet@gmail.com';
		
		$ch = curl_init($smtp['url']);
		
		# Setup request to send json via POST.
		if (isset($method) && $method == "post")
		{
			
			$data_post = '';
			curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($data) );
		}
	
		# Return response instead of printing.
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		
		# bypass restriction ssl from localhost
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		
		$headers = array(
			'Token: grevia',
			'api-key: ' . $smtp['key'],
			'content-type: application/json',
			'accept: application/json',
		);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);			

		# Send request.
		$result['response'] = curl_exec($ch);
		if (isset($result['response'])) $result['response'] = json_decode($result['response'],1);
		
		if (curl_errno($ch)) {
			$result['error_msg'] = curl_error($ch);
		}
		
		curl_close($ch);
		
		# Print response.
		return $result;
	}
	
	function paging($row_per_page,$total)
	{
		echo 'bisa';die;
		if((int) $total <= (int)$row_per_page ){return;}
		$ci=& get_instance();
		$ret=null;
		$_get=null;
		foreach($_GET as $key => $value){
			if($key=='page'){continue;}
			$_get[$key]=$value;
			
		}
		
		$url=base_url().$ci->uri->uri_string();
		$param=@http_build_query($_get);

		for($i=1;$i <= ceil($total/$row_per_page);$i++){
			$pages=($i<=1)?null:'page='.$i;
			$url_x=($pages || $param)?$url.'?'.$param.'&'.$pages:$url;
			$class=($i==$_GET['page'])?'class="selected"':null;
			
			if($i<=3 || $i>=ceil($total/$row_per_page)-2 || ($i>=$_GET['page']-1 && $i<=$_GET['page']+1) ){
				$ret[]='<a href="'.$url_x.'" '.$class.' ><span>'.$i.'</span></a>';
			}else{
				if($_GET['page']<=$i){
					$ret['pinaple']='..';
				}else{
					$ret['orange']='..';
				}
			}
			
		}
		
		return $ret='<div class="pagin">'.implode('',(array) $ret).'<form action="'.$url.'" style="display:inline-block"><input value="'.$_GET['page'].'" name="page" class="small"><button class="small">Go</button></form></div>';
	}
	
}