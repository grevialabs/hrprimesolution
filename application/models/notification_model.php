<?php

Class Notification_model extends MY_Model 
{
	public function __construct() {
        parent::__construct();
    }
	
	public function get($attr = NULL) 
	{
		$query = '
		SELECT n.*
		FROM job_notification n
		WHERE 1';
		
		if (isset($attr['notification_id'])) 
		{
			$query.= ' AND notification_id = ' . replace_quote($attr['notification_id']);
		}
		
		if (isset($attr['company_id'])) 
		{
			$query.= ' AND company_id = ' . replace_quote($attr['company_id']);
		}
		
		$result = $this->db->query($query)->row_array();
		return $result;
	}
	
	public function get_list($attr = NULL) 
	{
		$query = '
		SELECT *
		FROM job_notification 
		WHERE 1';
		
		if (isset($attr['notification_id'])) 
		{
			$query.= ' AND notification_id = ' . $attr['notification_id'];
		}
		
		if (isset($attr['is_read'])) 
		{
			$query.= ' AND is_read = ' . $attr['is_read'];
		}
		
		if (isset($attr['is_delete'])) 
		{
			$query.= ' AND is_delete = ' . $attr['is_delete'];
		}
		
		if (isset($attr['company_id'])) 
		{
			$query.= ' AND company_id = ' . $attr['company_id'];
		}
		
		$query.= ' ORDER BY creator_date DESC';
		
		$result['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE) 
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		$result['data'] = $this->db->query($query)->result_array();
		return $result;
	}
	
	public function save($data)
	{
		$list_field = $list_value = '';
		$query = 'INSERT INTO job_notification ';
		$i = 1;
		foreach($data as $key => $val)
		{
			$list_field.= $key;
			$list_value.= replace_quote($val);
			if ($i != count($data)) {
				$list_field.= ' ,';
				$list_value.= ' ,';
			}
			$i++;
		}
		$list_field.= ',creator_ip, creator_date';
		
		$list_value.= ','.replace_quote(getIP());
		$list_value.= ','.replace_quote(getDatetime());
		
		$query.= '('.$list_field.') VALUES('.$list_value.')';
		$save = $this->db->query($query);
		if ($save) return TRUE; else return FALSE;
	}
	
	public function update($id, $data)
	{
		$query = 'UPDATE job_notification SET';
		$i = 1;
		foreach($data as $key => $val)
		{
			$query.= ' '.$key .' = ' . replace_quote($val);
			if ($i != count($data)) $query.= ' ,';
			$i++;
		}
		$query.= ', editor_ip = '.replace_quote(getIP());
		$query.= ', editor_date = '.replace_quote(getDatetime());
		$query.= ' WHERE notification_id = '. replace_quote($id,'num');

		$update = $this->db->query($query);
		if ($update) return TRUE; else return FALSE;
	}
	
	public function delete($id)
	{
		$query = 'DELETE FROM job_notification WHERE notification_id = ' . $id;
		$delete = $this->db->query($query);
		if ($delete) return TRUE; else return FALSE;	
	}
	
}