<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('test_method'))
{
    function test_method($var = '')
    {
        $var = 'ahsenpai';
		return $var;
    }   
}

function cookie_order_id()
{
	$ret = '';
	if (isset($_COOKIE['order_id']) && is_numeric($_COOKIE['order_id']))
	{
		$ret = $_COOKIE['order_id'];
	}
	return $ret;
}

function cdn_url()
{
	return base_url().'asset/';
}

function asset_url()
{
	return base_url().'asset/';
}

function debug($arr){
	echo "<pre>".print_r($arr,1)."</pre>";
}

