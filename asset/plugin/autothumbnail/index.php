<?
// Rainbo Design PHP Thumbnail Maker
// Copyright (C) 2005-2012 by Richard L. Trethewey - rick@rainbo.net
// All Rights Reserved
// If you use this script, I'd appreciate a link!
// http://www.rainbodesign.com/pub/
// 05/12/12 Modified to accept /thumbs/(.*)
// 06/01/12 Modified for imagecopyresampled() and added quality setting in imagejpeg()

// User Settings
$thumbsize = 150;				// Default thumbnail width.


$error = '';

if (isset($_GET['width'])) { $thumbsize = $_GET['width']; }
if (isset($_GET['src'])) { $imagesource =  $_SERVER['DOCUMENT_ROOT'] . "/" . $_GET['src']; }

$filetype = substr($imagesource,strlen($imagesource)-4,4);
$filetype = strtolower($filetype);

if (file_exists($imagesource)) {

if($filetype == ".gif")  $image = @imagecreatefromgif($imagesource); 
if($filetype == ".jpg")  $image = @imagecreatefromjpeg($imagesource); 
if($filetype == ".png")  $image = @imagecreatefrompng($imagesource);

$imagewidth = imagesx($image);
$imageheight = imagesy($image); 

if ($imagewidth >= $thumbsize) {
  $thumbwidth = $thumbsize;
  $factor = $thumbsize / $imagewidth;
  $thumbheight = floor($imageheight * $factor);
} else {
  $thumbwidth = $imagewidth;
  $thumbheight = $imageheight;
  $factor = 1;
 }

// Create a thumbnail-sized GD Image object
$thumb = @imagecreatetruecolor($thumbwidth,$thumbheight);

// bool imagecopyresampled ( resource dst_image, resource src_image, int dst_x, int dst_y, int src_x, int src_y, int dst_w, int dst_h, int src_w, int src_h )
imagecopyresampled($thumb, $image, 0, 0, 0, 0, $thumbwidth, $thumbheight, $imagewidth, $imageheight);

// Send output to user as a jpeg type, regardless of original type
header("Content-type:image/jpeg;");
imagejpeg($thumb, NULL, 90);
imagedestroy($image);
imagedestroy($thumb);
 } else {
 $error = "File " . $_GET['src'] . " Not Found";
 } // endif file_exists

 if ($error != '') {
      header("HTTP/1.0 404 Not Found");
 } // endif $error

      exit;

?>
