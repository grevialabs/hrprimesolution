<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER;

$PAGE_HEADER = NULL;
$PAGE = $MODULE = PROFILE;
$bread['member'] = 'Member';
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);
$PAGE_TITLE = $PAGE.' - '.DEFAULT_PAGE_TITLE;

if (isset($_POST['btnUpdate'])) {
	$is_subscribe = 0;
	$dob = NULL;
	if (isset($_POST['is_subscribe'])) {
		$is_subscribe = 1;
	}
	if (isset($_POST['dob'])) {
		$tempdob = explode('-',$_POST['dob']);
		$dob = $tempdob[2].'-'.$tempdob[1].'-'.$tempdob[0];
	}
	$member_id = member_cookies('member_id');
	$param = array(
		'first_name' => $_POST['first_name'],
		'last_name' => $_POST['last_name'],
		'gender' => $_POST['gender'],
		'dob' => $dob,
		'is_subscribe' => $is_subscribe,
	);
	
	$update = $this->member_model->update($member_id, $param);
	($update)?$message['message'] = MESSAGE::UPDATE : $message['message'] = MESSAGE::ERROR;
	$message['message'] = getMessage($message['message']);
}

if ($this->uri->segment(3)) { 
	$data = $this->member_model->get(array('member_id' => $this->uri->segment(3)));
}
if (is_member()) {
	$data = $this->member_model->get(array('member_id' => member_cookies('member_id')));
	/*
	$ci =& get_instance();
	$data = NULL;
	$data = $ci->curl('api_member/get','post', array('member_id' => member_cookies('member_id')) );
	$data = (array) json_decode($data);
	*/
}

?>
<div class="col-md-3">
	<?php echo $SIDEMENUBAR?>
</div>

<div class="col-md-9">
	<h1><?php echo $MODULE?></h1><br/>
	
	<?php echo $SETTINGMENUBAR ?>
	
	<?php 
	if (isset($message['message'])) echo message($message['message']).BR;
	?>
	<?php 
	if(!empty($data)) {
		$obj = $data;
		if (isset($obj['dob'])) {
			$tempdob = explode('-',$obj['dob']);
			$dob = $tempdob[2].'-'.$tempdob[1].'-'.$tempdob[0];
		}
	?>
	<form class='form-horizontal' role='form' method='post'>
		<div class='form-group form-group-sm'>
			<label for='f_FullName' class='col-sm-2'><?php echo FULL_NAME?></label>
			<div class='col-sm-5'><input type='text' class='form-control' name='f_first_name' id='f_first_name' placeholder='<?php echo FILL.' '.FIRST_NAME ?>' value='<?php echo $obj['first_name']?>'></div>
			<div class='col-sm-5'><input type='text' class='form-control' name='f_last_name' id='f_last_name' placeholder='<?php echo FILL.' '.LAST_NAME ?>' value='<?php echo $obj['last_name']?>'></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_email' class='col-sm-2'><?php echo EMAIL?></label>
			<div class='col-sm-10'><input type='email' class='form-control' name='f_email' id='f_email' placeholder='<?php echo FILL.' '.EMAIL?>' value='<?php echo $obj['email']?>' disabled></div>
		</div>		
		<div class='form-group form-group-sm'>
			<label for='f_Gender' class='col-sm-2'><?php echo GENDER?></label>
			<div class='col-sm-10'>
			<select class='form-control' name='f_gender'>
			<option value='1' <?php if ($obj['gender'] == 1) echo 'selected'?>><?php echo MALE?></option>
			<option value='2' <?php if ($obj['gender'] == 2) echo 'selected'?>><?php echo FEMALE?></option>
			</select>
			</div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_DOB' class='col-sm-2'><?php echo DOB?></label>
			<div class='col-sm-10'><input type='text' class='form-control datepicker' name='f_dob' id='f_dob' placeholder='<?php echo FILL.' '.DOB?>' value='<?php if (isset($dob)) echo $dob?>'/></div>
		</div>
		<!--
		<div class='form-group form-group-sm'>
			<label for='f_Facebook' class='col-sm-2'>Facebook</label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_Facebook' id='f_Facebook' placeholder='Enter Facebook' value='<?php //echo $obj['Facebook']?>'></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_Twitter' class='col-sm-2'>Twitter</label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_Twitter' id='f_Twitter' placeholder='Enter Twitter' value='<?php //echo $obj['Twitter']?>'></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_AboutMe' class='col-sm-2'><?php echo ABOUT_ME?></label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_AboutMe' id='f_AboutMe' placeholder='Enter AboutMe' value='<?php// echo $obj['AboutMe']?>'></div>
		</div>
		-->
		<div class='form-group form-group-sm'>
			<label for='f_is_subscribe' class='col-sm-2'><?php echo SUBSCRIBE_NEWSLETTER?></label>
			<div class='col-sm-10 checkbox'>
				<label><input type='checkbox' name='f_is_subscribe' id='f_is_subscribe' value='1' <?php if ($obj['is_subscribe']) echo 'checked'?>></label>
			</div>
		</div>
		<div class='form-group padLeft'>
			<button class='btn btn-success' name='btnUpdate' value='1'><?php echo UPDATE?></button>
		</div>

		<!--
		<p><input type="file" name="f_image"/><br/>
		<?php if(is_filled($image)) echo '<img src="'.base_url().'/data/image/'.$image.'"/>'?>
		</p>
		-->
	</form>
	<!--<form class='form-horizontal'><div class='form-group padLeft'><button class="btn btn-danger" onclick="return confirm('apakah anda yakin ? \n Aksi ini akan menghapus permanen akun anda. ');"><?php echo DELETE.' '.ACCOUNT?></button></div></form>-->
	<?php
	}
	?>
</div>