<?php 

global $config;
// var_dump($config);die;

$url_service = base_url().'our_service/';
?>
<div class="jumbotron talCnt bg-home">
	<div class="container">
		<div class="rows bitter b h485" >
			<br/><br/>
			<br/><br/>
			<div class="col-sm-12 clrWht" style="opacity: 0.9;filter: alpha(opacity=90);">
				<br/><br/><br/><br/>
				<h2 class="talCnt">One step closer to your HR Solution</h2>
				
				<br/><br/>
				
				<div class="talCnt"><a href="<?php echo base_url().'contact'?>"><button class="btn btn-success">Start your Inquiry now</button></a></div>
				<br/><br/>
			</div>
			
			<div class="col-sm-3"></div>
			<div class="col-sm-6">
				
			</div>
			<div class="col-sm-3"></div>
			
		</div>
	</div>
</div>
<!--
<div class="container-fluid h350" style="border-bottom:1px solid #F8D8F8">
	<div class="container">
		<div class="row bitter" style="padding-top:15px;" id="">
			<div class="col-sm-8 col-sm-offset-2">
				<div class="talLft">
					Welcome to the HR PRIME SOLUTION. Here at HR PRIME SOLUTION, we are proudly offer a complete range of  HR services and solutions to our current and future customers alike and by representing only the best resource. We encourage you to explore this website and take full advantage of the information available. Please also feel free to contact us and we shall be only too delighted to help with any of your enquiries.
					<br/><br/>
					
					<div class="talCnt"><a href="<?php echo base_url().'contact'?>"><button class="btn btn-success">Start your Inquiry now</button></a></div>
					<br/><br/>
				</div>
			</div>
			<div class="col-sm-2"></div>
		</div>
	</div>
</div>
<div class="jumbotron">
	<div class="container">
		<div class="row bitter" style="">
			<div class="col-sm-6 talJst">
				<h2>Contact Us</h2>
				All HR Prime Solution services are designed in accordance with the climate and the needs of your company. Not only short-term gains, such as increased work motivation, but also long-term benefits related to the productivity of the company, is our target. Therefore, choose us as your partner is a right and profitable step for your business.<br/><br/>
				<div class="talCnt"><a href="<?php echo base_url().'/contact'?>" class="btn btn-info">Contact our team now</a></div>
			</div>
			<div class="col-sm-6 talJst">
				<h2>Find Career</h2> 
				Find your career with us now<br/><br/>
				<div class="talCnt"><a href="<?php echo base_url().'/career'?>" class="btn btn-info">Find career with us</a></div>
			</div>
		</div>
	</div>
</div>
-->

<div class="jumbotron">
	<div class="container">
		<div class="row bitter" style="min-height:250px;margin-top:45px">
			<div class="col-sm-12">
				<h2>Your one stop HR Solution.</h2><br/>
			</div>
			<div class="col-sm-6 talJst">
				Welcome to the HR PRIME SOLUTION. Here at HR PRIME SOLUTION, we are proudly offer a complete range of  HR services and solutions to our current and future customers alike and by representing only the best resource. We encourage you to explore this website and take full advantage of the information available. Please also feel free to contact us and we shall be only too delighted to help with any of your enquiries.<br/><br/><br/>
			</div>
			<div class="col-sm-2 col-sm-offset-2">
				<div class="talCnt"><a href="<?php echo base_url().'contact'?>" class="btn btn-info wdtFul">Contact our team now</a></div><br/>
				<div class="talCnt"><a href="<?php echo base_url().'career'?>" class="btn btn-warning wdtFul">Find career with us</a></div>
			</div>
			<div class="col-sm-2">
			</div>
		</div>		
	</div>		
</div>		

<div class="container">
	<div class="row bitter" style="min-height:250px;margin-top:45px">
		
		<div class="col-sm-12 talCnt">
			<h1>Our Services</h1><br/><br/>
		</div>
		
		<div class="col-sm-4">
			<div class="b talCnt">
				<a href="<?php echo $url_service.'recruitment-service'?>" class="none">
					<span class="fa-stack fa-5x">
					<i class="fa fa-circle fa-stack-2x"></i>
					<i class="fa fa-users fa-stack-1x text-primary"></i>
					</span><br/>
					<p class="">RECRUITMENT SERVICE</p>
				</a>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="b talCnt">
				<a href="<?php echo $url_service.'assesment-center'?>" class="none">	
					<span class="fa-stack fa-5x">
					<i class="fa fa-circle fa-stack-2x"></i>
					<i class="fa fa-book fa-stack-1x text-primary"></i>
					</span>
					<p class="">ASSESSMENT CENTER</p>
				</a>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="b talCnt">
				<a href="<?php echo $url_service.'hr-outsourcing'?>" class="none">	
					<span class="fa-stack fa-5x">
					<i class="fa fa-circle fa-stack-2x"></i>
					<i class="fa fa-male fa-stack-1x text-primary"></i>
					</span><br/>
					<p class="">HR OUTSOURCING</p>
				</a>
			</div>
		</div>
		<div class="col-sm-12"><br/><br/><br/></div>
		<div class="col-sm-4">
			<div class="b talCnt">
				<a href="<?php echo $url_service.'hr-consultancy'?>" class="none">	
					<span class="fa-stack fa-5x">
					<i class="fa fa-circle fa-stack-2x"></i>
					<i class="fa fa-comments-o fa-stack-1x text-primary"></i>
					</span><br/>
					<p class="">HR CONSULTANCY</p>
				</a>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="b talCnt">
				<a href="<?php echo $url_service.'training-development'?>" class="none">
					<span class="fa-stack fa-5x">
					<i class="fa fa-circle fa-stack-2x"></i>
					<i class="fa fa-clipboard fa-stack-1x text-primary"></i>
					</span>
					<p class="">TRAINING & DEVELOPMENT</p>
				</a>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="b talCnt">
				<a href="<?php echo $url_service.'executive-search'?>" class="none">
					<span class="fa-stack fa-5x">
					<i class="fa fa-circle fa-stack-2x"></i>
					<i class="fa fa-search-minus fa-stack-1x text-primary"></i>
					</span><br/>
					<p class="">EXECUTIVE SEARCH</p>
				</a>
			</div>
		</div>
		<div class="col-sm-12"><br/><br/><br/></div>
	</div>
</div>

<!--
<div class="container-fluid clrWht" style="border-bottom:1px solid #F8D8F8;">
	<div class="container">
		<div class="row bitter" style="padding-top:15px;" id="">
			<div class="col-sm-12">
				<div class="talCnt">
					<div class="talCnt" style="font-size:50px;color:#FFFFFF;">Hello world<br/></div><br/>
					
					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br/><br/>
					<button class="btn btn-success">Register here</button>
					<br/><br/><br/><br/>
				</div>
			</div>
		</div>
	</div>
</div>
-->
<!--
<div class="jumbotron" style="padding-bottom:0">
	<div class="container">
		<div class="rows">
			<div class="col-sm-12 talCnt">
				<span class="fa-stack fa-2x">
					<a href="">
						<i class="fa fa-circle fa-stack-2x"></i>
						<i class="fa fa-facebook fa-stack-1x clrWht"></i>
					</a>
				</span>
				&nbsp; &nbsp;
				<span class="fa-stack fa-2x">
					<a href="">
						<i class="fa fa-circle fa-stack-2x"></i>
						<i class="fa fa-twitter fa-stack-1x clrWht"></i>
					</a>
				</span>
				&nbsp; &nbsp;
				<span class="fa-stack fa-2x">
					<a href="">
						<i class="fa fa-circle fa-stack-2x"></i>
						<i class="fa fa-instagram fa-stack-1x clrWht"></i>
					</a>
				</span>
				
			</div>
		</div>
	</div>
</div>
-->

<!--
<div class="jumbotron" style="background-color:#F3F8FC">
	<div class="container">
		<div class="rows bitter" style="min-height:250px;margin-top:45px; ">
			<div class="col-sm-2">
			</div>
			<div class="col-sm-8 talCnt">
				Our Teams <br/><br/>
				<div class="col-sm-4">
					<div>-Rusdi Karsandi<br/>CTO</div>
				</div>
				<div class="col-sm-4">
					<div>-<br/>CEO</div>
				</div>
				<div class="col-sm-4">
					<div>-<br/>CFO</div>
				</div>
			</div>
			<div class="col-sm-2">
			</div>
		</div>
	</div>
</div>

<div class="jumbotron" style="background-color:#2E68B1">
	<div class="container">
		<div class="rows bitter" style="min-height:250px;margin-top:45px">
			<div class="col-sm-4">
			</div>
			<div class="col-sm-4 clrWht">
			<p class="f36">Our client</p><br/><br/>
			XXX
			</div>
			<div class="col-sm-4">
			</div>
		</div>
	</div>
</div>
-->

<!--
<div class="jumbotron" style="background-color:#FFFFFF">
	<div class="container">
		<div class="rows bitter" style="max-height:150px;margin-top:45px">
			<div class="col-sm-4">
			</div>
			<div class="col-sm-4 padMed bdrRed" style="border: 2px dotted #EB9316">
			<a href="" class="btn btn-warning">Sign Up Now</a> Try Jobtalento for free 30 days <br/>
			</div>
			<div class="col-sm-4">
			</div>
		</div>
	</div>
</div>
-->
	<!--
<div class="rows bitter" style="min-height:250px;margin-top:45px">
	<div class="col-sm-12">
		<div class="rows">
			<div class=" col-sm-6">
				<div class="b">
					<span class="fa-stack fa-4x">
					<i class="fa fa-circle fa-stack-2x"></i>
					<i class="fa fa-flask fa-stack-1x text-primary"></i>
					</span><br/>
				</div>
			</div>
			<div class="col-sm-4 col-sm-offset-2">		
				<div class="subindex">Setelah registrasi, kamu bisa mengikuti berbagai tes untuk mencari tahu kelebihan dan kekurangan personal kamu. Setelah registrasi, kamu bisa mengikuti berbagai tes untuk mencari tahu kelebihan dan kekurangan personal kamu. Setelah registrasi, kamu bisa mengikuti berbagai tes untuk mencari tahu kelebihan dan kekurangan personal kamu.</div>
			</div>
		</div>
	</div>
	
	<div class="col-sm-12">
		<div class="rows">
			<div class="col-sm-offset-2 col-sm-4">
				<p class="b">Isi tes di Job Talento</p>
				<div class="subindex">JobTalento hadir untuk membantu kamu menemukan passion dan berkarir di bidang yang kamu sukai. JobTalento hadir untuk membantu kamu menemukan passion dan berkarir di bidang yang kamu sukai. JobTalento hadir untuk membantu kamu menemukan passion dan berkarir di bidang yang kamu sukai.</div>
			</div>
			<div class="col-sm-4 col-sm-offset-2">		
				<div class="b">
					<span class="fa-stack fa-4x">
					<i class="fa fa-circle fa-stack-2x"></i>
					<i class="fa fa-tasks fa-stack-1x text-primary"></i>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>
-->
<!--
<div class="container">
	<div class="row bitter" style="min-height:250px;margin-top:45px">
		<div class="col-sm-4">
			<div class="b talCnt">
				<span class="fa-stack fa-4x">
				<i class="fa fa-circle fa-stack-2x"></i>
				<i class="fa fa-flask fa-stack-1x text-primary"></i>
				</span><br/>
				<p class="b">Registrasi cepat.</p>
			</div>
			<div class="subindex">Setelah registrasi, kamu bisa mengikuti berbagai tes untuk mencari tahu kelebihan dan kekurangan personal kamu.</div>
		</div>
		<div class="col-sm-4">
			<div class="b talCnt">
				<span class="fa-stack fa-4x">
				<i class="fa fa-circle fa-stack-2x"></i>
				<i class="fa fa-tasks fa-stack-1x text-primary"></i>
				</span>
				<p class="b">Isi tes di Job Talento</p>
			</div>
			<div class="subindex">JobTalento hadir untuk membantu kamu menemukan passion dan berkarir di bidang yang kamu sukai.</div>
		</div>
		<div class="col-sm-4">
			<div class="b talCnt">
				<span class="fa-stack fa-4x">
				<i class="fa fa-circle fa-stack-2x"></i>
				<i class="fa fa-dollar fa-stack-1x text-primary"></i>
				<i class="fa fa-ban fa-stack-2x text-danger"></i>
				</span><br/>
			
				<p class="b upper">Gratis dan mudah</p>
			</div>
			<div class="subindex">Cukup daftar sebagai member dan dapatkan hasil tes kamu untuk segera memulai karir impian kamu yang baru</div>
		</div>
	</div>
</div>


-->
