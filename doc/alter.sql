/* 8 maret 2016 */
CREATE TABLE hr_image(
image_id int NOT NULL AUTO_INCREMENT ,
image_name text,
image_size varchar(255) NULL,
creator_id varchar(10) NULL,
creator_ip varchar(20) NULL,
creator_date datetime NULL,
PRIMARY KEY(image_id)
)

/* 7 maret 2016 */
ALTER TABLE config ADD company_description text NULL AFTER site_description

/* 4 Maret */
ALTER TABLE page ADD slug varchar(250) NULL AFTER name

CREATE TABLE `hr_member` (
	`member_id` INT(11) NOT NULL AUTO_INCREMENT,
	`first_name` VARCHAR(80) NULL DEFAULT NULL,
	`last_name` VARCHAR(80) NULL DEFAULT NULL,
	`nama_panggilan` VARCHAR(180) NULL DEFAULT NULL,
	`email` VARCHAR(200) NULL DEFAULT NULL,
	`thepassword` VARCHAR(200) NULL DEFAULT NULL,
	`address` VARCHAR(250) NULL DEFAULT NULL,
	`gender` TINYINT(4) NULL DEFAULT '1' COMMENT '1 MALE , 2 FEMALE',
	`tanggal_lahir` DATE NULL DEFAULT NULL,
	`pekerjaan` VARCHAR(150) NULL DEFAULT NULL,
	`token` VARCHAR(100) NULL DEFAULT NULL,
	`forgot_pass_expired` DATETIME NULL DEFAULT NULL,
	`active_code` VARCHAR(50) NULL DEFAULT NULL,
	`is_active` TINYINT(4) NULL DEFAULT '0',
	`is_subscribe` TINYINT(4) NULL DEFAULT '0',
	`is_deleted` TINYINT(4) NULL DEFAULT '0',
	`creator_date` DATETIME NULL DEFAULT NULL,
	`editor_date` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`member_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=10;


CREATE TABLE config (
site_name text NULL,
site_description text NULL,
google_analytic text NULL,
themes text NULL
);

CREATE TABLE menu (
menu_id mediumint NOT NULL AUTO_INCREMENT,
name text NULL,
link text NULL,
the_order smallint DEFAULT 1,
is_show tinyint DEFAULT 0,
creator_id INT(11) NULL DEFAULT NULL,
creator_ip VARCHAR(15) NULL DEFAULT NULL,
creator_date DATETIME NULL DEFAULT NULL,
editor_id INT(11) NULL DEFAULT NULL,
editor_ip VARCHAR(15) NULL DEFAULT NULL,
editor_date DATETIME NULL DEFAULT NULL,
PRIMARY KEY(menu_id)
)

-- 26 oct
ALTER TABLE `wal_treasure`
	CHANGE COLUMN `deposito_date` `reminder_date` DATE NULL DEFAULT NULL AFTER `amount`;

-- 14 sept
CREATE TABLE job_notification(
notification_id bigint NOT NULL AUTO_INCREMENT,
company_id int NULL DEFAULT NULL,
participant_id int NULL DEFAULT NULL,
subject varchar(250) NULL,
message text DEFAULT NULL,
is_read tinyint DEFAULT 0,
is_delete tinyint DEFAULT 0,
`creator_id` INT(11) NULL DEFAULT NULL,
	`creator_ip` VARCHAR(15) NULL DEFAULT NULL,
	`creator_date` DATETIME NULL DEFAULT NULL,
	`editor_id` INT(11) NULL DEFAULT NULL,
	`editor_ip` VARCHAR(15) NULL DEFAULT NULL,
	`editor_date` DATETIME NULL DEFAULT NULL,
PRIMARY KEY(notification_id)
)engine=Innodb

-- 13 sept
ALTER TABLE job_participant ADD is_delete tinyint DEFAULT 0 AFTER token;
ALTER TABLE job_test ADD test_code varchar(15) DEFAULT NULL AFTER title;

ALTER TABLE `job_test_detail_option`
	CHANGE COLUMN `point_count` `point_count` VARCHAR(50) NULL DEFAULT NULL COMMENT 'bobot angka' AFTER `answer_detail`;
	

-- 10 sept
CREATE TABLE job_coupon(
coupon_id int NOT NULL AUTO_INCREMENT,
coupon_code varchar(50) NULL,
quota smallint DEFAULT 1,
start_date datetime DEFAULT NULL,
end_date datetime DEFAULT NULL,
is_active tinyint DEFAULT 0,
`creator_id` INT(11) NULL DEFAULT NULL,
`creator_ip` VARCHAR(15) NULL DEFAULT NULL,
`creator_date` DATETIME NULL DEFAULT NULL,
`editor_id` INT(11) NULL DEFAULT NULL,
`editor_ip` VARCHAR(15) NULL DEFAULT NULL,
`editor_date` DATETIME NULL DEFAULT NULL,
PRIMARY KEY(coupon_id)
)ENGINE=InnoDb;

CREATE TABLE job_coupon_detail(
coupon_detail_id int NOT NULL AUTO_INCREMENT,
coupon_id int NOT NULL,
company_id int NOT NULL,
is_used tinyint DEFAULT 0,
`creator_id` INT(11) NULL DEFAULT NULL,
`creator_ip` VARCHAR(15) NULL DEFAULT NULL,
`creator_date` DATETIME NULL DEFAULT NULL,
`editor_id` INT(11) NULL DEFAULT NULL,
`editor_ip` VARCHAR(15) NULL DEFAULT NULL,
`editor_date` DATETIME NULL DEFAULT NULL,
PRIMARY KEY(coupon_detail_id)
)ENGINE=InnoDb;

ALTER TABLE job_participant ADD job_position varchar(120) DEFAULT NULL AFTER thepassword;

-- 8 sept
ALTER TABLE job_participant ADD expired_test_date datetime DEFAULT NULL AFTER gender;
UPDATE job_participant SET token = md5(email);
UPDATE job_participant SET expired_test_date = now() + INTERVAL 1 MONTH;

-- 06 sept

ALTER TABLE job_order
ADD confirm_payment_type tinyint DEFAULT NULL AFTER notes,
ADD confirm_payment_date datetime DEFAULT NULL AFTER notes,
ADD confirm_payment_amount int DEFAULT NULL AFTER notes,
ADD confirm_payment_notes varchar(255) DEFAULT NULL AFTER notes;

-- 04 sept 2015
ALTER TABLE job_test ADD is_company tinyint DEFAULT 1 AFTER is_public;

ALTER TABLE `job_participant_type_test`
	ADD UNIQUE INDEX `job_unique1` (`participant_id`, `test_id`);

-- ALTER 25 AUGUST 2015 

ALTER TABLE job_participant_type_test ADD is_active tinyint NULL DEFAULT 0 AFTER test_id

ALTER TABLE job_test ADD is_company tinyint NULL DEFAULT 0 AFTER is_public 

----------------------------
----------------------------
----------------------------

INSERT INTO `job_test_detail_option` (`test_detail_id`, `answer_detail`, `point_count`, `creator_id`, `creator_ip`, `creator_date`, `editor_id`, `editor_ip`, `editor_date`) VALUES
(41, 'Praktis', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(41, 'Spontan', 2, NULL, NULL, NULL, NULL, NULL, NULL),
(41, 'Ketat pada waktu', 3, NULL, NULL, NULL, NULL, NULL, NULL),
(41, 'Pemalu', 4, NULL, NULL, NULL, NULL, NULL, NULL),

(42, 'Rapi / Teratur', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(42, 'Sopan / hormat', 2, NULL, NULL, NULL, NULL, NULL, NULL),
(42, 'Suka bicara terus terang', 3, NULL, NULL, NULL, NULL, NULL, NULL),
(42, 'Optimis', 4, NULL, NULL, NULL, NULL, NULL, NULL),

(43, 'Ramah tamah', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(43, 'Jujur', 2, NULL, NULL, NULL, NULL, NULL, NULL),
(43, 'Suka senda gurau', 3, NULL, NULL, NULL, NULL, NULL, NULL),
(43, 'Optimis', 4, NULL, NULL, NULL, NULL, NULL, NULL),

(44, 'Berani / tidak penakut', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(44, 'Menyukai kenikmatan', 2, NULL, NULL, NULL, NULL, NULL, NULL),
(44, 'Diplomatis / berhati hati', 3, NULL, NULL, NULL, NULL, NULL, NULL),
(44, 'Terperinci', 4, NULL, NULL, NULL, NULL, NULL, NULL),

(45, 'Penggembira', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(45, 'Konsisten / tidak mudah berubah', 2, NULL, NULL, NULL, NULL, NULL, NULL),
(45, 'Berbudaya / terpelajar', 3, NULL, NULL, NULL, NULL, NULL, NULL),
(45, 'Percaya diri', 4, NULL, NULL, NULL, NULL, NULL, NULL),

(46, 'Idealis', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(46, 'Mandiri', 2, NULL, NULL, NULL, NULL, NULL, NULL),
(46, 'Tidak suka menentang', 3, NULL, NULL, NULL, NULL, NULL, NULL),
(46, 'Suka memberi ilham / inspirasi', 4, NULL, NULL, NULL, NULL, NULL, NULL),

(47, 'Lincah / suka membuka diri', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(47, 'Mampu memutuskan', 2, NULL, NULL, NULL, NULL, NULL, NULL),
(47, 'Sedikit humor', 3, NULL, NULL, NULL, NULL, NULL, NULL),
(47, 'Tekun / ulet', 4, NULL, NULL, NULL, NULL, NULL, NULL),

(48, 'Perantara / penengah', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(48, 'Gemar musik lembut', 2, NULL, NULL, NULL, NULL, NULL, NULL),
(48, 'Cepat bertindak', 3, NULL, NULL, NULL, NULL, NULL, NULL),
(48, 'Mudah berbaur / bergaul', 4, NULL, NULL, NULL, NULL, NULL, NULL),

(49, 'Senang berpikir', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(49, 'Suka ngotot / Kuat bertahan', 2, NULL, NULL, NULL, NULL, NULL, NULL),
(49, 'Senang bicara', 3, NULL, NULL, NULL, NULL, NULL, NULL),
(49, 'Bersikap toleran', 4, NULL, NULL, NULL, NULL, NULL, NULL),

(50, 'Pendengar yang baik', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(50, 'Setia / tidak gampang berubah', 2, NULL, NULL, NULL, NULL, NULL, NULL),
(50, 'Senang membimbing', 3, NULL, NULL, NULL, NULL, NULL, NULL),
(50, 'Lincah / bersemangat', 4, NULL, NULL, NULL, NULL, NULL, NULL),

(51, 'Mudah menerima saran', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(51, 'Suka memimpin', 2, NULL, NULL, NULL, NULL, NULL, NULL),
(51, 'Berpikir matematis', 3, NULL, NULL, NULL, NULL, NULL, NULL),
(51, 'Lucu / humoris', 4, NULL, NULL, NULL, NULL, NULL, NULL),

(52, 'Perfeksionis / ingin sempurna', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(52, 'Suka mengijinkan / memperbolehkan', 2, NULL, NULL, NULL, NULL, NULL, NULL),
(52, 'Produktif / menghasilkan', 3, NULL, NULL, NULL, NULL, NULL, NULL),
(52, 'Terkenal luas / populer', 4, NULL, NULL, NULL, NULL, NULL, NULL),

(53, 'Bersemangat / gembira', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(53, 'Berani / tidak gampang takut', 2, NULL, NULL, NULL, NULL, NULL, NULL),
(53, 'Berkelakuan tenang / kalem', 3, NULL, NULL, NULL, NULL, NULL, NULL),
(53, 'Berpendirian tetap', 4, NULL, NULL, NULL, NULL, NULL, NULL),

-- weakness here
(54, 'Bicara meriah / ramai', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(54, 'Bersikap Seperti Boss', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(54, 'Suka malu-malu / segan', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(54, 'Tanpa ekspresi / datar', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(55, 'Kurang disiplin', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(55, 'Tidak simpatik', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(55, 'Kurang antusias / tidak bergairah', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(55, 'Tidak mudah memaafkan', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(56, 'Pendiam', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(56, 'Gampang tersinggung', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(56, 'Suka melawan / membantah', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(56, 'Sering mengulang-ulang', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(57, 'Rewel / Ngomel yang tidak perlu', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(57, 'Suka takut / kuatir', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(57, 'Pelupa', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(57, 'Terus terang / blak-blakan', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(58, 'Tidak sabaran', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(58, 'Tidak merasa aman / mantap', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(58, 'Sering bimbang memutuskan', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(58, 'Suka menyela / memotong bicara orang lain', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(59, 'Kurang terkenal / tidak populer', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 'Tidak suka melibatkan diri', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 'Sulit diramalkan / sukar diduga', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 'Tidak gampang terpengaruh', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(60, 'Keras kepala', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(60, 'Serampangan / sembrono', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(60, 'Sulit mengikhlaskan / merelakan', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(60, 'Bimbang / ragu-ragu', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(61, 'Sederhana', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(61, 'Pesimis', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(61, 'Tinggi hati / gengsi', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(61, 'Suka membiarkan', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(62, 'Gampang marah', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 'Tanpa arah / tujuan', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 'Suka berdebat / berbantah', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 'Memisahkan diri / lebih  senang sendiri', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(63, 'Polos / ceplas ceplos', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(63, 'Selalu berpikir jelek / sukar dipercaya', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(63, 'Kasar / suka menyerang', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(63, 'Masa bodo / tidak perduli', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(64, 'Kuatir / cemas', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(64, 'Menarik diri / pendiam', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(64, 'Bekerja keras / giat', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(64, 'Ingin pujian / penghargaan', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(65, 'Terlalu peka / sensitif', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 'Tidak bijaksana / tidak taktis', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 'Malu / segan', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 'Banyak bicara / monopoli percakapan', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(66, 'Banyak ragu-ragu / sangsi', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(66, 'Tidak teratur / berantakan', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(66, 'Menguasai secara ketat', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(66, 'Murung / patah semangat', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(67, 'Gampang berubah / tidak berpendirian tetap', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(67, 'Tertutup / sulit membuka diri', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(67, 'Sukar bertoleransi', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(67, 'lalai / acuh tak acuh', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(68, 'Tidak rapi / berantakan', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(68, 'Murung', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(68, 'Menggerutu / mengomel', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(68, 'Manipulatif / memanfaatkan', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(69, 'Lamban / amat berhati-hati', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(69, 'Bandel', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(69, 'Berlagak / pamer', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(69, 'Ragu-ragu / selalu curiga', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(70, 'Penyendiri', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(70, 'Suka memerintah / menggurui', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(70, 'Malas / berat langkah', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(70, 'Suka membual', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(71, 'Enggan / sering ogah', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(71, 'Sering berprasangka', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(71, 'Cepat marah / emosi', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(71, 'Bingung / sulit berkonsentrasi', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(72, 'Suka membalas dendam', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(72, 'Sering gelisah / Resah', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(72, 'Perlu dorongan / bimbingan', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(72, 'Gegabah / terburu-buru / kurang berpikir panjang', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(73, 'Mudah berkompromi / mengalah', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(73, 'Sering mencela / mengkritik', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(73, 'Senang berbohong / mengakali', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(73, 'Suka berubah pikiran', 8, NULL, NULL, NULL, NULL, NULL, NULL)
;
----------------------------------------------------------------------------------------
(74, '', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(74, '', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(74, '', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(74, '', 8, NULL, NULL, NULL, NULL, NULL, NULL),

-----------------------------------------------------------------

INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES 
 -- (5, 'Pendiam, tidak banyak bicara;Berjuang mencapai hasil;Mudah bergaul dengan orang baru;Berusaha menyenangkan orang', 1, '::1', '2015-09-16 15:08:21');
-- (5, 'Mengajak, Pemberi semangat;\r\nMengutamakan kesempurnaan;\r\nMengikuti pemimpin;\r\nKeberhasilan adalah tujuan', 1, '::1', '2015-09-16 15:08:21');
-- dua diatas udah diisi
(5, 'Mudah kecewa, patah semangat;Senang membantu sesama;Suka bercerita tentang diri sendiri;Memihak kepada oposisi', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Mengatur waktu secara efisien;Buru-buru ingin cepat selesai;Pandai bergaul, banyak teman;Mengerjakan sampai selesai', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Menghindari konflik;Suka membuat janji;Bekerja Runtut, sesuai aturan;Berani menghadapi sesuatu', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Mau mengalah dengan sesama;Peraturan itu membosankan;Suka memaksa;Memiliki standar tinggi', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Bersemangat aktif;Bekerja cepat, ingin menang;Menghidari pertengkaran;Menyendiri jika stress', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Takut mengambil keputusan;Penghargaan akan kemenangan;Tenang, tidak terburu-buru;Bersahabat, dikenal banyak orang', 1, '::1', '2015-09-16 15:08:21'),

-- 2 nih
(5, 'Bertanggung jawab akan tugas;Mudah mengekspresikan sesuatu;Mudah ditebak, konsisten;Selalu hati-hati, waspada', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Tidak mudah menyerah;Menjadi anggota dari kelompok;Periang dan selalu ceria;Semua teratur rapih', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Senang Mengarahkan, memimpin;Mengendalikan diri;Dapat membujuk orang lain;Cara berpikir sistematis, logis', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Tulus, tidak berprasangka buruk;Ketawa lepas, tidak ditahan;Berani tegas;Tenang, hati-hati, serius', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Kemauan keras;suka dengan hal baru;menolak perubahan mendadak;mempersiapkan masa depan', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Penyemangat yang baik;sabar mendengarkan pembicaraan;peraturan itu untuk keadilan;Mandiri,tidak tergantung orang lain', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Suka menyenangkan orang lain;Suka mengambil keputusan;Bersemangat dan Optimis;Mengutamakan fakta, data', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Ramah, lembut;Berpikir positif, bersemangat;suka mengambil resiko;Bekerja sesuai perintah', 1, '::1', '2015-09-16 15:08:21'),

--3
(5, 'Hati-hati, kontrol diri ketat;Berkata sesuai pikiran;Tidak mudah cemas akan sesuatu;jika belanja sesuai keinginan', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Ramah, mudah berteman;Cepat bosan dengan hal-hal rutin;Suka berubah-ubah;Menginginkan kepastian', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Mengalah, menghindari konflik;Hal-hal kecil jadi perhatian;Berubah pada saat-saat terakhir;Suka tantangan baru;', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Pendiam, tidak suka bicara;Riang, suka bicara;Cepat merasa puas;Cepat memutuskan, tegas', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Mampu sabar dalam menunggu;Menginginkan petunjuk yang jelas;Suka bercanda;Jika ada keinginan harus dipenuhi', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Peraturan itu menghambat;Suka menganalisa sampai detail;Unik, beda dengan yang lain;Bisa diharapkan bantuannya;', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Berani mengambil Resiko;Menyenangkan, suka membantu;Mudah mengungkapkan perasaan;Rendah hati, sederhana;', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Mengutamakan hasil;Menginginkan akurasi, ketepatan;Betah berbicara lama;Suka berkelompok, bersama-sama', 1, '::1', '2015-09-16 15:08:21');

-----------------

Mengatur waktu secara efisien;
Buru-buru ingin cepat selesai;
Pandai bergaul, banyak teman;
Mengerjakan sampai selesai

Menghindari konflik;
Suka membuat janji;
Bekerja Runtut, sesuai aturan;
Berani menghadapi sesuatu

Mau mengalah dengan sesama;
Peraturan itu membosankan;
Suka memaksa;
Memiliki standar tinggi

Bersemangat aktif;
Bekerja cepat, ingin menang;
Menghidari pertengkaran;
Menyendiri jika stress

Takut mengambil keputusan;
Penghargaan akan kemenangan;
Tenang, tidak terburu-buru;
Bersahabat, dikenal banyak orang

-----------------------------

Bertanggung jawab akan tugas;
Mudah mengekspresikan sesuatu;
Mudah ditebak, konsisten;
Selalu hati-hati, waspada

Tidak mudah menyerah;
Menjadi anggota dari kelompok;
Periang dan selalu ceria;
Semua teratur rapih

Senang Mengarahkan, memimpin;
Mengendalikan diri;
Dapat membujuk orang lain;
Cara berpikir sistematis, logis

Tulus, tidak berprasangka buruk;
Ketawa lepas, tidak ditahan;
Berani tegas;
Tenang, hati-hati, serius

Kemauan keras;
suka dengan hal baru;
menolak perubahan mendadak;
mempersiapkan masa depan

Penyemangat yang baik;
sabar mendengarkan pembicaraan;
peraturan itu untuk keadilan;
Mandiri,tidak tergantung orang lain

Suka menyenangkan orang lain;
Suka mengambil keputusan;
Bersemangat dan Optimis;
Mengutamakan fakta, data

Ramah, lembut;
Berpikir positif, bersemangat;
suka mengambil resiko;
Bekerja sesuai perintah

----------------------------

Hati-hati, kontrol diri ketat;
Berkata sesuai pikiran;
Tidak mudah cemas akan sesuat;
jika belanja sesuai keinginan

Ramah, mudah berteman;
Cepat bosan dengan hal-hal rutin;
Suka berubah-ubah;
Menginginkan kepastian

Mengalah, menghindari konflik;
Hal-hal kecil jadi perhatian;
Berubah pada saat-saat terakhir;
Suka tantangan baru;

Pendiam, tidak suka bicara;
Riang, suka bicara;
Cepat merasa puas;
Cepat memutuskan, tegas

Mampu sabar dalam menunggu;
Menginginkan petunjuk yang jelas;
Suka bercanda;
Jika ada keinginan harus dipenuhi

Peraturan itu menghambat;
Suka menganalisa sampai detail;
Unik, beda dengan yang lain;
Bisa diharapkan bantuannya;

Berani mengambil Resiko;
Menyenangkan, suka membantu;
Mudah mengungkapkan perasaan;
Rendah hati, sederhana;

Mengutamakan hasil;
Menginginkan akurasi, ketepatan;
Betah berbicara lama;
Suka berkelompok, bersama-sama

ni yang uda rata yang
-----------------------------------------------------------------------------------------------

-----------------

Mengatur waktu secara efisien;
Buru-buru ingin cepat selesai;
Pandai bergaul, banyak teman;
Mengerjakan sampai selesai

Menghindari konflik;
Suka membuat janji;
Bekerja Runtut, sesuai aturan;
Berani menghadapi sesuatu

Mau mengalah dengan sesama;
Peraturan itu membosankan;
Suka memaksa;
Memiliki standar tinggi

Bersemangat aktif;
Bekerja cepat, ingin menang;
Menghidari pertengkaran;
Menyendiri jika stress

Takut mengambil keputusan;
Penghargaan akan kemenangan;
Tenang, tidak terburu-buru;
Bersahabat, dikenal banyak orang

-----------------------------
2
Bertanggung jawab akan tugas;Mudah mengekspresikan sesuatu;Mudah ditebak, konsisten;Selalu hati-hati, waspada

Tidak mudah menyerah;Menjadi anggota dari kelompok;Periang dan selalu ceria;Semua teratur rapih

Senang Mengarahkan, memimpin;Mengendalikan diri;Dapat membujuk orang lain;Cara berpikir sistematis, logis

Tulus, tidak berprasangka buruk;Ketawa lepas, tidak ditahan;Berani tegas;Tenang, hati-hati, serius

Kemauan keras;suka dengan hal baru;menolak perubahan mendadak;mempersiapkan masa depan

Penyemangat yang baik;sabar mendengarkan pembicaraan;peraturan itu untuk keadilan;
Mandiri,tidak tergantung orang lain

Suka menyenangkan orang lain;Suka mengambil keputusan;Bersemangat dan Optimis;Mengutamakan fakta, data

Ramah, lembut;Berpikir positif, bersemangat;suka mengambil resiko;Bekerja sesuai perintah

----------------------------
3
Hati-hati, kontrol diri ketat;Berkata sesuai pikiran;Tidak mudah cemas akan sesuatu;jika belanja sesuai keinginan

Ramah, mudah berteman;Cepat bosan dengan hal-hal rutin;Suka berubah-ubah;Menginginkan kepastian

Mengalah, menghindari konflik;Hal-hal kecil jadi perhatian;Berubah pada saat-saat terakhir;Suka tantangan baru;

Pendiam, tidak suka bicara;Riang, suka bicara;Cepat merasa puas;Cepat memutuskan, tegas

Mampu sabar dalam menunggu;Menginginkan petunjuk yang jelas;Suka bercanda;Jika ada keinginan harus dipenuhi

Peraturan itu menghambat;Suka menganalisa sampai detail;Unik, beda dengan yang lain;Bisa diharapkan bantuannya;

Berani mengambil Resiko;Menyenangkan, suka membantu;Mudah mengungkapkan perasaan;Rendah hati, sederhana;

Mengutamakan hasil;Menginginkan akurasi, ketepatan;Betah berbicara lama;Suka berkelompok, bersama-sama

INSERT INTO `job`.`job_test` (`title`, `test_code`,is_publish,is_public,is_company) VALUES ('Test DISC 2', 'disc-2',1,1,1);
