<?php 

/* start paging */
$page = 1;
if (isset($_GET['page'])) $page = $_GET['page'];
if (OFFSET) $offset = OFFSET;
//$offset = 30;
$param['offset'] = $offset;

/* end paging */

$q = '';
if ($this->input->get("q")) $q = mysql_real_escape_string(urldecode($this->input->get("q")));
$str = "";

if (isset($q))
{
	$param = array('Keyword' => $q);
	$data = $this->article_model->get_list($param);
	$objListArticle = $data['data'];
	if( !empty($objListArticle) ){
		/* AUTO REDIRECT TO ARTICLE IF ONLY 1 RESULT */
		// if( count($objListArticle) == 1 ){
			// foreach($objListArticle as $obj){
				// redirect( friendlyUrl("/showarticle.php?aid=".$obj['ArticleID()."&title=".$obj['Title()) );
			// }
		// }
		$i = 1;

		$str.= '<div class="bg-info padLrg"><i class="fa fa-info"></i>&nbsp; Ditemukan '.$data['total_rows'].' '.$q.' artikel terkait keyword <span class="b">'. $q .' </span> </div>';
		foreach ($objListArticle as $obj) {
			if ($obj['IsPublish']) {
				/* COLORING BORDER ON TOP */
				$bgcolor = "";
				if($i > 1)$bgcolor = " bdrTopGry";
				$friendlyUrl = base_url()."showarticle/".$obj['ArticleID']."/".$obj['Slug'];
				
				if (is_internal()) {
					//OFFLINE
					$imageUrl = base_url()."asset/plugin/autothumbnail/index.php?src=staging/asset/images/article/".$obj['Image']."&width=340";
				} else {
					// ONLINE
					$imageUrl = base_url()."asset/plugin/autothumbnail/index.php?src=asset/images/article/".$obj['Image']."&width=340";
					//http://www.grevia.com/plugin/autothumbnail/index.php?src=data/article/2013.jpg&width=500
				}
				$str.= "
				<div class='$bgcolor'><br/>
				<div class='col-md-5'><a href='".$friendlyUrl."'><img data-original='".$imageUrl."' class='lazy img-responsive wdtFul' style='min-height:250px'/></a><br/>
				</div>
				<div class='col-md-7 padLrg'>
				<a href='".$friendlyUrl."' class='b clrBlk f18'>".$obj['Title']."</a><br/><br/>
				<span class='fntBld fntMed'>".$obj['ShortDescription']."</span>
				</div></div><br/>
				<div class='clearfix'></div>
				";
				$i++;
			}
		}
	}else{
		$str.= "Tag artikel tidak ditemukan.";
	}
}else{
	$str.= "Tag artikel tidak ditemukan.";
}
?>
<div class="col-xs-9">
<?php
echo $str;
?>
</div>
<div class="col-xs-3">
	<?php echo $SIDEBAR_RIGHT; ?>
</div>
<!--
<div class="col-xs-3 " align="right">
<blockquote class='bg-primary'>
ini sidebar nih
<div style="background:#fff;color:#000">-- Anonym</div>
</blockquote>
</div>
-->
