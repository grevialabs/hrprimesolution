<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter Curl Class
 *
 * Work with remote servers via cURL much easier than using the native PHP bindings.
 *
 * @package        	Native
 * @subpackage    	Libraries
 * @category    	Libraries
 * @author        	Rusdi
 * @license         
 * @link			https://stackoverflow.com/questions/26039848/php-asynchronous-curl-with-callback
 */
class Curl_async
{
    public $_ci;
    public $handle;

    public function __construct()
    {
        $this->_ci = & get_instance();
		$this->handle = curl_multi_init();
    }

    public function process($urls, $callback = NULL)
    {
        if (! is_array($urls)) {
			$temp = NULL;
			$temp[] = $urls;
			$urls = NULL;
			$urls = $temp;
		}
		
		foreach ($urls as $url) {
            $ch = curl_init($url);
            curl_setopt_array($ch, array(CURLOPT_RETURNTRANSFER => TRUE));
            curl_multi_add_handle($this->handle, $ch);
        }

        do {
            $mrc = curl_multi_exec($this->handle, $active);

            if ($state = curl_multi_info_read($this->handle)) {
                //print_r($state);
                $info = curl_getinfo($state['handle']);
                //print_r($info);
                // $callback(curl_multi_getcontent($state['handle']), $info);
                curl_multi_remove_handle($this->handle, $state['handle']);
            }

            usleep(10000); // stop wasting CPU cycles and rest for a couple ms

        } while ($mrc == CURLM_CALL_MULTI_PERFORM || $active);

    }

    public function __destruct()
    {
        curl_multi_close($this->handle);
    }
}
