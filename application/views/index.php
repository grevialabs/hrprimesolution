<?php
global $PAGE, $PAGE_TITLE, $BREADCRUMB , $PAGE_HEADER;
 
if (!isset($PAGE)) $PAGE = DEFAULT_PAGE_TITLE . ' - '.DEFAULT_PAGE_DESCRIPTION;
if (!isset($PAGE_TITLE)) $PAGE_TITLE = $PAGE;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<!-- hrprimesolution.com google verification-->
	<meta name="google-site-verification" content="QJKnDvxhAulKHUSNT95M6F6BisRZa15YkLtxdqfmdzU" />
	<title><?php echo $PAGE_TITLE ?></title>
	<link rel="shortcut icon" href="<?php echo base_url()?>favicon.ico" type="image/x-icon">
	<link rel="icon" href="<?php echo base_url()?>favicon.ico" type="image/x-icon">
	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<!-- Latest compiled and minified CSS 
		<link rel="stylesheet" href="http://localhost/ci/assets/css/bootstrap.min.css">
		-->
	
	<?php if (!is_internal()) {?>
	<!-- Google tag (gtag.js) apply new tag on 15 maret 2023 -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-XT55NH15Y3"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'G-XT55NH15Y3');
	</script>
	<?php } ?>

	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap.css"/>
		
	<!-- Include required JS files -->
	<link rel='stylesheet' href='<?php echo base_url(); ?>asset/css/ui-lightness/jquery-ui-1.10.4.css' />

	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-theme.css"/>
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-theme.css.map"/>
	<!--<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-theme.min.css"/>-->
	
    <link href="<?php echo base_url()?>asset/css/font-awesome.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-social.css"/>
    <link rel="stylesheet" href="<?php echo base_url()?>asset/css/style.css"/>
    <link rel="stylesheet" href="<?php echo base_url()?>asset/css/animate.css"/>

	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/jquery.1.11.2.min.js"></script>
	<script type="text/javascript" src='<?php echo base_url(); ?>asset/js/jquery-ui-1.10.4.js'></script>
	<script type="text/javascript" src='<?php echo base_url(); ?>asset/js/jquery.validate.js'></script>
	<script type="text/javascript" src='<?php echo base_url(); ?>asset/js/ui/jquery.ui.core.js'></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/bootstrap.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/scrollReveal.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/jquery.backstretch.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/lazyload/jquery.lazyload.js?v=1.9.1"></script>
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/colorbox.css" />
	<script src="<?php echo base_url()?>asset/js/jquery.colorbox.js"></script>
	<script src="<?php echo base_url()?>asset/js/wow.min.js"></script>
	
	<script type="text/javascript">
	$(document).ready(function(){	
		$(function() {	
			//$.backstretch("http://dl.dropbox.com/u/515046/www/garfield-interior.jpg");
			$("img.lazy").lazyload({ effect: "fadeIn" });
			
			$('[data-toggle="popover"]').popover();
			//$('.popover').popover(options);
			$( ".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true,
				showAnim: "slideDown",
				yearRange: '1950:+10' 
			});
		});
	});
	window.sr = new scrollReveal();
	new WOW().init();
	</script>
	
</head>
<?php 
global $fetch_class, $fetch_method;
$fetch_class = $this->router->fetch_class();
$fetch_method = $this->router->fetch_method();
function show_active_menu($menu_title) 
{
	global $fetch_class, $fetch_method;
	$str = '';
	if ($fetch_class == 'modular') 
	{
		if ($fetch_method == $menu_title) 
		{
			$str = 'btn btn-info';
			$str = '<span class="sr-only">(current)</span>';
		}
	}
	return $str;
}

?>
<body>
<?php include_once 'navheader.php';?>

<div class="container">	
	<div class="row">
		<div class="col-md-12" style="min-height:500px"><br/>
			<?php if (isset($BREADCRUMB)) echo $BREADCRUMB.'' ?>
			<?php if (isset($PAGE_HEADER)) echo '<h2>'.$PAGE_HEADER.'</h2>' ?>
			<!--<h1>Selamat datang</h1>
			<div class="alert alert-warning"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
  <span class="sr-only">Error:</span>&nbsp;Email / Password tidak valid.</div>-->
			<?php echo $CONTENT; ?>
		</div>
	</div>
</div>

<?php include_once 'navfooter.php';?>


</body>
</html>