<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER;
// echo __DIR__;die;
$PAGE_HEADER = NULL;
$PAGE = $MODULE = "Konten";
$bread['member'] = 'Member';
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);
$PAGE_TITLE = $PAGE.' - '.DEFAULT_PAGE_TITLE;

$do = $page_id = $list_image = $obj_image = NULL;

if (isset($_GET['do'])) $do = $_GET['do'];
if (isset($_GET['page_id'])) $page_id = $_GET['page_id'];

// SAVE
if (isset($_POST['btnSave']) && isset($_POST['name']) && isset($_POST['content'])) {
	
	$param = array(
		'name' => $_POST['name'],
		'content' => $_POST['content'],
		// 'gender' => $_POST['gender'],
	);
	
	$save = $this->page_model->save($param);
	($save)?$message['message'] = MESSAGE::SAVE : $message['message'] = MESSAGE::ERROR;
	$message['message'] = getMessage($message['message']);
}

// UPDATE
if (isset($_POST['btnUpdate']) && isset($_POST['name']) && isset($_POST['content'])) {
	
	// $content = preg_replace('/(<p>|<\/p>)/i','',$_POST['content']);
	$param = array(
		'name' => $_POST['name'],
		'slug' => $_POST['slug'],
		'content' => $_POST['content'],
	);
	
	$update = $this->page_model->update($page_id, $param);
	($update)?$message['message'] = MESSAGE::UPDATE : $message['message'] = MESSAGE::ERROR;
	$message['message'] = getMessage($message['message']);
}

if (($do == 'edit' || $do == 'view') && isset($page_id))
{
	$obj_image = $this->page_model->get( array('page_id' => $page_id) );
}
else
{
	$list_image = $this->image_model->get_list();
	$list_image = $list_image['data'];
}
?>

<?php


$dir = __DIR__;
$dir = str_replace('application'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'member','',$dir);
$target_dir = $dir."asset".DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."upload".DIRECTORY_SEPARATOR;

// $image_src = base_url().'asset/images/upload/';
// debug($target_dir);
if ($_POST)
{
	// debug(__DIR__);
	// $dir = __DIR__;
	// $dir = str_replace('application/views/member','',$dir);
	// $target_dir = $dir."asset/images/upload/";
	
	/*
	 | REPLACE ALL NON ALFANUMERIC BECOME -
	*/
	$image_name = $_FILES["fileToUpload"]["name"];
	$image_name = basename($_FILES["fileToUpload"]["name"]);
	$image_name = preg_replace('/[^(\w|\.)]/i','-',strtolower($image_name));
	$image_name = time().'-'.$image_name;
	
	$target_file = $target_dir .$image_name;
	$image_src = base_url().'asset/images/upload/'.$image_name;
	$is_validimage = 1;
	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
	
	// Check if image file is a actual image or fake image
	if(isset($_POST["submit"])) {
		$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
		if($check !== false) {
			$message['message'] = "File is an image - " . $check["mime"] . ".";
			$is_validimage = 1;
		} else {
			$message['message'] = "File is not an image.";
			$is_validimage = 0;
		}
	}
	// debug($target_file);
	
	// $this->config =  array(
                  // 'upload_path'     => dirname($_SERVER["SCRIPT_FILENAME"])."/files/",
                  // 'upload_url'      => base_url()."files/",
                  // 'allowed_types'   => "gif|jpg|png|jpeg|pdf|doc|xml",
                  // 'overwrite'       => TRUE,
                  // 'max_size'        => "1000KB",
                  // 'max_height'      => "768",
                  // 'max_width'       => "1024"  
                // );
	
	// Check if file already exists
	if (file_exists($target_file)) {
		$message['message'] = "Sorry, file already exists.";
		$is_validimage = 0;
	}
	// Check file size
	if ($_FILES["fileToUpload"]["size"] > 1000000) {
		$message['message'] = "Sorry, your file is too large.";
		$is_validimage = 0;
	}
	// Allow certain file formats
	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
	&& $imageFileType != "gif" ) {
		$message['message'] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		$is_validimage = 0;
	}
	// Check if is_validimage is set to 0 by an error
	if ($is_validimage == 0) {
		$message['message'] = "Sorry, your file was not uploaded.";
	// if everything is ok, try to upload file
	} else {
		if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
			$message['message'] = "The file ". $image_name. " has been uploaded.";
			
			$param_image['image_name'] = $image_name;
			$param_image['image_size'] = $_FILES["fileToUpload"]["size"];
			$this->image_model->save($param_image);
			
		} else {
			$message['message'] = "Sorry, there was an error uploading your file.";
		}
	}
}
?>


<div class="col-md-3">
	<?php echo $SIDEMENUBAR?>
</div>

<div class="col-md-9">
	<h1><?php echo $MODULE?></h1>
	
	<?php // echo $SETTINGMENUBAR ?>
	<?php 
	if (isset($message['message'])) echo message($message['message']).BR;
	?>
	
	<?php 
	if (!isset($do)) 
	{ 
		?>
		<a href="?do=insert" class="btn btn-success br"><i class="fa fa-plus"></i>&nbsp; <?php echo ADD?></a><br/>
		<?php
	}
	?>
	
	<?php 
	if ((($do == "edit" || $do == "view") && !empty($obj_image)) || $do == "insert")
	{
		// if(isset($obj_page['content'])) $obj_page['content'] = preg_replace('^(\<p/>|<\/p>)$','',$obj_page['content']);
		if ($_POST && $is_validimage)
		{
			?>
			Copy Url Image<br/>
			<textarea class="wdtFul input br" rows="3"><?php echo $image_src?></textarea><br/>
			<img src="<?php echo $image_src?>" class="img-responsive" />
			<?php 
		}
		else			
		{
		?>
		<form action="" method="post" enctype="multipart/form-data">
			Select image to upload:
			<input type="file" class="input" name="fileToUpload" id="fileToUpload"> <br/>
			<?php 
			if ( $do == 'insert')
			{
				?>
			<input type="submit" name="btnSave" name="content" class="btn btn-success btn-md wdtFul" value="<?php echo SAVE ?>"/>
				<?php 
			}
			elseif( $do == 'edit')
			{
				?>
			<input type="submit" name="btnUpdate" name="content" class="btn btn-success btn-md wdtFul" value="<?php echo UPDATE ?>"/>
				<?php 
			}
			?>
		</form>
			<?php
		}
		if (($do == 'view' || $do == 'edit') && empty($obj_image))
		{
			echo "No data";
		}
	}
	elseif(!empty($list_image)) 
	{
		?>
		<table class="table table-bordered">
			<tr class="alert alert-warning">
				<td class="b talCnt" width="5px">#</td>
				<td class="b" class="talLft">Image</td>
				<!--<td class="b talCnt" width="200px">Option</td>-->
			</tr>
			<?php 
			foreach($list_image as $key => $rs)
			{
				$key+=1;
				$id = $rs['image_id'];
				$img = base_url().'asset/images/upload/'.$rs['image_name'];
				?>
			<tr>
				<td class="talCnt"><?php echo $key?></td>
				<td><img src="<?php echo $img?>" class="wdtFul" />
				<br/>
				IMAGE URL
				<br/>
				<textarea rows="5" class="wdtFul"><?php echo $img ?></textarea>
				</td>
				<!--
				<td width="100px" class="talCnt">
				
				<a class="btn btn-success btn-sm" href="?do=edit&page_id=<?php echo $id?>"><?php echo EDIT ?></a>
				<a class="btn btn-danger btn-sm" href="?do=delete&page_id=<?php echo $id?>"><?php echo DELETE ?></a>
				</td>
				-->
			</tr>
				<?php 
			}
			?>
		</table>
	<?php
	}
	else
	{
		echo "No data";
	}
	?>
	<br/><br/>
</div>
<script>
	$(document).ready(function(){
		$('.name').keyup(function(){
			var name = $(this).val().toLowerCase();
			name = name.replace(/[^\w]+/ig, "-");
			$('.slug').val(name);
		})
	})
</script>