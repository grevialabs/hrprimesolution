<?php

class Modular extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('member_model');
		$this->load->model('menu_model');
		$this->load->model('config_model');
		$this->load->model('page_model');
		$this->load->model('image_model');
		$this->load->model('log_model');
		
		global $config;
		$config = $this->config_model->get();
		// debug_array($config);die;
	}
	
	public function index()
	{
		$data['SIDEBAR'] = $this->load->view('sidebar',NULL,TRUE);
		$data['CONTENT'] = $this->load->view('modular/v_index',$data,TRUE);
        $this->load->view('index_landing', $data);
	}
	
	public function admin()
	{
		// redirect('admin/dashboard');
	}
	
	public function sitemap()
	{
		$this->load->view('modular/v_sitemap');
	}
	
	public function redir() 
	{
		if (isset($_GET['url'])) {
			$url = urldecode($_GET['url']);
			redirect($url);
		} else {
			redirect(base_url());
		}
	}
	
	public function logout()
	{
		session_start();
		session_destroy();
		unset($_SESSION['hash']);
		setcookie('hash', '', time()-3600, base_url());
		setcookie('hash_company', '', time()-3600, base_url());
		unset($_COOKIE['hash']);
		unset($_COOKIE['hash_company']);
		$this->session->unset_userdata('hash');
		redirect(base_url());
	}	
	
	public function error404()
	{
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Error 404');
		$data['PAGE_TITLE'] = 'Halaman tidak ditemukan.';
		$data['PAGE_HEADER'] = br(2).'<div class="talCnt">Error 404<br/>Halaman tidak ditemukan.</div>';
		$data['CONTENT'] = $this->load->view('modular/v_error404',$data,TRUE);
		$this->load->view('index', $data);
	}
	
	public function about()
	{
        $data = NULL;
		$data['CONTENT'] = $this->load->view('modular/v_page',$data,TRUE);
        $this->load->view('index_landing', $data);
	}
	
	public function contact()
	{
        $this->load->model('emailblast_model');
		
		$data = NULL;
		$data['CONTENT'] = $this->load->view('modular/v_contact',$data,TRUE);
        $this->load->view('index_landing', $data);
	}
	
	public function service()
	{
        $data = NULL;
		$data['CONTENT'] = $this->load->view('modular/v_service',$data,TRUE);
        $this->load->view('index_landing', $data);
	}
	
	public function why()
	{
        $data = NULL;
		$data['CONTENT'] = $this->load->view('modular/v_page',$data,TRUE);
        $this->load->view('index_landing', $data);
	}
	
	public function login()
	{
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Login / Register');
		$data['PAGE_HEADER'] = NULL;
        $data['CONTENT'] = $this->load->view('modular/v_login',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function login_admin()
	{
		// echo decrypt('2uWDfpNs');die;
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Login Admin');
		$data['PAGE_HEADER'] = NULL;
        $data['CONTENT'] = $this->load->view('modular/v_login_admin',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function landing1()
	{
		$data = NULL;
		$data['CONTENT'] = $this->load->view('modular/v_landing1',$data,TRUE);
		$this->load->view('index_landing',$data);
	}
	
	public function career()
	{
        $data = NULL;
		$data['CONTENT'] = $this->load->view('modular/v_page',$data,TRUE);
        $this->load->view('index_landing', $data);
	}
	
	public function our_service($PRODUCT_SLUG = NULL)
	{
        $data = NULL;
		$data['PRODUCT_SLUG'] = $PRODUCT_SLUG;
		$data['CONTENT'] = $this->load->view('modular/v_page',$data,TRUE);
        $this->load->view('index_landing', $data);
	}
	
	// public function search()
	// {	
		// $data['SIDEBAR_RIGHT'] = $this->load->view('sidebar',NULL,TRUE);
		// $data['PAGE'] = 'Search';
		// $data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Article');
		// $data['PAGE_HEADER'] = 'Search';
		// $data['CONTENT'] = $this->load->view('modular/v_search',$data,TRUE);
        // $this->load->view('index', $data);
	// }
	
	/* GENERAL AJAX */
	public function ajax()
	{
		$this->load->model('subscribe_model');
		$data = null;
		$data['CONTENT'] = $this->load->view('modular/v_ajax',$data,TRUE);
		//$this->load->view('index', $data);
	}

}