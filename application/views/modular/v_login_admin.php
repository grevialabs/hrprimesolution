<?php 
global $PAGE_TITLE;

$PAGE_TITLE = LOGIN.' '.ADMIN.' - '.DEFAULT_PAGE_TITLE;
$PAGE_HEADER = LOGIN .' '.ADMIN;

if ($this->common_model->is_member()) redirect(base_url().'member/content');
/*|
  | LOGIN
*/
if ($this->input->post('hdnLogin')) 
{
	$gInfo = '';
	$email = filter(post('l_email'));
	$password = filter(post('l_password'));
	if(isset($email) && isset($password))
	{
		$member = $this->member_model->get(array('email' => $email));
		if(!empty($member))
		{
			if($member['thepassword'] == encrypt($password))
			{
				$encrypt = encrypt($member['member_id']."#".$member['first_name']." ".$member['last_name']."#".$member['email']."#".$member['is_admin']);
				
				$this->session->set_userdata('hash',$encrypt);

				redirect('member/content');

				// redirect to reference url if exist
				if ($this->input->get('url')) 
				{
					$url = $this->input->get('url');
					redirect(base_url().urldecode($url));
				}
				else
				{
					redirect('member/content');
				}
			}
			else
			{
				$gInfo = EMAIL.' / '.PASSWORD.' '.NOT_VALID;
			}
		}
		else
		{
			$gInfo = EMAIL.' '.NOT_FOUND;
		}
	}
}
// echo encrypt("hr2016");
?>
<div class="col-sm-3">
&nbsp;
</div>
<div class='col-sm-6'>
	<h1 class="title-header">&nbsp;<?php echo $PAGE_HEADER?></h1><hr/>
	<div class="errLog"></div>
	<form class='form-horizontal' role='form' method='post'>
		<div class='form-group form-group-sm'>
			<div class='col-sm-12'><input type='email' class='form-control' name='l_email' id='l_email' placeholder='<?php echo EMAIL?>' value='<?php if ($this->input->post('l_email')) echo filter(post('l_email'))?>' required></div>
		</div>
		<div class='form-group form-group-sm'>
			<div class='col-sm-12'><input type='password' class='form-control' name='l_password' id='l_password' placeholder='<?php echo PASSWORD?>' required></div>
		</div>
		<div class='form-group'>
			<div class='col-sm-12'>
			<input type="hidden" name="hdnLogin" value="1"/><button type="submit" value="" class="btn btn-success wdtFul"/><?php echo LOGIN?></button></div>
		</div>
	</form>
	
	<?php
	if (isset($gInfo)) echo print_message($gInfo);
	?>
</div>
<div class="col-sm-3">
&nbsp;
</div>
