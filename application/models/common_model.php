<?php 

Class Common_model extends MY_Model 
{
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function breadcrumb($array_menu = null,$active_menu = NULL)
	{
		//$str = '<div class="clrGry well well-sm br">';
		$str = '<div class="clrGry" style="padding-top:0">';
		$str.= '<a href=\''.base_url().'\'>Home</a>';
		
		if (!empty($array_menu)) {
			foreach($array_menu as $key => $value) {
				$str.= ' <i class="fa fa-chevron-right fntSml clrGry"></i> <a href=\''.base_url().$key.'\'>'.ucwords($value).'</a>&nbsp;';
			}
		}
		if (isset($active_menu)) $str.= ' <i class="fa fa-chevron-right fntSml clrGry"></i> <a class="active">'.ucwords($active_menu).'</a>';
		$str.= '</div>';
		return $str;
	}
	
	public function curl($url)
	{
		// IF NOT CONTAIN http then CURL API ONLY
		if (strpos($url,'ttp:') == FALSE) 
		{
			$url = $this->config->item('api_url').$url;
		}
		
		// init curl object        
		$ch = curl_init();

		// define options
		$optArray = array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true
		);

		// apply those options
		curl_setopt_array($ch, $optArray);

		// execute request and get response
		$result = curl_exec($ch);
		
		return $result;
	}
	
	function common_paging($totalRow = "",$dataPerPage = 10, $param = null){
	$str = "";
	$offset = 0;
	$noPage = '';
	$activePage = '';
	
	// $param['is_show_question_mark'] = TRUE;
	// $param['is_show_total_row'] = FALSE;
	
	if (!isset($param['is_show_question_mark'])) $param['is_show_question_mark'] = TRUE;
	if (!isset($param['is_show_total_row'])) $param['is_show_total_row'] = FALSE;
	
	if(!is_filled($dataPerPage) || $dataPerPage<=0){
		$dataPerPage = 20;
	}
	if (isset($_GET["page"])) {
		$noPage = $activePage = $_GET["page"];
	}
	else

	$noPage = 1;
	
	$showPage = '0';
	$jumData = $totalRow;
	$jumPage = ceil($jumData/$dataPerPage);
	if(!is_numeric($activePage) || $activePage > $jumPage || $activePage<=0){
		$activePage = 1;
	}
	
	// AUTO REMOVE PARAMETER "PAGE"
	$currentUrlParameter = current_url();
	$i = 1;
	if (!empty($_GET))
	{
		if (isset($_GET['page']))
		{
			unset($_GET['page']);
		}

		foreach($_GET as $key => $val)
		{
			$currentUrlParameter.= $key.'='.$val;
			if (count($_GET)!=$i) $currentUrlParameter.= '&';
			$i++;
		}
	}
	
	if($noPage>=1){
		$offset = ($noPage-1) * $dataPerPage;
	}
	
	if ($jumData <= $dataPerPage) $dataPerPage = $jumData;

	if ($noPage > 1) {
		$i = ($noPage-1) * $dataPerPage + 1;
		$dataPerPage = $noPage * $dataPerPage;
		if ($dataPerPage > $jumData) $dataPerPage = $jumData;
	}
	
	if( $jumData!=0 ){
		$currentUrlParameter.= "?";
		$str.= "<nav>
		<ul class='pagination'>";
		//if ($param['is_show_total_row'] == TRUE ) {
			$str.= "<li class=''><a>Show ". $i ." - ".$dataPerPage." of <strong>".$jumData."</strong> row(s)<span class='sr-only'>Total <strong>".$jumData."</strong> row(s)</span></a></li>";
		//}
		$str.= "<li class=''><a>Page ".$activePage." of ".$jumPage." <span class='sr-only'>Page ".$activePage." of ".$jumPage."</span> </a></li>
		";
		if($noPage>1){
			$prev=($noPage-1);
			$str.= "<li><a href=".$currentUrlParameter."page=".$prev."><span aria-hidden='true'>Prev</span><span class='sr-only'>Prev</span></a></li>" ;
		}
		else
		$str.= "<li><a><span aria-hidden='true'>Prev</span><span class='sr-only'>Prev</span></a></li>" ;

		for($page = 1; $page <= $jumPage; $page++){
			if ((($page >= $noPage - 3) && ($page <= $noPage + 8)) || ($page == 1) || ($page == $jumPage)) {   
				if (($showPage == 1) && ($page != 2))  $str.= "<li><a>... <span class='sr-only'>...</span></a><li>"; 
				if (($showPage != ($jumPage - 1)) && ($page == $jumPage))  $str.= "<li><a>... <span class='sr-only'>...</span></a><li>";
				if ($page == $noPage) $str.= "<li class='active'><a>".$page." <span class='sr-only'>".$page."</span></a><li>";
				else 
					$str.= "<li><a href='".$currentUrlParameter."page=".$page."'>".$page." <span class='sr-only'>".$page."</span></a><li>";
					$showPage = $page;
			}
		}

		if($noPage<$jumPage){
			$next=($noPage+1);
			$str.= "<li><a href='".$currentUrlParameter."page=".$next."'>Next <span class='sr-only'>Next</span></a><li>";
		}
		else
		$str.= "<li><a>Next <span class='sr-only'>Next</span></a><li>";
		$str .= "</ul></nav>";
	}
	return $str;
	}
	
	// This function need jquery to be executed
	function js_sleep_redirect($url, $timer = 2)
	{
		?>
		<script>
		$(function(){
		   function show_popup(){
			  window.location = "<?php echo $url;?>";
		   };
		   window.setTimeout( show_popup, <?php echo $timer; ?>000 ); // 2 seconds
		});
		</script>
		<?php
	}
	
	function get_right($string,$chars){
		$str = substr($string, strlen($string)-$chars,$chars);
		return $str;  
	}
	function get_left($string,$chars){
		$str = substr($string,0,$chars);
		return $str;
	}
	
	function is_member($user = "member")
	{
		if ($this->session->userdata('hash'))
		{
			explode('#',decrypt($this->session->userdata('hash')));
			return TRUE;
		}
		else
		return FALSE;
	}
	
}