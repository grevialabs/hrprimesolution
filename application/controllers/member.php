<?php 

class Member extends MY_Controller 
{

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('member_model');
		$this->load->model('page_model');
		$this->load->model('image_model');
		
		$this->load->helper('directory');
		
		$current_url = str_replace(base_url(),'',current_url());
		// if (!member_cookies("member_id")) redirect(base_url().'login?url='.urlencode($current_url));
		if (!$this->common_model->is_member()) redirect(base_url().'login_admin?url='.urlencode($current_url));
	}
	
	public function index()
	{
		$data = NULL;
		$PAGE_TITLE = HOME;
		$data['PAGE_TITLE'] = $PAGE_TITLE;
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, $PAGE_TITLE);
		$data['PAGE_HEADER'] = NULL;
		$data['SUBPAGE_HEADER'] = $PAGE_TITLE;
		
        $data['CONTENT'] = $this->load->view('member/adm_member',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function profile()
	{
		$data = NULL;		
		$data['SIDEMENUBAR'] = $this->load->view('member/mbr_sidemenubar',$data,TRUE);
		$data['SETTINGMENUBAR'] = $this->load->view('member/mbr_settingmenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('member/mbr_profile',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function content()
	{
		$data = NULL;
		$data['SIDEMENUBAR'] = $this->load->view('member/mbr_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('member/mbr_content',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function image()
	{
		$data = NULL;
		$data['SIDEMENUBAR'] = $this->load->view('member/mbr_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('member/mbr_image',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function setting()
	{
		$data = NULL;
		$PAGE_TITLE = SETTING;
		$data['PAGE_TITLE'] = $PAGE_TITLE;
		$data['PAGE_HEADER'] = NULL;
		
		$data['SIDEMENUBAR'] = $this->load->view('member/mbr_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('member/mbr_setting',$data,TRUE);
        $this->load->view('index', $data);
	}

}