<?php 

/* MENU */
DEFINE('MENU_ABOUT_US', 'Tentang Kami');
DEFINE('MENU_CONTACT', 'Kontak');
DEFINE('MENU_LOGIN', 'Login / Registrasi');
DEFINE('MENU_SEARCH', 'Cari');
DEFINE('MENU_FORGOT_PASSWORD', 'Lupa Password');
DEFINE('MENU_RESEND_ACTIVATION_EMAIL', 'Kirim ulang email aktivasi');
DEFINE('MENU_NOTIFICATION', 'Notifikasi');

// MENU COMPANY
DEFINE('MENU_COMPANY_BILLING', 'Billing');
DEFINE('MENU_COMPANY_ACCOUNT', 'Akun Perusahaan');
DEFINE('MENU_COMPANY_REPORT', 'Laporan');
DEFINE('MENU_COMPANY_TEST_CANDIDATE', 'Tes Kandidat');
DEFINE('MENU_COMPANY_PARTICIPANT', 'Data Peserta');

DEFINE('MENU_MY_ACCOUNT', 'Akun Saya');
DEFINE('MENU_MY_SETTING', 'Pengaturan');
DEFINE('MENU_MY_PROFILE', 'Profil');
DEFINE('MENU_MY_ARTICLE', 'Artikel');
DEFINE('MENU_MY_FORUM', 'Forum');
DEFINE('MENU_ORDER_PRODUCT', 'Paket Produk');

DEFINE('MENU_CHANGE_PASSWORD', 'Ubah kata sandi');
DEFINE('MENU_LOGOUT', 'Keluar');
DEFINE('MENU_LOGIN_REGISTER', 'Masuk / Registrasi');
DEFINE('MENU_SUBSCRIBE', 'Berlangganan');
DEFINE('MENU_HISTORY_TEST', 'Riwayat Test');
DEFINE('MENU_TEST', 'Ikut Test');
DEFINE('MENU_REGISTER', 'Registrasi');
//DEFINE('', '');

/* MESSAGE */
DEFINE('INFO_SAVE_SUCCESS', 'Data berhasil disimpan.');
DEFINE('INFO_DATA_INVALID', 'Data tidak valid.');
DEFINE('INFO_ERROR_OCCURED', 'Mohon maaf terjadi kesalahan.');
DEFINE('INFO_EMAIL_SUBSCRIBE_SUCCESS', 'Email berhasil berlangganan.');
DEFINE('INFO_EMAIL_SUBSCRIBE_EXIST', 'Email sudah terdaftar.');

DEFINE('INFO_I_AGREE_TERM_CONDITION', 'Dengan ini, saya setuju dengan syarat dan ketentuan yang berlaku');
DEFINE('INFO_REMEMBER_ME', 'Ingat saya');
/* END MESSAGE */

/* REGISTER PAGE */
DEFINE('ACTIVATION_ACCOUNT_MEMBER', 'Aktivasi akun member');
DEFINE('PLEASE_USE_ANOTHER_EMAIL', 'Silakan gunakan email lain');
DEFINE('REGISTRATION_FAIL_PLEASE_TRY_AGAIN', 'Registrasi Gagal. Silakan coba lagi.');
DEFINE('REGISTRATION_SUCCESS', 'Registrasi sukses');
DEFINE('PLEASE_CLICK', 'Silakan klik');
DEFINE('THIS_LINK', 'link berikut');
DEFINE('OR_COPY_PASTE_THIS_URL_TO_BROWSER', 'atau copy & paste ini ke browser anda');
DEFINE('TO_CONFIRM_YOUR_ACCOUNT', 'untuk konfirmasi akun anda');
DEFINE('YOU_GOT_THIS_EMAIL_BECAUSE_YOU_REGISTER_IN', 'Anda mendapatkan email ini karena anda telah mendaftar di');
DEFINE('PLEASE_LOGIN', 'Silakan Login');

/* ACTIVATION */
DEFINE('IS_ACTIVATED_ALREADY', 'sudah aktif');
DEFINE('IS_ACTIVATED', 'berhasil diaktivasi');



/* VOCAB */
// A
DEFINE('ABOUT_ME', 'Tentang saya');
DEFINE('ABOUT_US', 'Tentang kami');
DEFINE('ACTIVATION', 'Aktivasi');
DEFINE('ACTIVE', 'Aktif');
DEFINE('ACTION', 'Aksi');
DEFINE('ACCOUNT', 'Akun');
DEFINE('ADMIN', 'Admin');
DEFINE('ARTICLE', 'Artikel');
DEFINE('ARTICLE_CATEGORY', 'Kategori artikel');
DEFINE('ANSWER', 'Jawaban');
DEFINE('ALL', 'Semua');
DEFINE('ASSESSMENT', 'Penafsiran');
DEFINE('AVAILABLE_TEST', 'Test Tersedia');

// B
DEFINE('BACK','Kembali');
DEFINE('BEFORE','Sebelum');
DEFINE('BILLING','Tagihan');

// C
DEFINE('CANCEL', 'Batal');
DEFINE('CATEGORY', 'Kategori');
DEFINE('CONFIRM', 'Konfirmasi');
DEFINE('COMPANY', 'Perusahaan');
DEFINE('CURRENCY', 'IDR');
DEFINE('COST', 'Biaya');
DEFINE('CHANGE_PASSWORD', 'Ubah Sandi');
DEFINE('CONTACT', 'Kontak');
DEFINE('CONTACT_US', 'Kontak kami');
DEFINE('COMPANY_NAME', 'Nama Perusahaan');
DEFINE('COMPANY_ADDRESS', 'Alamat Perusahaan');
DEFINE('COMPANY_PIC', 'PIC Perusahaan');
DEFINE('CONFIRMATION_ORDER', 'Konfirmasi pesanan');
DEFINE('CREATE_ACCOUNT_DATE', 'Tgl Buat Akun');

// D
DEFINE('DATA', 'Data');
DEFINE('DATA_NOT_FOUND', 'Data tidak ditemukan');
DEFINE('DETAIL', 'Detail');
DEFINE('DETAIL_DESCRIPTION', 'Detail Deskripsi');
DEFINE('DETAIL_ATTEND', 'Detail Kehadiran');
DEFINE('DESCRIPTION', 'Deskripsi');
DEFINE('DEAR', 'Dear');
DEFINE('DOB', 'Tgl Lahir');
DEFINE('DISCOUNT', 'Diskon');

// E
DEFINE('EMAIL', 'Email');
DEFINE('EMAIL_BLAST_TEST', 'Kirim email tes');
DEFINE('EXPIRED_TEST_DATE', 'Tgl tes berakhir');
DEFINE('ENTER', 'Masuk');
DEFINE('EXPORT_PDF', 'Eksport PDF');

// F
DEFINE('FAIL', 'Gagal');
DEFINE('FAILED', 'Gagal');
DEFINE('FEMALE', 'Wanita');
DEFINE('FIRST_NAME', 'Nama depan');
DEFINE('FILL', 'Masukkan');
DEFINE('FILTER', 'Filter');
DEFINE('FORUM', 'Forum');
DEFINE('FROM', 'dari');
DEFINE('FREE_CHARGE', 'Gratis');
DEFINE('FULL_NAME', 'Nama lengkap');
DEFINE('FORGOT_PASSWORD', 'Lupa Password');

// G
DEFINE('GENDER', 'Jenis kelamin');

// H
DEFINE('HAS_REGISTERED', 'sudah terdaftar');
DEFINE('HISTORY_TEST', 'Riwayat Tes');
DEFINE('HOME', 'Beranda');

// I
DEFINE('INACTIVE', 'Tidak aktif');
DEFINE('INPUT', 'Masukan');
DEFINE('IS_PUBLISH', 'Di-publish');
DEFINE('IS_PUBLIC', 'Di-publik');

DEFINE('ORDER_NOT_PAID', 'Belum dibayar');
DEFINE('ORDER_WAITING_CONFIRMATION', 'Menunggu Konfirmasi');
DEFINE('ORDER_PENDING', 'Pending');
DEFINE('ORDER_ISSUED', 'Sukses');
DEFINE('ORDER_CANCELED', 'Batal');

// J
DEFINE('JOBTALENTO_TEAM', 'Tim JobTalento');
DEFINE('JOBTALENTO', 'JobTalento');
DEFINE('JOBTALENTO.COM', 'JobTalento.com');
DEFINE('JOB_POSITION', 'Jabatan yang ditawarkan');


// K
DEFINE('KEYWORD', 'Kata kunci');
DEFINE('KEYWORDS', 'Kata kunci');

// L


// M
DEFINE('MALE', 'Pria');
DEFINE('MARK_UNREAD', 'Tandai Belum Terbaca');
DEFINE('MAX_USER_ALLOWED', 'Maksimum pengguna');
DEFINE('MILLION', 'juta');
DEFINE('MINUTE', 'menit');
DEFINE('MEMBER', 'Member');
DEFINE('MUST_FILLED', 'Harus diisi');
DEFINE('MOBILE', 'No hp');

// N
DEFINE('NAME', 'Nama');
DEFINE('NO_DATA', 'Data tidak tersedia');
DEFINE('NOTES', 'Catatan');
DEFINE('NOTIFICATION', 'Notifikasi');
DEFINE('NOT_FOUND', 'Tidak ditemukan');
DEFINE('NOT_VALID', 'Tidak valid');
DEFINE('NOT_READY_YET', 'Belum tersedia');

// O
DEFINE('OR', 'Atau');
DEFINE('ORDER', 'Pesan');

// L
DEFINE('LAST_NAME', 'Nama belakang');
DEFINE('LOGIN', 'Login'); 

// P
DEFINE('PACKET_BRONZE','Paket Bronze');
DEFINE('PACKET_SILVER','Paket Silver');
DEFINE('PACKET_GOLD','Paket Gold');
DEFINE('PARTICIPANT_TEST','Tes Kandidat');
DEFINE('PARTICIPANT_DATA','Data Kandidat');
DEFINE('PASSWORD', 'Kata sandi');
DEFINE('PER_MONTH', 'per bulan');
DEFINE('POWERED_BY_JOBTALENTO', 'teks <b>Powered by Jobtalento</b>');
DEFINE('PRIORITY_SUPPORT', 'Prioritas Support');
DEFINE('PIC', 'PIC');
DEFINE('PHONE', 'Telepon');
DEFINE('PRODUCT', 'Produk');

DEFINE('PAYMENT_STATUS', 'Status bayar');
DEFINE('PARTICIPANT', 'Peserta');
DEFINE('PRICE', 'Harga');
DEFINE('PROFILE', 'Profil');
DEFINE('PRODUCT_NAME', 'Nama Produk');
DEFINE('PLEASE', 'Silakan');
DEFINE('PLEASE_SELECT', 'Silakan pilih');


//Q
DEFINE('Qty', 'Qty');
DEFINE('QUANTITY', 'Kuantitas');
DEFINE('QUOTA_NOT_ENOUGH', 'Quota tidak mencukupi');

// R
DEFINE('REGISTRATION', 'Registrasi');
DEFINE('REGISTER', 'Daftar');
DEFINE('REPORT', 'Laporan');
DEFINE('RESULT', 'Hasil');

// S
DEFINE('SCORING', 'Hasil skor');
DEFINE('SCORE_DESCRIPTION', 'Deskripsi Skoring');
DEFINE('SEARCH', 'Cari');
DEFINE('SETTING', 'Pengaturan');
DEFINE('SELECTED', 'Terpilih');
DEFINE('SET_TYPE_TEST', 'Atur tipe tes');
DEFINE('SELECT_GENDER', 'Pilih jenis kelamin');
DEFINE('SUBTOTAL', 'Subtotal');
DEFINE('SUBSCRIBE_NEWSLETTER', 'Berlangganan newsletter');
DEFINE('STATISTIC', 'Statistik');
DEFINE('START_TEST', 'Mulai Tes');
DEFINE('STATUS', 'Status');

// T
DEFINE('TAX', 'Pajak');
DEFINE('THANK_YOU', 'Terima kasih');
DEFINE('THIS', 'ini');
DEFINE('TEST', 'Tes');
DEFINE('TEST_ALREADY_TAKEN', 'Tes sudah dijalani');
DEFINE('TEST_RESULT', 'Hasil Tes');
DEFINE('TEST_NAME', 'Nama Tes');
DEFINE('TEST_TITLE', 'Judul Tes');
DEFINE('TEST_DATE', 'Tgl Tes');
DEFINE('TEST_TYPE', 'Tipe Tes');
DEFINE('TEST_SCORE', 'Skor Tes');
DEFINE('TREASURE', 'Aset');
DEFINE('TRY', 'Lagi');
DEFINE('TITLE', 'Judul');
DEFINE('TIMER', 'Waktu durasi');
DEFINE('TOKEN', 'Token');

DEFINE('OLD_PASSWORD', 'Kata sandi lama');
DEFINE('NEW_PASSWORD', 'Kata sandi baru');
DEFINE('CONFIRM_NEW_PASSWORD', 'Konfirmasi sandi baru');

// U


// V
DEFINE('ISSUE', 'Issue');

// W
DEFINE('WARM_REGARDS', 'Salam Hangat');

// X


// Y
DEFINE('YOUR_BRAND', 'Powered by merek anda');
DEFINE('YOUR_RESULT', 'Hasil tes anda');
DEFINE('YOU_DONT_HAVE_NOTIFICATION_UNREAD', 'Anda tidak memiliki notifikasi tidak terbaca');
// Z



// 
/* */

/* */
DEFINE('ADD_NEW', 'Tambah baru');
DEFINE('ADD', 'Tambah');
DEFINE('EDIT', 'Mengedit');
DEFINE('INSERT', 'Tambahkan');
DEFINE('SAVE', 'Simpan');
DEFINE('SUBMIT', 'Kirim');
DEFINE('UPDATE', 'Perbarui');
DEFINE('DELETE', 'Hapus');
//DEFINE('', '');

/*
v_participant_test
*/

DEFINE('YOU_WILL_FOLLOW_TEST_FROM', 'Anda akan mengikuti tes dari');
DEFINE('PLEASE_TAKE_TEST_IN_TIME', 'Silakan mengisi test dalam waktu yang telah ditentukan');
DEFINE('YOUR_TIME_IS_UP', 'Waktu anda telah habis');

/*
company/com_participant
*/
DEFINE('ADD_PARTICIPANT', 'Tambah user');
DEFINE('YOU_HAVE_QUOTA', 'Anda memiliki quota');
DEFINE('PLEASE_BUY_QUOTA_FIRST', 'Silakan beli quota terlebih dahulu');