<?php

$CI = &get_instance();

global $config;
// debug($config['company_description']);

global $PAGE, $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;

$PAGE = "Contact Us";
$BREADCRUMB = $this->common_model->breadcrumb(NULL, $PAGE);
$PAGE_TITLE = $PAGE.' - '.DEFAULT_PAGE_TITLE;;
$PAGE_HEADER = $PAGE;

$gInfo = '';

// ======================================
// RECAPTCHA
$is_recaptcha_ok = 0;
$site_key_recaptcha = '6Lee7WweAAAAACgctSMqT9Cd6jMipg6kG_iFSP3Y';
$secret_key_recaptcha = '6Lee7WweAAAAAP93f02R2dGdGwQeCCmFuHcZ4TlX';

// RECAPTCHA START
if ($_POST) 
{
	$is_recaptcha_ok = -1;
	$captcha;
	
	$response = $responseKeys = NULL;
	if (isset($_POST['g-recaptcha-response'])){
		$captcha = $_POST['g-recaptcha-response'];

		$ip = $_SERVER['REMOTE_ADDR'];
		// post request to server
		$url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secret_key_recaptcha) .  '&response=' . urlencode($captcha);
		$response = file_get_contents($url);
		$responseKeys = json_decode($response,true);
		
		// Save to log 
		$log = $tmp = NULL;
		$tmp['raw'] = $url;
		$tmp['result'] = $responseKeys;
		$log['source'] = 'contact';
		$log['subject'] = 'RECAPTCHA';
		$log['content'] = json_encode($tmp,1);
		// $log['notes'] = '';
		$this->log_model->save($log);
		
		// should return JSON with success as true
		if (isset($responseKeys["success"]) && $responseKeys["success"] === TRUE && !isset($responseKeys['error-codes'])) {	
			$is_recaptcha_ok = 1;
		}
		
		// debug($responseKeys);
		// debug($is_recaptcha_ok);
		// die;
	
	}

}
// RECAPTCHA END
// ======================================

// DEFAULT RESPONSE
if ($is_recaptcha_ok < 0) $gInfo = "Ooops, your captcha input was wrong. Please try again";
// debug($is_recaptcha_ok);
// die;
if ($is_recaptcha_ok >= 1)
{
	$name = post('tName');
	$email = filter($this->input->post('tEmail'));
	$subject = filter($this->input->post('tSubject'));
	$message = filter($this->input->post('tMessage'));
	$company = filter($this->input->post('tCompany'));
	$phone = filter($this->input->post('tPhone'));
	if(!is_filled($name)){
		$gInfo .= "Name must be filled.";
	} elseif (!is_filled($email)){
		$gInfo .= "Email must be filled.";
	} elseif (!is_filled($subject)){
		$gInfo .= "Subject must be filled.";
	} elseif (!is_filled($company)){
		$gInfo .= "Company must be filled.";
	} elseif (!is_filled($message)){
		$gInfo .= "Message must be filled.";
	}		else {
		// $save = array(
			// 'fullname' => $name,
			// 'email' => $email,
			// 'subject' => $subject,
			// 'message' => $message,
		// );
		// $save = $this->inbox_model->save($save);
		
		$content = '
		<html>
		<body>
		<table style="" cellpadding="15px" cellspacing="0" border="1">
		<tr>
			<td width="80px">Name</td>
			<td>'.$name.'</td>
		</tr>
		<tr>
			<td width="80px">Email</td>
			<td>'.$email.'</td>
		</tr>
		<tr>
			<td width="80px">Subject</td>
			<td>'.$subject.'</td>
		</tr>
		<tr>
			<td width="80px">Company</td>
			<td>'.$company.'</td>
		</tr>
		<tr>
			<td width="80px">Phone</td>
			<td>'.$phone.'</td>
		</tr>
		<tr>
			<td width="80px">Message</td>
			<td>'.$message.'</td>
		</tr>
		</table>
		</body>
		</html>
		';
		
		
		// $headers = "From: Grevia<rusdi@grevia.com> \r\n";
		// $headers .= "Reply-To: \r\n";
		// $headers .= "MIME-Version: 1.0\r\n";
		// $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		
		$config_email = NULL;
		
		// Zoho still not working motherfucker
		// ----------------------------------------------------
		// // $config_email['smtp_host'] = 'smtppro.zoho.com';
		// $config_email['smtp_host'] = 'smtppro.zoho.com';
		// $config_email['smtp_user'] = 'rusdi@grevia.com';
		// // $config_email['smtp_pass'] = 'Rusdi2012!';
		// $config_email['smtp_pass'] = 'Q45RaMUT1XwG!';
		// $config_email['validate'] = true;
		// $config_email['smtp_port'] = 465;
		// $config_email['smtp_crypto'] = 'ssl';
		// $config_email['protocol'] = 'smtp';
		// $config_email['mailtype'] = 'html';
		// ----------------------------------------------------
		
		
		// ----------------------------------------------------
		// smtp sending not working too
		// $config_email['smtp_host'] = 'smtp-relay.sendinblue.com';
		// $config_email['smtp_user'] = 'grevianet@gmail.com';
		// $config_email['smtp_pass'] = 'xsmtpsib-b766e3043442a60c80fa7b4473db77b4eca048981f87c1f1a3820ac91260d1c2-ETmBknMySCqOHjWX';
		// $config_email['validate'] = true;
		// $config_email['smtp_port'] = 587;
		// $config_email['smtp_crypto'] = 'tls';
		// $config_email['protocol'] = 'smtp';
		// $config_email['mailtype'] = 'html';

		// USE sendinblue
		// ----------------------------------------------------
		
		/*
		USE CURL
		
		curl --request POST \
		  --url https://api.sendinblue.com/v3/smtp/email \
		  --header 'accept: application/json' \
		  --header 'api-key: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' \
		  --header 'content-type: application/json' \
		  --data '{  
		   "sender":{  
			  "name":"Grevianet",
			  "email":"grevianet@gmail.com"
		   },
		   "to":[  
			  {  
				 "email":"rusdi.karsandi@gmail.com",
				 "name":"RusdiGrevia"
			  }
		   ],
		   "subject":"Hello world",
		   "htmlContent":"<html><head></head><body><p>Hello,</p>This is my first transactional email sent from Sendinblue.</p></body></html>"
		}'
		
		if success the callback is json like below:
		{"messageId":"<202202080724.91590161634@smtp-relay.mailin.fr>"
		
		*/
		// if using api curl use this key: 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
		
		
		// content-type: application/json
		// accept: application/json
		// 
		// $apismtp = "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";
		// $header['api-key'] = $apismtp;
		// $header['smtp_url'] = "https://api.sendinblue.com/v3/smtp/email";
		
		// $url = 'http://localhost/greviacom/api/membernew/get_list';
		// $curl = $CI->curl_sendblue($url,'get','','');
		$is_sent = 0;
		
		// Change accordingly admin
		// $to = 'winartaulina@gmail.com';
		$to = 'wina@hrprimesolution.com';
		// $to = 'rusdi.karsandi@gmail.com';
		$to_alias = 'Wina HRPrimesolution';
		
		$from = 'info@hrprimesolution.com';
		$from_alias = 'HRPrime Form';
		
		$paramcurl = NULL;
		$paramcurl['sender'] = array(
			'name' => $from_alias,
			'email' => $from
			);
		$paramcurl['to'][] = array(
			'name' => $to_alias,
			'email' => $to,
		);
		$paramcurl['subject'] = $from_alias.' '.$subject;
		$paramcurl['htmlContent'] = $content;
		
		$curl = $CI->curl_sendblue('post',$paramcurl);
		// callback in json messageId
		
		// Check callback exist or not
		
		if (isset($curl['response']['messageId'])) $is_sent = 1;
					
		// ----------------------------------------------------
		// SMTP not working
		// $this->email->initialize($config_email);
		// $this->email->from($from, $from_alias);
		// $this->email->to($to);
		// $this->email->subject($subject);
		// $this->email->message($content);
		// $send = $this->email->send();
		// ----------------------------------------------------
		
		// Save to database
		$param = NULL;
		$param['from_alias'] = $from;
		$param['subject'] = $subject;
		$param['content'] = $content;
		$param['to_list'] = $to;
		// $param['cc_list'] = ;
		// $param['bcc_list'] = ;
		// $param['is_html'] = ;
		$param['is_sent'] = $is_sent;
		$param['sent_count'] = 1;
		$param['remarks'] = json_encode($paramcurl);
		if (isset($curl)) $param['log'] = json_encode($curl);
		$param['creator_id'] = 999;
		$param['creator_date'] = get_datetime();
		$save = $this->emailblast_model->save($param);

		if ($is_sent) 
			$gInfo = "Thank you for your email, we will respond in 2 x 24 hour.";
		else
			$gInfo = "Sorry, error connection. Please try again.";
	}	
}

$page = $this->page_model->get(array('slug' => 'contact'));
$content = preg_replace('/(<p>|<\/p>)/i','',$page['content']);
?>

<script src='https://www.google.com/recaptcha/api.js' async defer></script>
<script>
// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
  

function initMap() {
 
   var contentString = '<div><b>HR Prime Solution Office</b><br/>PT. Sinergi Prima Solusi Indonesia<br/>Ged. Gondangdia Lama 25 lt. 5 Rg. 7<br/>Jl. RP. Soeroso No. 25 Menteng, Jakarta Pusat</div>';

  var myLatLng = {lat: -6.191429, lng: 106.836390};

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 15,
    center: myLatLng
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: 'Hello World!'
  });
  
  //-------------------------------------------
  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });

  // var marker = new google.maps.Marker({
    // position: uluru,
    // map: map,
    // title: 'Uluru (Ayers Rock)'
  // });
  // marker.addListener('click', function() {
    infowindow.open(map, marker);
  // });
}

</script>
<style>
 #map {
	height: 500px;
	width : 100%;
	padding: 15px ;
 }

</style>

<div class="jumbotron bgWht">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<?php if (isset($BREADCRUMB)) echo $BREADCRUMB.'' ?>
				<?php // if (isset($PAGE_HEADER)) echo '<h2>'.$PAGE_HEADER.'</h2>' ?>
			
				<div class="col-sm-6">
					<?php if (isset($PAGE_HEADER)) echo '<h2>'.$PAGE_HEADER.'</h2>' ?>
					<!--
					All HR Prime Solution services are designed in accordance with the climate and the needs of your company. Not only short-term gains, such as increased work motivation, but also long-term benefits related to the productivity of the company, is our target. Therefore, choose us as your partner is a right and profitable step for your business.<br/><br/>
					
					With the various offers that are professional and flexible, we are ready to serve you with our expertise. If you are interested and want to know more about the programs and services we provide, please feel free to contact us in order to obtain more detailed explanation.
					<br/><br/>
					
					<span class="b">
					HR Prime Solution<br/>
					AEKI Building 3rd Floor<br/>
					Jl. RP Soeroso No. 20 Cikini, Menteng - Jakarta Pusat<br/>
					0858-8886-7080<br/>
					info@hrprimesolution.com<br/>
					wina@hrprimesolution.com<br/><br/>
					</span>
					-->
					<?php echo nl2br($content)?>
				</div>
				<div class="col-sm-6">
					<h2>Inquiry Form</h2>
					<?php if (is_filled($gInfo)) echo BR.BR.message($gInfo)."";?>
					<form method="post">
						<input type="text" name="tName" class="input wdtFul" placeholder="Name" required /><br/><br/>
						<input type="text" name="tEmail" class="input wdtFul" class="input round bdrGry" placeholder="Email" required/><br/><br/>
						<input type="text" name="tSubject" class="input wdtFul" placeholder="Subject" /><br/><br/>
						<input type="text" name="tCompany" class="input wdtFul" placeholder="Company" /><br/><br/>
						<input type="text" name="tPhone" class="input wdtFul" placeholder="Phone" required /><br/><br/>
						<textarea placeholder="Message here..." name="tMessage" rows="8" class="input wdtFul" required></textarea><br/><br/>
						<input type="hidden" name="hdnSubmit" value="1"/>

						<div class="g-recaptcha" data-sitekey="<?php echo $site_key_recaptcha; ?>"></div>

						<input type="submit" class="btn btn-success" value="Submit"/>
					</form><br/>
				</div>
				
				<div id="map"></div>
			</div>
		</div>
	</div>
</div>

<!--
<div class="jumbotron" >
	<div class="container">
		<div class="rows min-height-250">
			<div class="col-sm-12">
			
			
			Silakan mengisi form dibawah ini untuk menghubungi tim kami. <i class="fa fa-question-circle pointer" data-toggle="popover" data-trigger="hover" title="Panduan Mengisi" data-content="Silakan mengisi seluruh field dengan lengkap"></i>
				<br/><br/><?php if (is_filled($gInfo)) echo BR.BR.message($gInfo)."";?>
				<form method="post">
				<input type="text" name="tName" class="input wdtFul" placeholder="Name"/><br/><br/>
				<input type="text" name="tEmail" class="input wdtFul" class="input round bdrGry" placeholder="Email"/><br/><br/>
				<input type="text" name="tSubject" class="input wdtFul" placeholder="Subject" value="<?php if (isset($_GET['subject'])) echo $_GET['subject']?>"/><br/><br/>
				<textarea placeholder="Message here..." name="tMessage" rows="8" class="input wdtFul"></textarea><br/><br/>
				<input type="hidden" name="hdnSubmit" value="1"/>
				<input type="submit" class="btn btn-success" value="<?php echo SUBMIT?>"/>
				</form><br/>
	
			</div>
		</div>
	</div>
</div>
-->

 <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCNBifdMEWimVBn9YmqoPzluMrccOuWLQA&signed_in=true&libraries=places&callback=initMap">
    </script>
	
	<script>
	$(document).ready(function(){
		$('img').addClass('img-responsive');
	})
	</script>

<!-- RECAPTCHA START -->
<script type="text/javascript">
  var onloadCallback = function() {
    console.log("grecaptcha is ready!");
  };
</script>

<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
    async defer>
</script>
<!-- RECAPTCHA END -->