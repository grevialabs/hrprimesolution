<?php 

global $config;

// debug($config);
?>
<!--
<div class="jumbotron bgBluLight" style="margin-bottom:0;padding:35px 10px">
	<div class="container">
		<div class="col-md-3 lnkLogo">
		<a class="fntNml"><i class="fa fa-comments fa-2x"></i> <span class="clrWht">Navigation</span></a><br/>

			<a href="<?php echo base_url()?>about">About <?php echo show_active_menu('about')?></a><br/>
			<a href="<?php echo base_url()?>contact">Contact <?php echo show_active_menu('contact')?></a>
		</div>
		
		<div class="col-md-3 lnkLogo">
		<a class="fntNml"><i class="fa fa-comments fa-2x"></i> <span class="clrWht">Navigation</span></a><br/>
		<a class="btn btn-social-icon btn-twitter"><i class="fa fa-twitter"></i>&nbsp;</a>
		<a class="btn btn-social-icon btn-facebook"><i class="fa fa-facebook"></i>&nbsp;</a>
		</div>
		<div class="col-md-6 talCnt lnkWht"></div>
		<div class="clearfix"></div>
	</div>
</div>
-->

<?php if ($this->uri->segment(2) != 'admin') { ?>
<!--
<div class="jumbotron bgTopFooter" style="margin-bottom:0; padding-bottom:0; padding: 20px">
	<div class="container clrLgtGry" style="padding: 20px">
		<div class="col-sm-2 talLft lnkBlk">
			<img class="img-responsive wdtFul" src="<?php echo base_url()?>asset/images/logo-grevia.png"/>
		</div>
		<div class="col-sm-3 talLft ">
	Grevia adalah <strong>Grup Evangelist Internet Indonesia</strong> yang memiliki visi meningkatkan jumlah pengusaha di bidang teknologi informasi khususnya digital kreatif dan technopreneur di Indonesia.<br/><br/>
		</div>
		<div class="col-sm-2 talLft f12 lnkLgtGry">
			 &nbsp;
		</div>
		<div class="col-sm-1 f12 lnkLgtGry" style="padding-left:25px"><span class="b">Navigation</span><br/>
		<a href="<?php echo base_url()?>article">Article<br/>
		<a href="<?php echo base_url()?>about">About<br/>
		<a href="<?php echo base_url()?>contact">Contact</a><br/>
		</div>
		<div class="col-sm-1 talLft clrWht"></div>
		<div class="col-sm-3 talLft clrWht">
			<div class="well well-sm clrBlk" id="divSubscribe">
				<div class="br">Daftar Email anda ke newsletter</div>
				<input class="input br wdtFul" type="text" name="t_EmailSubscribe" id="t_EmailSubscribe" placeholder="Email anda..."/><br/>
				<button class="btn btn-info wdtFul subscribe">Submit</button>
			</div><br/>
			<a href="<?php echo current_url().'?lang=en'?>"/>English</a>
			<a href="<?php echo current_url().'?lang=id'?>"/>Indonesian</a>
		</div>
	</div>
</div>
DD6B7F
e87162
-->
<?php } ?>
<div class="" style="margin-bottom:0; background:#444444; padding:25px 0 25px 0;border-top:1px solid grey">
	<div class="container">
		<div class="row clrWht lnkLogo">
			<div class="col-md-6 talLft">
				<?php if (isset($config['company_description'])) echo nl2br($config['company_description'])?>
			</div>
			<div class="col-md-6 talRgt">
				<?php 
				if (!isset($config['facebook']))
				{	
					$config['facebook'] = '#';
				}
					$url = 'https://www.facebook.com/'.$config['facebook'];
					?>
				<a href="<?php echo $url?>" target="_blank" title="Fanpage HRPrimeSolution" alt="Fanpage HRPrimeSolution">
				<span class="fa-stack fa-lg">
				  <i class="fa fa-square fa-stack-2x text-info"></i>
				  <i class="fa fa-facebook fa-stack-1x"></i>
				</span>
				</a>
					<?php 
				// }

				if (!isset($config['twitter']))
				{	
					$config['twitter'] = '#';
				}
					$url = 'https://twitter.com/'.$config['twitter'];
					?>
				<a href="<?php echo $url?>" target="_blank" title="Twitter HRPrimeSolution" alt="Twitter HRPrimeSolution">
				<span class="fa-stack fa-lg">
				  <i class="fa fa-square fa-stack-2x text-info"></i>
				  <i class="fa fa-twitter fa-stack-1x"></i>
				</span>
				</a>
				
					<?php
				// }
				
				if (!isset($config['instagram']))
				{	
					$config['instagram'] = '#';
				}
					$url = 'https://www.instagram.com/'.$config['instagram'];
					?>
				<a href="<?php echo $url?>" target="_blank" title="Instagram HRPrimeSolution" alt="Instagram HRPrimeSolution">
				<span class="fa-stack fa-lg">
				  <i class="fa fa-square fa-stack-2x text-info"></i>
				  <i class="fa fa-instagram fa-stack-1x"></i>
				</span>
				</a>
					<?php 
				// }
				?>
			</div>
			
			<div class="col-sm-12 br-md">
			<hr/>
			</div>
			
			<div class="col-sm-12 ">
				<div class="row">
				<div class="col-md-3 "></div>
				<div class="col-md-6 talCnt clrWht">
					<a class="nohover"><span class="fntNml"><span ondblclick="window.location='<?=base_url()?>login_company'">&copy;</span> copyright <?php echo date('Y',time())?></span></a>
					<!-- <br/>Page rendered in <strong>{elapsed_time}</strong> seconds {memory_usage}-->
				</div>
				<div class="col-md-3 clrWht talRgt">Powered by <a href="http://www.grevia.com?ref=hrprimesolution" target="_blank" alt="Grevia Networks - Webdesign company" title="Grevia Networks - Webdesign company" >Grevia Networks</a></div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
function IsEmail(email) {
	var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if(!regex.test(email)) {
		return false;
	}else{
		return true;
	}
  }
  
function togglebox(){
	if ($('.togglebox').prop('checked')) {
		$('.chkbox').prop('checked',true);
	}
	if (!$('.togglebox').prop('checked')) {
		$('.chkbox').prop('checked',false);
	}
}

function confirmDelete() {
	return confirm('<?php DELETE.' '. DATA.'. '.SELECTED ?> ?');	
}

function sompla()
{
	var txt;
	var r = confirm("Press a button!");
	if (r == true) {
		return true;
	}
	return false;
}

function doConfirm() {
	return confirm('<?php echo CONFIRM.' '.ACTION?> ?');	
}
$(".subscribe").click(function(){
	var the_name = $('#t_name_subscribe').val();
	var the_email = $('#t_email_subscribe').val();
	if (IsEmail(the_email)) {
		$.ajax({
			type: "POST",
			url: "<?php echo base_url()?>ajax",
			data: "apitoken=<?php echo rand(100000000,9999999999);?>&do=subscribe&name="+ the_name +"&email=" + the_email,
			cache: false,
			success: function(resp) {
				if (resp == "<?php echo INFO_SAVE_SUCCESS?>") {
					$('#divSubscribe').html('<div class=""><i class="fa fa-info"></i> &nbsp;<?php echo INFO_EMAIL_SUBSCRIBE_SUCCESS?></div>');
					$('#form_register').html('<div class="bg-primary padMed"><i class="fa fa-info"></i> &nbsp;<?php echo INFO_EMAIL_SUBSCRIBE_SUCCESS?></div>');
				} else {
					if (resp == "<?php echo INFO_EMAIL_SUBSCRIBE_EXIST?>") {
						$('#t_email_subscribe').focus();
					} 
					alert(resp);
				}
			}
		});
	} else {
		$('#t_email_subscribe').focus();
		alert('Email tidak valid.');
	}
});
</script>