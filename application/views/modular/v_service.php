<?php
global $PAGE, $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;

$PAGE = "Service";
$BREADCRUMB = $this->common_model->breadcrumb(NULL, $PAGE);
$PAGE_TITLE = $PAGE.' - '.DEFAULT_PAGE_TITLE;;
$PAGE_HEADER = $PAGE;

$gInfo = '';

$img = base_url().'asset/images/';
?>
<style>
.service{
	width:100%;
	height:180px
}
</style>
<div class="jumbotron">
	<div class="container">
		<div class="row ">
			
				<?php if (isset($BREADCRUMB)) echo $BREADCRUMB.'' ?>
				<?php if (isset($PAGE_HEADER)) echo '<h2>'.$PAGE_HEADER.'</h2>' ?>
			
				<div class="col-sm-4">
					<img src="<?php echo $img.'recruitment_service.jpg';?>" class="img-responsive service" />
				</div>
				<div class="col-sm-8">
					<span class="b">RECRUITMENT SERVICE<br/></span>
					The recruitment process is the first step towards creating the competitive strength and the recruitment strategic advantage for the organizations.  Through this service, we make the recruitment process in accordance with client needs. Our selection process can be done starting from the search candidate until the implementation of the medical examination. We provide comprehensive services in a series of recruitment process. This process allows the efficiency in managing the recruitment process.<br/><br/>
				</div>
				
				<div class="col-sm-12"><br/><br/></div>
				
				<div class="col-sm-4">
					<img src="<?php echo $img.'assessment_center.jpg';?>" class="img-responsive service"/>
				</div>
				<div class="col-sm-8">
					<span class="b">ASSESSMENT CENTER<br/></span>
					Our carefully developed assessment solutions allow you to assess the skills, aptitudes, and abilities of your workforce, enabling you to identify and fill any skills gaps. These services provide a range of examination or psychological evaluation which aims to see the potential employee is usually used in HR development. Often an assessment process undertaken to conduct the selection process, which determines the right to occupy certain positions or positions in accordance with the requirements of the positions have been determined.
					<br/><br/>
				</div>
				
				<div class="col-sm-12"><br/><br/></div>
				
				<div class="col-sm-4">
					<img src="<?php echo $img.'hr_outsourcing.jpg';?>" class="img-responsive service"/>
				</div>
				<div class="col-sm-8">
					<span class="b">HR OUTSOURCING<br/></span>
					We provide companies of all sizes with valuable HR Consultancy advice for a manageable retainer or day to day service. Get the benefit from in-depth advice on best practices and employment regulations. For a growing number of companies, you will get more benefit from a team of professional HR specialists who can lend a helping hand with day-to-day HR duties as well as employee recruitment, retention and management. It is also an attractive, high-quality and cost-effective alternative than set up your own HR department. HR Prime Solution will provide client with our in-house experience HR consultant which can take up various HR tasks, from payroll to benefits to recruiting, to free up a client to focus on its core business. Outsourcing non-core business processes such as Human Resources to us will allows companies to focus on core competencies and free up valuable resources to drive growth and innovation.
					<br/><br/>
				</div>
				
				<div class="col-sm-12"><br/><br/></div>
				
				<div class="col-sm-4">
					<img src="<?php echo $img.'hr_consultancy.jpg';?>" class="img-responsive service"/>
				</div>
				<div class="col-sm-8">
					<span class="b">HR CONSULTANCY<br/></span>
					Our dedicated team of HR experts are on hand to provide professional support for your HR setup requirements. Working closely alongside your organization, we will identify gaps in your current policies and procedures, and propose solutions to overcome these issues. Our area of expertise are start from 
					<br/><br/>
				</div>
				
				<div class="col-sm-12"><br/><br/></div>

				<div class="col-sm-4">
					<img src="<?php echo $img.'training_development.jpg';?>" class="img-responsive service"/>
				</div>
				<div class="col-sm-8">
					<span class="b">TRAINING & DEVELOPMENT<br/></span>
					Investing in your employees shows that you value them and opens communication gateways. It also increases morale, longevity, and reduces turnover. Putting time into employee development is a crucial step in the overall development of any company or business. Our professional training services will enable your organization to optimize the skills, knowledge, and abilities of its workforce. This service aims to facilitate the development of potential employees with the approach of tailor made training. We deliver a wide range of training courses, including public training, in-house training, outbound, and professional coaching.
					<br/><br/>
				</div>
				
				<div class="col-sm-12"><br/><br/></div>
				
				<div class="col-sm-4">
					<img src="<?php echo $img.'executive_search.jpg';?>" class="img-responsive service"/>
				</div>
				<div class="col-sm-8">
					<span class="b">EXECUTIVE SEARCH<br/></span>
					This service aims to find candidates who are in managerial positions from various departments and boards of directors or the CEO in accordance with company requirements. Through databases and networks that we have, we will find and provide some alternative candidate who best meets the qualification requirements of the company.
					<br/><br/>
				</div>
			
		</div>
	</div>
</div>