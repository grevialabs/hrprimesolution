<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER;

$PAGE_HEADER = NULL;
$PAGE = $MODULE = SETTING;
$bread['member'] = 'Member';
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);
$PAGE_TITLE = $PAGE.' - '.DEFAULT_PAGE_TITLE;

global $page, $param, $message;

if ($_POST) {
	$tmp['member_id'] = member_cookies('member_id');
	$obj = $this->member_model->get($tmp);
	if (!empty($obj)) {
		
		$old_password = filter(post('f_old_password'));
		$new_password = filter(post('f_new_password'));
		$confirm_new_password = filter(post('f_confirm_new_password'));
		
		if (encrypt($old_password) == $obj['thepassword']) {
			if ($new_password == $confirm_new_password) {
				$new_password = encrypt($confirm_new_password);
				
				$update = $this->member_model->update($tmp['member_id'],array('thepassword' => $new_password));
				($update)?$message['message'] = MESSAGE::UPDATE : $message['message'] = MESSAGE::ERROR;
				$message['message'] = getMessage($message['message']);
			} else {
				$message['message'] = 'Password baru tidak sesuai';
			}
		} else {
			$message['message'] = 'Password lama salah';
		}
	} else {
		$message['message'] = getMessage(MESSAGE::NOT_FOUND);
	}
}

if ($_POST) {
	unset($_POST['btnUpdate']);
	$post = $_POST;
	$update = $this->config_model->update($post);
	if ($update) redirect(current_url().'?success=1');
	// ($update)?$message['message'] = MESSAGE::UPDATE : $message['message'] = MESSAGE::ERROR;
	// $message['message'] = getMessage($message['message']);
}

global $config;
?>

<div class="col-md-3">
	<?php echo $SIDEMENUBAR?>
</div>

<div class="col-md-9">
	<h1 class=""><?php echo $MODULE?></h1><br/>
	
	<?php if(is_filled($message['message']))echo message($message['message']).BR?>
	<!--
	<form class='form-horizontal' role='form' method='post'>
		<div class='form-group form-group-sm'>
			<label for='f_OldPassword' class='col-sm-2'><?php echo OLD_PASSWORD?></label>
			<div class='col-sm-10'><input type='password' class='form-control' name='f_OldPassword' id='f_old_password' placeholder=' <?php echo INPUT.' '.OLD_PASSWORD?>' required></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_NewPassword' class='col-sm-2'><?php echo NEW_PASSWORD?></label>
			<div class='col-sm-10'><input type='password' class='form-control' name='f_NewPassword' id='f_new_password' placeholder='<?php echo INPUT.' '.NEW_PASSWORD?>' required></div>
		</div>		
		<div class='form-group form-group-sm'>
			<label for='f_ConfirmNewPassword' class='col-sm-2'><?php echo CONFIRM_NEW_PASSWORD?></label>
			<div class='col-sm-10'><input type='password' class='form-control' name='f_ConfirmNewPassword' id='f_confirm_new_password' placeholder='<?php echo INPUT.' '.CONFIRM_NEW_PASSWORD?>' required></div>
		</div>
		<div class='form-group' style='padding-left:15px'>
			<button class='btn btn-success'><?php echo UPDATE?></button>
		</div>
	</form>
	-->
	
	<form method="post">
		<div class="col-sm-12 br-lg">
			<span class="b">Nama Situs</span><br/>
			<input name="site_name" class="input wdtFul" value="<?php if (isset($config['site_name'])) echo $config['site_name'] ?>"/>
		</div>
		
		<div class="col-sm-12 br-lg">
			<span class="b">Deskripsi situs</span><br/>
			<textarea name="site_description" class="input wdtFul" rows="8"><?php if (isset($config['site_description'])) echo $config['site_description'] ?></textarea>
		</div>
		
		<div class="col-sm-6 br-lg"><span class="b">Facebook</span><br/>
			<input class="input wdtFul" type="text" placeholder="facebook" name="facebook" value="<?php if(isset($config['facebook'])) echo $config['facebook']?>" /> 
			<small class="text-muted">
			  Jika url situs anda http://facebook.com/abcde , maka inputlah <b>abcde</b>
			</small>
		</div>
		<div class="col-sm-6"><span class="b">Twitter</span><br/>
			<input class="input wdtFul" type="text" placeholder="twitter" name="twitter" value="<?php if(isset($config['twitter'])) echo $config['twitter']?>" />
			<small class="text-muted">
			  Jika url situs anda http://twitter.com/abcde , maka inputlah <b>abcde</b>
			</small>
		</div>
		
		<div class="col-sm-12 talCnt"></div>
		
		<div class="col-sm-6 br-lg"><span class="b">Instagram</span><br/>
			<input class="input wdtFul" type="text" placeholder="instagram" name="instagram" value="<?php if(isset($config['instagram'])) echo $config['instagram']?>" /> 
			<small class="text-muted">
			  Jika url situs anda http://instagram.com/<b>abcde</b> , maka inputlah <b>abcde</b>
			</small>
		</div>
		<!--
		<div class="col-sm-6"><input class="input wdtFul br" type="text" placeholder="whatsap" name="whatsap" value="<?php if(isset($config['whatsap'])) echo $config['whatsap']?>" /> </div>

		<div class="col-sm-6"><input class="input wdtFul br" type="text" placeholder="bbm" name="bbm" value="<?php if(isset($config['bbm'])) echo $config['bbm']?>" /> </div>
		<div class="col-sm-6"><input class="input wdtFul br" type="text" placeholder="path" name="path" value="<?php if(isset($config['path'])) echo $config['path']?>" /> </div>

		<div class="col-sm-6"><input class="input wdtFul br" type="text" placeholder="youtube" name="youtube" value="<?php if(isset($config['youtube'])) echo $config['youtube']?>" /> </div>
		-->
		<div class="col-sm-6"></div>
		
		<div class="col-sm-12 br-lg">
		<span class="b">Company Address</span> <small>*muncul di halaman footer</small><br/>
		<textarea name="company_description" class="input wdtFul" rows="8"><?php if (isset($config['company_description'])) echo $config['company_description'] ?></textarea>
		</div>
		
		<div class="col-sm-12 talCnt">
			<input class="btn btn-success" type="submit" name="btnUpdate" value="<?php echo UPDATE?>" /><br/><br/>
		</div>
	</form>
</div>