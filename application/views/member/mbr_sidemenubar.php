	<?php
	global $current_segment;
	if ($this->uri->segment(2)) $current_segment = $this->uri->segment(2);
	
	function member_active($page) 
	{
		global $current_segment;
		$str = '';
		if ($current_segment === $page)
		{
			$str = 'active';
		}
		return $str;
	}
	
	function admin_active($page) 
	{
		global $current_segment;
		$str = '';
		if ($current_segment === $page)
		{
			$str = 'active';
		}
		return $str;
	}
	?>
	Hola, Admin<hr>

	<div class="panel panel-primary">
		<div class="list-group">
			<?php if (is_admin()) {?>
			<!--
			<a class="list-group-item bgRed clrWht" href="<?php echo base_url().'admin/dashboard'?>"><i class="fa fa-file-text fa-fw"></i>&nbsp; Adminarea</a>
			<a class="list-group-item <?php echo member_active('test')?>" href="<?php echo base_url().'member/test'?>"><i class="fa fa-file-text fa-fw"></i>&nbsp; <?php echo MENU_TEST?></a>
			-->
			<?php } ?>
			<a class="list-group-item <?php echo member_active('content')?>" href="<?php echo base_url().'member/content'?>"><i class="fa fa-desktop fa-fw"></i>&nbsp; Konten</a>
			<a class="list-group-item <?php echo member_active('image')?>" href="<?php echo base_url().'member/image'?>"><i class="fa fa-image fa-fw"></i>&nbsp; Gambar</a>
			<a class="list-group-item <?php echo member_active('setting')?>" href="<?php echo base_url().'member/setting'?>"><i class="fa fa-cog fa-fw"></i>&nbsp; <?php echo MENU_MY_SETTING?></a>
			<a class="list-group-item <?php echo member_active('logout')?>" href="<?php echo base_url().'logout'?>"><i class="fa fa-user fa-fw"></i>&nbsp; Logout</a>
		</div>
	</div>