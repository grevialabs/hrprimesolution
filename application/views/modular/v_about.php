<?php
global $PAGE, $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;

$PAGE = "About Us";
$BREADCRUMB = $this->common_model->breadcrumb(NULL, $PAGE);
$PAGE_TITLE = $PAGE.' - '.DEFAULT_PAGE_TITLE;
$PAGE_HEADER = $PAGE;
?>

<div class="jumbotron bg-about" >
	<div class="container">
		<div class="rows min-height-250">
			&nbsp;
		</div>
	</div>
</div>
<div class="jumbotron" >
	<div class="container">
		<div class="rows min-height-250">
			<div class="col-sm-12">
			<?php if (isset($BREADCRUMB)) echo $BREADCRUMB.'' ?>
			<?php if (isset($PAGE_HEADER)) echo '<h2>'.$PAGE_HEADER.'</h2>' ?>
			
			HUMAN RESOURCES PRIME SOLUTION provides employers with human resource solutions that result in measurably improved employee and organizational performance while minimizing employment practice risk. We can help organizations large and small establish, outsource and troubleshoot any and all essential Human Resource functions.  Our services ranging from Recruitment, Assessment Center, Human Resource Consultancy, Human Resource Outsourcing in corporate environments from various sectors from the private, public to many major industries and represent diverse business challenges.<br/><br/>
 
			At first, HR PRIME SOLUTION known as the Jakarta Human Consulting established in 2009. As the rapid business growth, in 2013 Jakarta Human Consulting merged become one of the business units of PT. Sinergi Prima Solusi Indonesia and sooner changed its name to HR PRIME SOLUTION.<br/><br/>
			 
			In carrying out our work, we strive to listen to our clients needs and be flexible to be able to provide the best outcome for the client. The program we design for each of our clients are customized and developed specifically to meet the needs of each unique and different organization. Our aim is to help clients develop talent, organize people to be more effective and motivate them to perform at their best. Furthermore, we are going making change happen and helping people and organizations realize their potential. Currently HR PRIME SOLUTION is located at AEKI Building 3rd Floor, Jl. RP. Soeroso No. 20 Cikini – Menteng, Jakarta Pusat.<br/><br/>
			
			<div class="" style="font-size:30px;color:#024089;">Vision & Mission<br/></div><br/>
					<span class="b">Vision</span><br/>
					To be a leading corporate entity constantly innovating to provide comprehensive, creative HR Solutions to our customers.<br/><br/>
					<span class="b">Mission</span><br/>
					We aim to build a customer-centric relationship with our clients, innovating constantly to provide them with real world human resource solutions, thus enabling them to focus on their core business and strengthen their competitive advantages to boost business results.<br/><br/>
			
			</div>
		</div>
	</div>
</div>