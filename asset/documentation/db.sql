

-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.27 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table grv.grv_forumcategory
CREATE TABLE IF NOT EXISTS `grv_forumcategory` (
  `ForumCategoryID` mediumint(3) NOT NULL AUTO_INCREMENT,
  `ParentCategoryID` mediumint(3) DEFAULT NULL,
  `Name` varchar(100) NOT NULL,
  `Description` varchar(255) NOT NULL,
  PRIMARY KEY (`ForumCategoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Dumping data for table grv.grv_forumcategory: ~0 rows (approximately)
/*!40000 ALTER TABLE `grv_forumcategory` DISABLE KEYS */;
INSERT IGNORE INTO `grv_forumcategory` (`ForumCategoryID`, `ParentCategoryID`, `Name`, `Description`) VALUES
	(1, NULL, 'Forum KitaKita', ''),
	(2, 1, 'Citizen Journalism', 'Forum berita sehari-hari(topik bebas) dari member untuk member'),
	(3, 1, 'Latihan Posting', 'Forum untuk latihan posting bagi para member baru'),
	(4, NULL, 'Jual Beli', ''),
	(5, 4, 'Area Jual Beli', 'Forum seputar jual beli barang dan jasa'),
	(6, 1, 'Kuliah', 'Forum diskusi seputar kuliah dan masalah seputar dunia kampus'),
	(7, 1, 'Promosi dan Diskon', 'Forum seputar diskon dan promosi'),
	(8, 1, 'Kritik dan Saran', 'Forum untuk masukan dan kritik tentang forum atau website kisi-kisi.'),
	(9, 1, 'Surat Terbuka Pembaca', 'Forum seputar keluhan untuk organisasi atau individu atas pelayanannya.'),
	(10, 1, 'Curhat', 'Forum seputar curahan hati dan prahara.');
/*!40000 ALTER TABLE `grv_forumcategory` ENABLE KEYS */;


-- Dumping structure for table grv.grv_forumtopic
CREATE TABLE IF NOT EXISTS `grv_forumtopic` (
  `ForumTopicID` int(7) NOT NULL AUTO_INCREMENT,
  `ForumCategoryID` mediumint(3) NOT NULL,
  `Topic` varchar(255) NOT NULL,
  `CreatorID` varchar(8) NOT NULL,
  `CreatorDateTime` datetime NOT NULL,
  `CreatorIP` varchar(20) NOT NULL,
  `View` mediumint(7) NOT NULL,
  `ReplyCount` smallint(5) NOT NULL,
  `EditorID` varchar(20) DEFAULT NULL,
  `EditorDateTime` datetime DEFAULT NULL,
  `EditorIP` varchar(20) DEFAULT NULL,
  `IsLocked` smallint(1) NOT NULL,
  `IsSticky` smallint(1) NOT NULL,
  `PrefixBuySell` smallint(1) DEFAULT '0',
  `PrefixItemCondition` smallint(1) DEFAULT '0',
  `PrefixLocation` smallint(2) DEFAULT '0',
  PRIMARY KEY (`ForumTopicID`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;

-- Dumping data for table grv.grv_forumtopic: ~0 rows (approximately)
/*!40000 ALTER TABLE `grv_forumtopic` DISABLE KEYS */;
INSERT IGNORE INTO `grv_forumtopic` (`ForumTopicID`, `ForumCategoryID`, `Topic`, `CreatorID`, `CreatorDateTime`, `CreatorIP`, `View`, `ReplyCount`, `EditorID`, `EditorDateTime`, `EditorIP`, `IsLocked`, `IsSticky`, `PrefixBuySell`, `PrefixItemCondition`, `PrefixLocation`) VALUES
	(1, 3, 'Bakmi 213 Cirebon Promo Gulai Empal + Lemon Tea For Rp. 15.000 Only', '1', '2011-09-28 22:01:49', '', 3, 7, '1', '2011-10-08 10:58:38', '127.0.0.1', 0, 0, 0, 0, 0),
	(3, 3, 'Panduan Sebelum Membuat Topik Jual Beli a', '1', '2011-10-03 23:58:45', '', 824, 27, '1', '2012-03-03 18:01:46', '127.0.0.1', 0, 1, 0, 0, 0),
	(4, 3, 'Trit Pertamax', '1', '2011-10-04 00:51:05', '', 73, 6, '2', '2011-10-08 10:03:47', '127.0.0.1', 1, 0, 0, 0, 0),
	(5, 3, 'Bisa ga nih', '1', '2011-10-04 00:33:08', '', 61, 10, '1', '2011-11-25 21:29:06', '127.0.0.1', 0, 0, 0, 0, 0),
	(6, 2, 'Forum Baru: Debat Opini', '1', '2011-10-07 20:29:18', '', 151, 9, '1', '2011-12-31 11:42:37', '127.0.0.1', 0, 0, 0, 0, 0),
	(7, 3, 'Get The Best Wagyu Stick Only At Platinum for Only 20.000 From 35.000 ', '1', '2011-11-12 17:21:49', '', 77, 16, '1', '2011-12-03 15:08:52', '127.0.0.1', 0, 0, 0, 0, 0),
	(8, 2, 'latihan testing', '1', '2011-11-23 22:08:36', '', 12, 0, '1', '2011-11-23 22:08:36', '127.0.0.1', 1, 0, 0, 0, 0),
	(9, 1, 'iseng aja nih promo', '1', '2011-12-21 23:31:09', '', 0, 0, '1', '2011-12-21 23:31:09', '127.0.0.1', 0, 0, 0, 0, 0),
	(10, 1, 'iseng aja nih promo', '1', '2011-12-21 23:30:11', '', 0, 0, '1', '2011-12-21 23:30:11', '127.0.0.1', 0, 0, 0, 0, 0),
	(11, 3, 'iseng aja nih promo', '1', '2011-12-21 23:32:18', '', 3, 0, '1', '2011-12-21 23:32:18', '127.0.0.1', 0, 0, 0, 0, 0),
	(12, 3, 'iseng aja nih promo', '1', '2011-12-21 23:33:19', '', 1, 0, '1', '2011-12-21 23:33:19', '127.0.0.1', 0, 0, 0, 0, 0),
	(13, 3, 'iseng aja nih promo', '1', '2011-12-21 23:37:09', '', 0, 0, '1', '2011-12-21 23:37:09', '127.0.0.1', 0, 0, 0, 0, 0),
	(14, 3, 'iseng aja nih promo', '1', '2011-12-21 23:38:02', '', 0, 0, '1', '2011-12-21 23:38:02', '127.0.0.1', 0, 0, 0, 0, 0),
	(15, 3, 'iseng aja nih promo', '1', '2011-12-21 23:39:31', '', 0, 0, '1', '2011-12-21 23:39:31', '127.0.0.1', 0, 0, 0, 0, 0),
	(16, 3, 'nih platinum', '1', '2011-12-21 23:42:54', '', 1, 0, '1', '2011-12-21 23:42:54', '127.0.0.1', 0, 0, 0, 0, 0),
	(17, 3, 'nih platinum', '1', '2011-12-21 23:43:12', '', 0, 0, '1', '2011-12-21 23:43:12', '127.0.0.1', 0, 0, 0, 0, 0),
	(18, 3, 'YURAKU BANDUNG test ', '1', '2011-12-22 11:12:22', '', 0, 0, '1', '2011-12-22 11:12:22', '127.0.0.1', 0, 0, 0, 0, 0),
	(19, 3, 'BAKMI BANDUNG test ', '1', '2011-12-22 11:18:13', '', 5, 1, '1', '2011-12-22 11:23:40', '127.0.0.1', 0, 0, 0, 0, 0),
	(20, 3, 'Bakmi keblenger cirebon + nasi', '1', '2011-12-24 14:09:41', '', 10, 1, '1', '2011-12-24 16:05:37', '127.0.0.1', 0, 0, 0, 0, 0),
	(21, 3, 'Nasi keblenger cirebon + ramen', '1', '2011-12-24 14:29:11', '', 1, 0, '1', '2011-12-24 14:29:11', '127.0.0.1', 0, 0, 0, 0, 0),
	(22, 3, 'Nasi keblenger cirebon + ramen', '1', '2011-12-24 14:36:52', '', 0, 0, '1', '2011-12-24 14:36:52', '127.0.0.1', 0, 0, 0, 0, 0),
	(23, 3, 'Nasi keblenger cirebon + ramen', '1', '2011-12-24 14:37:20', '', 1, 0, '1', '2011-12-24 14:37:20', '127.0.0.1', 0, 0, 0, 0, 0),
	(25, 4, 'test', '4', '2012-01-02 10:04:37', '', 57, 6, '1', '2012-01-14 10:41:17', '127.0.0.1', 0, 0, 0, 0, 0),
	(27, 3, 'Get The Best Wagyu Stick Only At Platinum for Only 20.000 From 35.000', '1', '2012-01-07 22:32:46', '', 29, 1, '1', '2012-01-10 22:11:35', '127.0.0.1', 0, 0, 0, 0, 0),
	(28, 2, 'baru', '1', '2012-01-11 00:58:05', '127.0.0.1', 35, 0, '1', '2012-01-11 00:58:05', '127.0.0.1', 0, 1, 0, 0, 0),
	(29, 3, 'Promo Arcade Angry Birds for IDR 99.000 only', '1', '2012-01-15 23:21:33', '127.0.0.1', 17, 0, '1', '2012-01-15 23:21:33', '127.0.0.1', 0, 0, 0, 0, 0),
	(30, 2, 'Menjadi jablay', '4', '2012-01-29 22:32:40', '127.0.0.1', 33, 1, '4', '2012-01-29 22:40:10', '127.0.0.1', 0, 0, 0, 0, 0),
	(32, 5, 'Panduan Sebelum Membuat Topik Jual Beli a', '1', '2012-02-02 10:25:30', '127.0.0.1', 118, 8, '1', '2012-03-03 10:46:04', '127.0.0.1', 0, 1, 0, 0, 0),
	(33, 5, 'jual diri', '1', '2012-02-02 10:26:09', '127.0.0.1', 17, 1, '1', '2012-02-07 09:17:15', '127.0.0.1', 0, 0, 0, 0, 0),
	(34, 2, 'wewew', '1', '2012-02-04 19:25:08', '127.0.0.1', 1, 4, NULL, NULL, NULL, 0, 0, 0, 0, 0),
	(35, 3, 'belajar posting yu', '1', '2012-03-03 18:15:23', '127.0.0.1', 19, 0, '1', '2012-03-03 18:29:40', '127.0.0.1', 1, 1, 0, 0, 0),
	(36, 2, 'inilah umum', '1', '2012-03-03 23:48:18', '127.0.0.1', 2, 2, '1', '2012-03-03 23:54:19', '127.0.0.1', 0, 0, 0, 0, 0),
	(37, 3, 'Tas Lucu Angry Birds Beludru 5 warna HANYA 89.000', '1', '2012-03-05 20:45:07', '127.0.0.1', 2, 0, '1', '2012-03-05 20:45:07', '127.0.0.1', 0, 0, 0, 0, 0),
	(38, 3, 'Tas Lucu Angry Birds Beludru Dengan 5 Pilihan Warna dan Karakter Seharga 89.000', '1', '2012-03-05 21:23:56', '127.0.0.1', 0, 0, '1', '2012-03-05 21:23:56', '127.0.0.1', 0, 0, 0, 0, 0),
	(39, 3, 'Tas Lucu Angry Birds Beludru Dengan 5 Pilihan Warna dan Karakter Seharga 89.000', '1', '2012-03-05 21:27:00', '127.0.0.1', 0, 0, '1', '2012-03-05 21:27:00', '127.0.0.1', 0, 0, 0, 0, 0),
	(40, 3, 'mi cirebon ga jelas 25000', '1', '2012-03-05 21:36:08', '127.0.0.1', 1, 0, '1', '2012-03-05 21:36:08', '127.0.0.1', 0, 0, 0, 0, 0),
	(41, 3, 'mi cirebon ga jelas 25000', '1', '2012-03-05 21:40:33', '127.0.0.1', 0, 0, '1', '2012-03-05 21:40:33', '127.0.0.1', 0, 0, 0, 0, 0),
	(42, 3, 'mi cirebon ga jelas 25000', '1', '2012-03-05 22:17:01', '127.0.0.1', 0, 0, '1', '2012-03-05 22:17:01', '127.0.0.1', 0, 0, 0, 0, 0),
	(43, 3, 'mi cirebon ga jelas 25000', '1', '2012-03-05 22:17:33', '127.0.0.1', 0, 0, '1', '2012-03-05 22:17:33', '127.0.0.1', 0, 0, 0, 0, 0),
	(44, 3, 'mi cirebon ga jelas 25000', '1', '2012-03-05 22:17:57', '127.0.0.1', 0, 0, '1', '2012-03-05 22:17:57', '127.0.0.1', 0, 0, 0, 0, 0),
	(45, 3, 'mi cirebon ga jelas 25000', '1', '2012-03-05 22:18:14', '127.0.0.1', 0, 0, '1', '2012-03-05 22:18:14', '127.0.0.1', 0, 0, 0, 0, 0),
	(46, 3, 'mi cirebon ga jelas 25000', '1', '2012-03-05 22:18:48', '127.0.0.1', 0, 0, '1', '2012-03-05 22:18:48', '127.0.0.1', 0, 0, 0, 0, 0),
	(47, 3, 'mi cirebon ga jelas 25000', '1', '2012-03-05 22:20:20', '127.0.0.1', 0, 0, '1', '2012-03-05 22:20:20', '127.0.0.1', 0, 0, 0, 0, 0),
	(48, 3, 'mi cirebon ga jelas 25000', '1', '2012-03-06 08:06:53', '127.0.0.1', 0, 0, '1', '2012-03-06 08:06:53', '127.0.0.1', 0, 0, 0, 0, 0),
	(49, 3, 'mi cirebon ga jelas 25000', '1', '2012-03-06 08:08:02', '127.0.0.1', 0, 0, '1', '2012-03-06 08:08:02', '127.0.0.1', 0, 0, 0, 0, 0),
	(50, 3, 'mi cirebon ga jelas 25000', '1', '2012-03-06 08:17:31', '127.0.0.1', 0, 0, '1', '2012-03-06 08:17:31', '127.0.0.1', 0, 0, 0, 0, 0),
	(51, 3, 'mi cirebon ga jelas 25000', '1', '2012-03-06 08:18:56', '127.0.0.1', 0, 0, '1', '2012-03-06 08:18:56', '127.0.0.1', 0, 0, 0, 0, 0),
	(52, 3, 'Tas Lucu Angry Birds Beludru Dengan 5 Pilihan Warna dan Karakter Seharga 89.000', '1', '2012-03-06 08:35:30', '127.0.0.1', 11, 0, '1', '2012-03-06 08:35:30', '127.0.0.1', 0, 0, 0, 0, 0),
	(53, 5, 'Tas Lucu Angry Birds Beludru Dengan 5 Pilihan Warna dan Karakter Seharga 89.000', '1', '2012-03-06 09:10:29', '127.0.0.1', 10, 0, '1', '2012-03-06 09:10:29', '127.0.0.1', 0, 0, 0, 0, 0),
	(54, 7, 'Tas Lucu Angry Birds Beludru Dengan 5 Pilihan Warna dan Karakter Seharga 89.000', '1', '2012-03-06 09:13:07', '127.0.0.1', 12, 0, '1', '2012-03-06 09:13:07', '127.0.0.1', 0, 0, 0, 0, 0),
	(55, 5, 'jual barang lama', '1', '2012-03-19 23:44:35', '127.0.0.1', 3, 25, '1', '2012-03-22 22:01:23', '127.0.0.1', 0, 0, 0, 0, 0),
	(56, 5, 'test', '1', '2012-03-25 15:45:19', '127.0.0.1', 10, 0, '1', '2012-03-25 15:45:19', '127.0.0.1', 0, 0, 1, 1, 4),
	(57, 1, 'ini yang paling baru', '1', '2012-04-29 21:43:38', '127.0.0.1', 15, 0, '1', '2012-04-29 21:43:38', '127.0.0.1', 0, 0, 0, 0, 0);
/*!40000 ALTER TABLE `grv_forumtopic` ENABLE KEYS */;


-- Dumping structure for table grv.grv_forumtopicdetail
CREATE TABLE IF NOT EXISTS `grv_forumtopicdetail` (
  `ForumTopicDetailID` int(7) NOT NULL AUTO_INCREMENT,
  `ForumTopicID` int(7) NOT NULL,
  `Content` text NOT NULL,
  `CreatorID` varchar(8) NOT NULL,
  `CreatorDateTime` datetime NOT NULL,
  `CreatorIP` varchar(20) NOT NULL,
  `EditorID` varchar(8) DEFAULT NULL,
  `EditorIP` varchar(20) DEFAULT NULL,
  `EditorDateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ForumTopicDetailID`)
) ENGINE=InnoDB AUTO_INCREMENT=217 DEFAULT CHARSET=utf8;

-- Dumping data for table grv.grv_forumtopicdetail: ~0 rows (approximately)
/*!40000 ALTER TABLE `grv_forumtopicdetail` DISABLE KEYS */;
INSERT IGNORE INTO `grv_forumtopicdetail` (`ForumTopicDetailID`, `ForumTopicID`, `Content`, `CreatorID`, `CreatorDateTime`, `CreatorIP`, `EditorID`, `EditorIP`, `EditorDateTime`) VALUES
	(1, 1, 'Selamat datang, perkenalan.', '1', '2011-09-28 22:02:10', '127.0.0.1 ', '0', '', '0000-00-00 00:00:00'),
	(2, 1, 'testing', '1', '2011-09-30 01:00:54', '127.0.0.1', NULL, NULL, NULL),
	(3, 1, 'test', '1', '2011-09-30 02:05:01', '127.0.0.1', NULL, NULL, NULL),
	(4, 1, 'hehe', '1', '2011-10-02 21:14:32', '127.0.0.1', NULL, NULL, NULL),
	(5, 1, 'hehe', '1', '2011-10-02 21:35:37', '127.0.0.1', NULL, NULL, NULL),
	(6, 1, 'Ini adalah isi coba bsa ga sih ??', '1', '2011-10-03 23:58:45', '127.0.0.1', NULL, NULL, NULL),
	(7, 4, 'harusnya da bisa\r\n\r\n\r\ngw mabok juga ni', '1', '2011-10-04 00:51:05', '127.0.0.1', NULL, NULL, NULL),
	(8, 4, 'harusnya da bisa\r\n\r\n\r\ngw mabok juga ni', '1', '2011-10-04 00:51:05', '127.0.0.1', NULL, NULL, NULL),
	(10, 1, 'testing tengah malam', '1', '2011-10-05 00:57:47', '127.0.0.1', NULL, NULL, NULL),
	(11, 4, '', '1', '2011-10-06 13:36:58', '127.0.0.1', NULL, NULL, NULL),
	(13, 6, '[b]coba ini ni bos :D[/b]', '1', '2011-10-07 20:54:21', '127.0.0.1', NULL, NULL, NULL),
	(14, 6, '', '1', '2011-10-07 22:32:48', '127.0.0.1', NULL, NULL, NULL),
	(15, 6, '', '1', '2011-10-07 22:34:48', '127.0.0.1', NULL, NULL, NULL),
	(18, 4, 'hehe keempax...\r\n\r\nakhirnya jadi juga forumnya hehe', '2', '2011-10-08 09:04:08', '127.0.0.1', NULL, NULL, NULL),
	(19, 4, 'test', '1', '2011-10-08 10:55:37', '127.0.0.1', NULL, NULL, NULL),
	(20, 4, 'hehe', '2', '2011-10-08 10:03:47', '127.0.0.1', NULL, NULL, NULL),
	(21, 5, 'awaw', '1', '2011-10-08 13:55:26', '127.0.0.1', NULL, NULL, NULL),
	(22, 5, '[quote]bisa ga nih[/quote]', '1', '2011-10-08 15:05:00', '127.0.0.1', NULL, NULL, NULL),
	(23, 5, '[quote]bisa ga nih[/quote]', '1', '2011-10-08 15:50:30', '127.0.0.1', NULL, NULL, NULL),
	(24, 5, '[b][/b][b][/b][b][/b][b][/b]awdawd', '1', '2011-10-08 15:36:38', '127.0.0.1', NULL, NULL, NULL),
	(25, 5, '[b]bisa ga[/b]', '1', '2011-10-08 15:51:38', '127.0.0.1', NULL, NULL, NULL),
	(26, 5, '[u][b][i]testing[/i][/b][/u]', '1', '2011-10-08 15:34:54', '127.0.0.1', NULL, NULL, NULL),
	(27, 5, '[url]aaaa[/url]', '1', '2011-10-08 16:52:03', '127.0.0.1', NULL, NULL, NULL),
	(28, 5, 'makan nih [url]http://localhost/kisi-kisi.com/showtopic.php?t=5[/url]', '1', '2011-10-08 16:07:05', '127.0.0.1', NULL, NULL, NULL),
	(30, 3, 'hai', '1', '2011-10-08 19:39:42', '127.0.0.1', '1', '127.0.0.1', '2012-03-03 18:01:46'),
	(36, 3, 'oioio\r\n', '1', '2011-10-08 20:11:28', '127.0.0.1', NULL, NULL, NULL),
	(37, 3, 'hehe \r\n\r\noioi', '1', '2011-10-08 20:25:29', '127.0.0.1', '1', '127.0.0.1', '2011-10-08 20:32:29'),
	(38, 3, 'hehe bisa jgua nih hehe', '1', '2011-10-08 20:20:31', '127.0.0.1', '1', '127.0.0.1', '2011-10-21 01:05:58'),
	(39, 3, 'hai bisa ga wewew', '1', '2011-10-08 20:48:31', '127.0.0.1', '1', '127.0.0.1', '2011-10-08 20:52:31'),
	(40, 3, 'a', '1', '2011-10-08 20:03:43', '127.0.0.1', NULL, NULL, NULL),
	(41, 3, 'awda', '1', '2011-10-08 20:05:43', '127.0.0.1', NULL, NULL, NULL),
	(42, 3, 'adw', '1', '2011-10-08 20:08:43', '127.0.0.1', NULL, NULL, NULL),
	(43, 3, 'awd', '1', '2011-10-08 20:11:43', '127.0.0.1', NULL, NULL, NULL),
	(44, 3, 'bisa nih', '1', '2011-10-08 20:15:43', '127.0.0.1', NULL, NULL, NULL),
	(45, 3, 'awdaw', '1', '2011-10-08 20:19:43', '127.0.0.1', NULL, NULL, NULL),
	(46, 3, 'adaw', '1', '2011-10-08 20:23:43', '127.0.0.1', NULL, NULL, NULL),
	(47, 3, 'awda', '1', '2011-10-08 20:26:43', '127.0.0.1', NULL, NULL, NULL),
	(49, 3, 'adwa', '1', '2011-10-08 20:35:47', '127.0.0.1', NULL, NULL, NULL),
	(50, 3, 'awd', '1', '2011-10-08 20:37:47', '127.0.0.1', NULL, NULL, NULL),
	(51, 3, 'awd', '1', '2011-10-08 20:38:47', '127.0.0.1', NULL, NULL, NULL),
	(52, 3, 'awd', '1', '2011-10-08 20:39:47', '127.0.0.1', NULL, NULL, NULL),
	(53, 3, 'awd', '1', '2011-10-08 20:41:47', '127.0.0.1', NULL, NULL, NULL),
	(54, 3, 'aaa', '1', '2011-10-08 20:42:47', '127.0.0.1', NULL, NULL, NULL),
	(55, 3, '[quote]hehe ini quote[/quote]', '1', '2011-10-08 23:54:55', '127.0.0.1', NULL, NULL, NULL),
	(56, 3, '[quote][quote]hehe ini quote[/quote] dalem quote lagi haha[/quote] bisa ga nih', '1', '2011-10-09 00:11:22', '127.0.0.1', NULL, NULL, NULL),
	(57, 6, '[quote]test[/quote]', '1', '2011-10-09 10:48:34', '127.0.0.1', '1', '127.0.0.1', '2011-10-09 10:02:35'),
	(58, 6, '[quote][quote]test[/quote][/quote]\r\n\r\nakhirnya bisa juga ni quote\r\n\r\nmantab brada...', '1', '2011-10-09 10:20:35', '127.0.0.1', NULL, NULL, NULL),
	(59, 6, 'keenamx', '1', '2011-10-09 10:08:56', '127.0.0.1', NULL, NULL, NULL),
	(60, 6, '[quote][quote][quote]test[/quote][/quote]\r\n\r\nakhirnya bisa juga ni quote\r\n\r\nmantab brada...[/quote]\r\nbisa ga sih nested begini &lt;script&gt;iseng aja&lt;/script&gt;', '1', '2011-11-02 23:17:50', '127.0.0.1', NULL, NULL, NULL),
	(61, 6, '[quote][quote][quote]test[/quote][/quote]\r\n\r\nakhirnya bisa juga ni quote\r\n\r\nmantab brada...[/quote]', '1', '2011-11-02 23:17:54', '127.0.0.1', NULL, NULL, NULL),
	(62, 3, '[quote]hehe \r\n\r\noioi[/quote]\r\nbisa ga sih', '1', '2011-11-09 22:48:58', '127.0.0.1', NULL, NULL, NULL),
	(63, 3, 'test', '1', '2011-11-09 22:54:33', '127.0.0.1', NULL, NULL, NULL),
	(64, 3, 'test', '1', '2011-11-09 22:55:39', '127.0.0.1', NULL, NULL, NULL),
	(65, 3, 'bisa kan ??? :D', '1', '2011-11-09 22:55:54', '127.0.0.1', NULL, NULL, NULL),
	(70, 7, '[img]platinum.jpg[/img]<br><br>Dapatkan steak wagyu premium di Platinum cabang mana aja Guys..<br>', '1', '2011-11-12 17:06:32', '127.0.0.1', NULL, NULL, NULL),
	(71, 7, '			test', '1', '2011-11-23 10:32:38', '127.0.0.1', NULL, NULL, NULL),
	(72, 7, '			test', '1', '2011-11-23 10:35:09', '127.0.0.1', NULL, NULL, NULL),
	(73, 7, '			aaa', '1', '2011-11-23 10:36:12', '127.0.0.1', NULL, NULL, NULL),
	(74, 7, 'testaaa', '1', '2011-11-23 10:42:00', '127.0.0.1', NULL, NULL, NULL),
	(75, 7, '			wewew', '1', '2011-11-23 10:43:57', '127.0.0.1', NULL, NULL, NULL),
	(76, 7, '			ww', '1', '2011-11-23 10:43:59', '127.0.0.1', NULL, NULL, NULL),
	(77, 7, '			aa', '1', '2011-11-23 10:44:01', '127.0.0.1', NULL, NULL, NULL),
	(78, 7, 'testtttt', '1', '2011-11-23 10:45:48', '127.0.0.1', NULL, NULL, NULL),
	(79, 7, 'testtttt', '1', '2011-11-23 10:45:57', '127.0.0.1', NULL, NULL, NULL),
	(80, 7, '			aa', '1', '2011-11-23 10:46:00', '127.0.0.1', NULL, NULL, NULL),
	(81, 7, 'bisa nih harusnya 63', '1', '2011-11-23 18:39:44', '127.0.0.1', NULL, NULL, NULL),
	(82, 7, 'bisa lagii', '1', '2011-11-23 18:42:26', '127.0.0.1', NULL, NULL, NULL),
	(83, 7, 'harusbisa', '1', '2011-11-23 18:49:15', '127.0.0.1', NULL, NULL, NULL),
	(84, 7, '123', '1', '2011-11-23 18:50:01', '127.0.0.1', NULL, NULL, NULL),
	(85, 7, 'ooo', '1', '2011-11-23 18:55:50', '127.0.0.1', NULL, NULL, NULL),
	(86, 7, 'qwe', '1', '2011-11-23 18:55:55', '127.0.0.1', NULL, NULL, NULL),
	(87, 7, 'qwe', '1', '2011-11-23 18:55:58', '127.0.0.1', NULL, NULL, NULL),
	(88, 7, 'wwew', '1', '2011-11-23 19:01:46', '127.0.0.1', NULL, NULL, NULL),
	(89, 7, 'aa', '1', '2011-11-23 19:03:10', '127.0.0.1', NULL, NULL, NULL),
	(90, 7, 'aaa', '1', '2011-11-23 19:05:50', '127.0.0.1', NULL, NULL, NULL),
	(91, 5, 'test', '1', '2011-11-23 20:34:00', '127.0.0.1', NULL, NULL, NULL),
	(92, 8, '[u]ini underline[/u]\r\n\r\n[quote]ini quote ni[/quote]\r\n\r\n[url]test[/url]', '1', '2011-11-23 22:08:36', '127.0.0.1', NULL, NULL, NULL),
	(93, 5, 'testlagi', '1', '2011-11-25 21:28:45', '127.0.0.1', NULL, NULL, NULL),
	(94, 5, 'coba lagi', '1', '2011-11-25 21:29:06', '127.0.0.1', NULL, NULL, NULL),
	(95, 7, 'wah manteb nich pastinya langsung dateng buat nyobain promo wagyu sticknya yang dijamin maknyozzz :D', '1', '2011-12-03 10:01:13', '127.0.0.1', NULL, NULL, NULL),
	(96, 7, 'masih ada ga nih promonya :D ???', '1', '2011-12-03 15:08:52', '127.0.0.1', NULL, NULL, NULL),
	(97, 9, '[img]test.jpg[/img]<br><br>promo gila gilaan<br>', '1', '2011-12-21 23:24:18', '127.0.0.1', NULL, NULL, NULL),
	(98, 9, '[img]test.jpg[/img]<br><br>promo gila gilaan<br>', '1', '2011-12-21 23:24:50', '127.0.0.1', NULL, NULL, NULL),
	(99, 10, '[img]test.jpg[/img]<br><br>promo gila gilaan<br>', '1', '2011-12-21 23:31:09', '127.0.0.1', NULL, NULL, NULL),
	(100, 12, '[img]test.jpg[/img]<br><br>promo gila gilaan<br>', '1', '2011-12-21 23:32:18', '127.0.0.1', NULL, NULL, NULL),
	(101, 13, '[img]test.jpg[/img]\r\n\r\npromo gila gilaan\r\n', '1', '2011-12-21 23:33:19', '127.0.0.1', NULL, NULL, NULL),
	(102, 14, '[img]test.jpg[/img]<br><br>promo gila gilaan<br>', '1', '2011-12-21 23:37:09', '127.0.0.1', NULL, NULL, NULL),
	(103, 15, '[img]test.jpg[/img]<br><br>promo gila gilaan<br>', '1', '2011-12-21 23:38:02', '127.0.0.1', NULL, NULL, NULL),
	(104, 16, '[img]test.jpg[/img]<br><br>promo gila gilaan<br>', '1', '2011-12-21 23:39:31', '127.0.0.1', NULL, NULL, NULL),
	(105, 17, '[img]bboong.jpg[/img]<br><br>desckripi platinum<br>', '1', '2011-12-21 23:42:54', '127.0.0.1', NULL, NULL, NULL),
	(106, 18, '[img]bboong.jpg[/img]<br><br>desckripi platinum<br>', '1', '2011-12-21 23:43:12', '127.0.0.1', NULL, NULL, NULL),
	(107, 19, '[img]gada.jpg[/img]<br><br>deskripsi<br>', '1', '2011-12-22 11:12:22', '127.0.0.1', NULL, NULL, NULL),
	(108, 20, '[img]gada.jpg[/img]<br><br>deskripsi<br>', '1', '2011-12-22 11:18:13', '127.0.0.1', NULL, NULL, NULL),
	(109, 19, 'haha akhirnya eror juga', '1', '2011-12-22 11:23:40', '127.0.0.1', NULL, NULL, NULL),
	(110, 20, '[img]iseng.jpg[/img]<br><br>kapan lagi dapat promo bakmi keblenger cirebon + nasi dengan harga 25.000<br>', '1', '2011-12-24 14:09:41', '127.0.0.1', NULL, NULL, NULL),
	(111, 21, '[img]iseng.jpg[/img]<br><br>kapan lagi dapat promo nasi keblenger cirebon + ramen dengan harga 37.000<br>', '1', '2011-12-24 14:29:11', '127.0.0.1', NULL, NULL, NULL),
	(112, 22, '[img]iseng.jpg[/img]<br><br>kapan lagi dapat promo nasi keblenger cirebon + ramen dengan harga 37.000<br>', '1', '2011-12-24 14:36:52', '127.0.0.1', NULL, NULL, NULL),
	(113, 23, '[img]iseng.jpg[/img]<br><br>kapan lagi dapat promo nasi keblenger cirebon + ramen dengan harga 37.000<br>', '1', '2011-12-24 14:37:20', '127.0.0.1', NULL, NULL, NULL),
	(115, 6, 'test', '1', '2011-12-31 11:42:37', '127.0.0.1', NULL, NULL, NULL),
	(117, 25, 'bisa ga sih hehe gw penasaran', '4', '2012-01-02 10:04:37', '127.0.0.1', '1', '127.0.0.1', '2012-01-10 13:03:37'),
	(118, 25, 'te', '4', '2012-01-02 10:04:58', '127.0.0.1', NULL, NULL, NULL),
	(120, 27, '[img]http://static.kisi-kisi.com/images/promo/sapi-bakar-set-platinum.jpg[/img]\r\n\r\nget platinum boo\r\n\r\ntesting lagi\r\n[url]http://www.kisi-kisi.com/promo/promo.php?p=12[/url]', '1', '2012-01-07 22:32:46', '127.0.0.1', '1', '127.0.0.1', '2012-01-10 22:04:33'),
	(121, 25, 'testaa  aadwa', '1', '2012-01-10 12:30:22', '127.0.0.1', '1', '127.0.0.1', '2012-01-10 13:02:44'),
	(122, 25, 'hahaha okokadawd', '1', '2012-01-10 12:45:27', '127.0.0.1', '1', '127.0.0.1', '2012-01-10 13:03:14'),
	(123, 25, 'wew adwa', '1', '2012-01-10 13:03:53', '127.0.0.1', '1', '127.0.0.1', '2012-01-10 13:06:01'),
	(124, 25, 'aa wd', '1', '2012-01-10 13:07:24', '127.0.0.1', '1', '127.0.0.1', '2012-01-10 13:11:45'),
	(129, 27, '[img]http://static.kisi-kisi.com/images/data/terapi-ikan-191011.jpg[/img]', '1', '2012-01-10 22:11:35', '127.0.0.1', NULL, NULL, NULL),
	(130, 27, '[img]http://static.kisi-kisi.com/images/promo/blackberry-bellagio.jpg[/img]<br><br>Dear Kisiholic, dalam rangka merayakan peluncuran website promo and deals, kita dari Kisimedia Networks uda siapin 1 Blackberry Bellagio khusus untuk pemesan voucher dari website kisi-kisi secara [b]GRATIS[/b]. \r\n\r\nVoucher yang tersedia hanya 1000 lembar. Jadi, tidak ada salahnya mencoba peruntungan anda bukan ?\r\n\r\nSemua voucher akan diundi dan ditayangkan pemenangnya pada tanggal jatuh tempo yang ditentukan di poin dibawah ini.\r\n\r\nSyaratnya gampang banget:\r\n- promo dibuka selama 2 bulan \r\n- wajib menjadi member\r\n- membeli voucher melalui form yang tersedia.\r\n\r\nBuruan pesan dan tunggu promo-promo diskon kita yang udah siap menanti kalian guys.<br>', '1', '2012-01-10 23:18:09', '127.0.0.1', NULL, NULL, NULL),
	(131, 27, '[img]http://static.kisi-kisi.com/images/promo/blackberry-bellagio.jpg[/img]<br><br>Dear Kisiholic, dalam rangka merayakan peluncuran website promo and deals, kita dari Kisimedia Networks uda siapin 1 Blackberry Bellagio khusus untuk pemesan voucher dari website kisi-kisi secara [b]GRATIS[/b]. \r\n\r\nVoucher yang tersedia hanya 1000 lembar. Jadi, tidak ada salahnya mencoba peruntungan anda bukan ?\r\n\r\nSemua voucher akan diundi dan ditayangkan pemenangnya pada tanggal jatuh tempo yang ditentukan di poin dibawah ini.\r\n\r\nSyaratnya gampang banget:\r\n- promo dibuka selama 2 bulan \r\n- wajib menjadi member\r\n- membeli voucher melalui form yang tersedia.\r\n\r\nBuruan pesan dan tunggu promo-promo diskon kita yang udah siap menanti kalian guys.<br>', '1', '2012-01-10 23:20:35', '127.0.0.1', NULL, NULL, NULL),
	(132, 27, '[img]http://static.kisi-kisi.com/images/promo/blackberry-bellagio.jpg[/img]<br><br>Dear Kisiholic, dalam rangka merayakan peluncuran website promo and deals, kita dari Kisimedia Networks uda siapin 1 Blackberry Bellagio khusus untuk pemesan voucher dari website kisi-kisi secara [b]GRATIS[/b]. \r\n\r\nVoucher yang tersedia hanya 1000 lembar. Jadi, tidak ada salahnya mencoba peruntungan anda bukan ?\r\n\r\nSemua voucher akan diundi dan ditayangkan pemenangnya pada tanggal jatuh tempo yang ditentukan di poin dibawah ini.\r\n\r\nSyaratnya gampang banget:\r\n- promo dibuka selama 2 bulan \r\n- wajib menjadi member\r\n- membeli voucher melalui form yang tersedia.\r\n\r\nBuruan pesan dan tunggu promo-promo diskon kita yang udah siap menanti kalian guys.<br>', '1', '2012-01-10 23:32:01', '127.0.0.1', NULL, NULL, NULL),
	(133, 27, '[img]http://static.kisi-kisi.com/images/promo/test[/img]<br><br>dapatkan tas spongebob lucu<br>', '1', '2012-01-10 23:44:58', '127.0.0.1', NULL, NULL, NULL),
	(134, 27, '[img]http://static.kisi-kisi.com/images/promo/blackberry-bellagio.jpg[/img]<br><br>Dear Kisiholic, dalam rangka merayakan peluncuran website promo and deals, kita dari Kisimedia Networks uda siapin 1 Blackberry Bellagio khusus untuk pemesan voucher dari website kisi-kisi secara [b]GRATIS[/b]. \r\n\r\nVoucher yang tersedia hanya 1000 lembar. Jadi, tidak ada salahnya mencoba peruntungan anda bukan ?\r\n\r\nSemua voucher akan diundi dan ditayangkan pemenangnya pada tanggal jatuh tempo yang ditentukan di poin dibawah ini.\r\n\r\nSyaratnya gampang banget:\r\n- promo dibuka selama 2 bulan \r\n- wajib menjadi member\r\n- membeli voucher melalui form yang tersedia.\r\n\r\nBuruan pesan dan tunggu promo-promo diskon kita yang udah siap menanti kalian guys.<br>', '1', '2012-01-10 23:55:38', '127.0.0.1', NULL, NULL, NULL),
	(135, 27, 'test', '1', '2012-01-11 00:34:23', '127.0.0.1', NULL, NULL, NULL),
	(136, 27, 'etet', '1', '2012-01-11 00:53:48', '127.0.0.1', NULL, NULL, NULL),
	(137, 27, 'wewewe', '1', '2012-01-11 00:54:09', '127.0.0.1', NULL, NULL, NULL),
	(138, 28, 'baru', '1', '2012-01-11 00:58:05', '127.0.0.1', NULL, NULL, NULL),
	(139, 25, '[url]http://google.com[/url]', '1', '2012-01-14 10:41:17', '127.0.0.1', '1', '127.0.0.1', '2012-01-14 10:42:05'),
	(140, 29, '[img]http://static.kisi-kisi.com/images/promo/angry-birds-toys.jpg[/img]&lt;br&gt;&lt;br&gt;Siapa yang tidak tahu Angry Birds ? Bonekanya? pasti uda banyak yang punya. Gantungan kunci? Pasti sama deh kayak boneka. Tapi kalau real life game angry birds? \r\n\r\nPasti kisiholic belum ada yang punya! Dengan Real Life Game Angry Birds Knock on Wood, kalian bisa memainkan permainan Angry Birds seperti yang di iPad atau iPhone kalian semua secara real loh. Penasaran kan? Makanya buruan dibeli!&lt;br&gt;', '1', '2012-01-15 23:21:33', '127.0.0.1', '1', '127.0.0.1', '2012-02-02 10:40:46'),
	(141, 30, 'awalnya emang bingung sih mo curhat dimana, akhirnya gw disini deh ditemani oleh galau nite wkwkwkw\r\n\r\n[quote]emang begitulah kenyataannya cape edh[/quote]', '4', '2012-01-29 22:32:40', '127.0.0.1', NULL, NULL, NULL),
	(142, 30, '[size=10]123[/size]', '4', '2012-01-29 22:40:10', '127.0.0.1', NULL, NULL, NULL),
	(144, 32, 'Hai para kisiholic...  :D \r\nSaya akan menjelaskan aturan atau kriteria sebelum kalian membuka thread berjualan..\r\n\r\n1.cantumkan tag JUAL bila ingin menjual dan BELI untuk mencari barang yg diinginkan\r\n2.Mencantumkan deskripsi barang,secara detil,dan jangan lupa menyertakan gambar agar buyer mempunyai gambaran dari barang yg akan dibeli\r\n3.menjaga nama baik dengan menjaga kualitas dan tidak menipu', '1', '2012-02-02 10:25:30', '127.0.0.1', '1', '127.0.0.1', '2012-03-03 10:46:04'),
	(145, 32, 'tes', '1', '2012-02-02 10:25:41', '127.0.0.1', NULL, NULL, NULL),
	(146, 33, 'ini gw lagi jual diri ang berminat silakan', '1', '2012-02-02 10:26:09', '127.0.0.1', NULL, NULL, NULL),
	(147, 3, 'test', '4', '2012-02-03 22:06:38', '127.0.0.1', NULL, NULL, NULL),
	(148, 34, 'wew\r\n', '1', '2012-02-04 19:25:08', '127.0.0.1', NULL, NULL, NULL),
	(149, 33, '[img]http://localhost/kisi-kisi.com/images/masterlogocropsmall.png[/img]', '1', '2012-02-07 09:17:15', '127.0.0.1', NULL, NULL, NULL),
	(150, 32, 'tes', '1', '2012-02-24 20:32:57', '127.0.0.1', NULL, NULL, NULL),
	(151, 32, 'testatawt', '1', '2012-02-24 20:33:03', '127.0.0.1', NULL, NULL, NULL),
	(152, 32, 'bisa yah', '1', '2012-02-24 20:56:30', '127.0.0.1', NULL, NULL, NULL),
	(153, 32, 'a', '1', '2012-02-24 20:59:45', '127.0.0.1', NULL, NULL, NULL),
	(154, 32, 'test\r\n', '1', '2012-02-24 21:00:37', '127.0.0.1', NULL, NULL, NULL),
	(155, 32, 'again', '1', '2012-02-24 23:20:14', '127.0.0.1', NULL, NULL, NULL),
	(156, 32, ' :(  :) ', '2', '2012-02-29 22:10:44', '127.0.0.1', NULL, NULL, NULL),
	(157, 3, 'test', '4', '2012-03-03 09:45:08', '127.0.0.1', NULL, NULL, NULL),
	(158, 32, 'tes', '1', '2012-03-03 09:45:55', '127.0.0.1', NULL, NULL, NULL),
	(159, 35, 'hehe\r\nbisa kan', '1', '2012-03-03 18:15:23', '127.0.0.1', '1', '127.0.0.1', '2012-03-03 23:59:27'),
	(160, 34, 'test', '1', '2012-03-03 23:41:15', '127.0.0.1', NULL, NULL, NULL),
	(161, 34, 'www', '1', '2012-03-03 23:42:09', '127.0.0.1', NULL, NULL, NULL),
	(162, 34, 'aa', '1', '2012-03-03 23:43:22', '127.0.0.1', NULL, NULL, NULL),
	(163, 34, 'tewe', '1', '2012-03-03 23:47:06', '127.0.0.1', NULL, NULL, NULL),
	(164, 36, 'umum banget a', '1', '2012-03-03 23:48:18', '127.0.0.1', '1', '127.0.0.1', '2012-03-03 23:50:16'),
	(165, 36, 'test', '1', '2012-03-03 23:51:22', '127.0.0.1', NULL, NULL, NULL),
	(166, 36, 'test again hehe', '1', '2012-03-03 23:54:19', '127.0.0.1', NULL, NULL, NULL),
	(169, 37, '[img]http://static.kisi-kisi.com/images/promo/angrybird-big-bag-red_1.jpg[/img]<br><br>Hai para Kisilovers, kali ini kita ada promo untuk tas lucu angrybirds berbahan beludru dengan 5 pilihan karakter. Tas lucu ini cocok banget buat kamu-kamu yang masih remaja dan hobi dengan si burung imut yang populer lewat game ini.\r\n\r\nBuruan beli yuu..karena barangnya terbatas guys.<br> [url]http://localhost/kisi-kisi.com/promo/promo.php?p=18[/url]', '1', '2012-03-05 20:45:07', '127.0.0.1', NULL, NULL, NULL),
	(170, 38, '[img]http://static.kisi-kisi.com/images/promo/angrybird-big-bag-red_1.jpg[/img]<br><br>Hai para Kisilovers, kali ini kita ada promo untuk tas lucu angrybirds berbahan beludru dengan 5 pilihan karakter. Tas lucu ini cocok banget buat kamu-kamu yang masih remaja dan hobi dengan si burung imut yang populer lewat game ini.\r\n\r\nBuruan beli yuu..karena barangnya terbatas guys.<br> [url]http://localhost/kisi-kisi.com/promo/promo.php?p=19[/url]', '1', '2012-03-05 21:23:56', '127.0.0.1', NULL, NULL, NULL),
	(171, 39, '[img]http://static.kisi-kisi.com/images/promo/angrybird-big-bag-red_1.jpg[/img]<br><br>Hai para Kisilovers, kali ini kita ada promo untuk tas lucu angrybirds berbahan beludru dengan 5 pilihan karakter. Tas lucu ini cocok banget buat kamu-kamu yang masih remaja dan hobi dengan si burung imut yang populer lewat game ini.\r\n\r\nBuruan beli yuu..karena barangnya terbatas guys.\r\n<br> [url]http://localhost/kisi-kisi.com/promo/promo.php?p=20[/url]', '1', '2012-03-05 21:27:00', '127.0.0.1', NULL, NULL, NULL),
	(172, 40, '[img]http://s1.kisi-kisi.com/images/promo/sample_promo1.jpg[/img]<br><br>nih promo bakmi ga jelas<br> [url]http://localhost/kisi-kisi.com/promo/promo.php?p=21[/url]', '1', '2012-03-05 21:36:08', '127.0.0.1', NULL, NULL, NULL),
	(173, 41, '[img]http://s1.kisi-kisi.com/images/promo/sample_promo1.jpg[/img]<br><br>nih promo bakmi ga jelas<br> [url]http://localhost/kisi-kisi.com/promo/promo.php?p=22[/url]', '1', '2012-03-05 21:40:33', '127.0.0.1', NULL, NULL, NULL),
	(174, 42, '[img]http://s1.kisi-kisi.com/images/promo/sample_promo1.jpg[/img]<br><br>nih promo bakmi ga jelas<br> [url]http://localhost/kisi-kisi.com/promo/promo.php?p=23[/url]', '1', '2012-03-05 22:17:01', '127.0.0.1', NULL, NULL, NULL),
	(175, 43, '[img]http://s1.kisi-kisi.com/images/promo/sample_promo1.jpg[/img]<br><br>nih promo bakmi ga jelas<br> [url]http://localhost/kisi-kisi.com/promo/promo.php?p=24[/url]', '1', '2012-03-05 22:17:33', '127.0.0.1', NULL, NULL, NULL),
	(176, 44, '[img]http://s1.kisi-kisi.com/images/promo/sample_promo1.jpg[/img]<br><br>nih promo bakmi ga jelas<br> [url]http://localhost/kisi-kisi.com/promo/promo.php?p=25[/url]', '1', '2012-03-05 22:17:57', '127.0.0.1', NULL, NULL, NULL),
	(177, 45, '[img]http://s1.kisi-kisi.com/images/promo/sample_promo1.jpg[/img]<br><br>nih promo bakmi ga jelas<br> [url]http://localhost/kisi-kisi.com/promo/promo.php?p=26[/url]', '1', '2012-03-05 22:18:14', '127.0.0.1', NULL, NULL, NULL),
	(178, 46, '[img]http://s1.kisi-kisi.com/images/promo/sample_promo1.jpg[/img]<br><br>nih promo bakmi ga jelas<br> [url]http://localhost/kisi-kisi.com/promo/promo.php?p=27[/url]', '1', '2012-03-05 22:18:48', '127.0.0.1', NULL, NULL, NULL),
	(179, 47, '[img]http://s1.kisi-kisi.com/images/promo/sample_promo1.jpg[/img]<br><br>nih promo bakmi ga jelas<br> [url]http://localhost/kisi-kisi.com/promo/promo.php?p=28[/url]', '1', '2012-03-05 22:20:20', '127.0.0.1', NULL, NULL, NULL),
	(180, 48, '[img]http://s1.kisi-kisi.com/images/promo/sample_promo1.jpg[/img]<br><br>nih promo bakmi ga jelas<br> [url]http://localhost/kisi-kisi.com/promo/promo.php?p=29[/url]', '1', '2012-03-06 08:06:53', '127.0.0.1', NULL, NULL, NULL),
	(181, 49, '[img]http://s1.kisi-kisi.com/images/promo/sample_promo1.jpg[/img]<br><br>nih promo bakmi ga jelas<br> [url]http://localhost/kisi-kisi.com/promo/promo.php?p=30[/url]', '1', '2012-03-06 08:08:02', '127.0.0.1', NULL, NULL, NULL),
	(182, 50, '[img]http://s1.kisi-kisi.com/images/promo/sample_promo1.jpg[/img]<br><br>promo ga jelas<br> [url]http://localhost/kisi-kisi.com/promo/promo.php?p=31[/url]', '1', '2012-03-06 08:17:31', '127.0.0.1', NULL, NULL, NULL),
	(183, 51, '[img]http://s1.kisi-kisi.com/images/promo/sample_promo1.jpg[/img]<br><br>promo ga jelas<br> [url]http://localhost/kisi-kisi.com/promo/promo.php?p=32[/url]', '1', '2012-03-06 08:18:56', '127.0.0.1', NULL, NULL, NULL),
	(184, 52, '[img]http://s1.kisi-kisi.com/images/promo/angrybird-big-bag-red_1.jpg[/img] /n /n Hai para Kisilovers, kali ini kita ada promo untuk tas lucu angrybirds berbahan beludru dengan 5 pilihan karakter. Tas lucu ini cocok banget buat kamu-kamu yang masih remaja dan hobi dengan si burung imut yang populer lewat game ini.\r\n\r\nBuruan beli yuu..karena barangnya terbatas guys.&lt;br&gt; [url]http://localhost/kisi-kisi.com/promo/promo.php?p=33[/url]', '1', '2012-03-06 08:35:30', '127.0.0.1', '1', '127.0.0.1', '2012-03-06 09:07:54'),
	(185, 53, '[img]http://s1.kisi-kisi.com/images/promo/angrybird-big-bag-red_1.jpg[/img]\r\n			\r\n			Hai para Kisilovers, kali ini kita ada promo untuk tas lucu angrybirds berbahan beludru dengan 5 pilihan karakter. Tas lucu ini cocok banget buat kamu-kamu yang masih remaja dan hobi dengan si burung imut yang populer lewat game ini.\r\n\r\nBuruan beli yuu..karena barangnya terbatas guys.\r\n			\r\n			[url]http://localhost/kisi-kisi.com/promo/promo.php?p=34[/url]', '1', '2012-03-06 09:10:29', '127.0.0.1', NULL, NULL, NULL),
	(186, 54, '\r\n[img]http://s1.kisi-kisi.com/images/promo/angrybird-big-bag-red_1.jpg[/img]\r\n			\r\nHai para Kisilovers, kali ini kita ada promo untuk tas lucu angrybirds berbahan beludru dengan 5 pilihan karakter. Tas lucu ini cocok banget buat kamu-kamu yang masih remaja dan hobi dengan si burung imut yang populer lewat game ini.\r\n\r\nBuruan beli yuu..karena barangnya terbatas guys.\r\n			\r\n[url]http://localhost/kisi-kisi.com/promo/promo.php?p=35[/url]', '1', '2012-03-06 09:13:07', '127.0.0.1', NULL, NULL, NULL),
	(188, 55, 'ni jualan barang bekas aaaa', '1', '2012-03-19 23:44:35', '127.0.0.1', '1', '127.0.0.1', '2012-03-20 23:07:29'),
	(189, 55, 'a', '1', '2012-03-22 21:59:37', '127.0.0.1', NULL, NULL, NULL),
	(190, 55, 'a', '1', '2012-03-22 21:59:45', '127.0.0.1', NULL, NULL, NULL),
	(191, 55, 'a', '1', '2012-03-22 21:59:48', '127.0.0.1', NULL, NULL, NULL),
	(192, 55, 'wewe', '1', '2012-03-22 21:59:52', '127.0.0.1', NULL, NULL, NULL),
	(193, 55, 'adwe', '1', '2012-03-22 21:59:55', '127.0.0.1', NULL, NULL, NULL),
	(194, 55, 'tes', '1', '2012-03-22 21:59:59', '127.0.0.1', NULL, NULL, NULL),
	(195, 55, 'gubrak', '1', '2012-03-22 22:00:02', '127.0.0.1', NULL, NULL, NULL),
	(196, 55, 'alay', '1', '2012-03-22 22:00:05', '127.0.0.1', NULL, NULL, NULL),
	(197, 55, 'absurd', '1', '2012-03-22 22:00:08', '127.0.0.1', NULL, NULL, NULL),
	(198, 55, ' :stare: ', '1', '2012-03-22 22:00:12', '127.0.0.1', NULL, NULL, NULL),
	(199, 55, ' :shy:  :shy: ', '1', '2012-03-22 22:00:16', '127.0.0.1', NULL, NULL, NULL),
	(200, 55, ' :D ', '1', '2012-03-22 22:00:22', '127.0.0.1', NULL, NULL, NULL),
	(201, 55, ' :devil: ', '1', '2012-03-22 22:00:25', '127.0.0.1', NULL, NULL, NULL),
	(202, 55, ' :stare: ', '1', '2012-03-22 22:00:28', '127.0.0.1', NULL, NULL, NULL),
	(203, 55, ' :stare: ', '1', '2012-03-22 22:00:30', '127.0.0.1', NULL, NULL, NULL),
	(204, 55, ' :) ', '1', '2012-03-22 22:00:33', '127.0.0.1', NULL, NULL, NULL),
	(205, 55, ' :) ', '1', '2012-03-22 22:00:40', '127.0.0.1', NULL, NULL, NULL),
	(206, 55, ' :stare: ', '1', '2012-03-22 22:00:48', '127.0.0.1', NULL, NULL, NULL),
	(207, 55, ' :stare: ', '1', '2012-03-22 22:00:53', '127.0.0.1', NULL, NULL, NULL),
	(208, 55, 'a', '1', '2012-03-22 22:00:59', '127.0.0.1', NULL, NULL, NULL),
	(209, 55, 'te', '1', '2012-03-22 22:01:03', '127.0.0.1', NULL, NULL, NULL),
	(210, 55, 'te', '1', '2012-03-22 22:01:07', '127.0.0.1', NULL, NULL, NULL),
	(211, 55, 'wad', '1', '2012-03-22 22:01:17', '127.0.0.1', NULL, NULL, NULL),
	(212, 55, 'adawd', '1', '2012-03-22 22:01:20', '127.0.0.1', NULL, NULL, NULL),
	(213, 55, 'adwawd', '1', '2012-03-22 22:01:23', '127.0.0.1', NULL, NULL, NULL),
	(215, 56, 'test', '1', '2012-03-25 15:45:19', '127.0.0.1', NULL, NULL, NULL),
	(216, 57, 'test adawdadawd', '1', '2012-04-29 21:43:38', '127.0.0.1', NULL, NULL, NULL);
/*!40000 ALTER TABLE `grv_forumtopicdetail` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;